from alpha_csg_cudaless import *



# TODO: delme! =)
# TODO: have add_instruction/operation function which sets partial_program index and stores intermediate sdfs
def program_to_image(program, hyper):
    image = torch.ones(hyper['image_size'], hyper['image_size']).cuda()
    sdf = sdf_inf

    for one_hot_shape in program:
        # print(one_hot_shape)
        index = torch.argmax(one_hot_shape).item()
        if index == NUM_DISCRETE_POSITIONS_XY**2:
            break  # 'Stop' symbol
        y, x = index//NUM_DISCRETE_POSITIONS_XY, index%NUM_DISCRETE_POSITIONS_XY
        # print(index, y, x)
        shape_class = Shape(6*x, 6*y, 3, 'circle', sdf=hyper['viz_sim_mode'] == 'sdf')
        image = torch.min(shape_class.image, image)
        sdf = torch.min(shape_class.sdf, sdf)

    image.sdf = sdf  # SNEAKILY attach this to a torch.tensor. i'm a bad person i know
    return image



def test_viz_sim_score():
    hyper = {}

    hyper['viz_sim_mode'] = 'sdf'
    hyper['image_size'] = 32

    empty_prog = new_random_program(hyper, 1)[1]
    one_prog = new_random_program(hyper, 2)[1]
    one_prog2 = new_random_program(hyper, 2)[1]
    full_prog = new_random_program(hyper, 200)[1]

    empty_image = torch.zeros(hyper['image_size'], hyper['image_size'])

    #last_img = empty_image
    #for i in range(17):
    #    p = torch.zeros(1, 17)
    #    p[0][i] = 1
    #    img = p.image
    #    print_two_images(last_img, img)
    #    last_img = img

    print_two_images(empty_prog.image, full_prog.image)
    print_two_images(one_prog.image, one_prog2.image)
    print_sdf(one_prog)

    print_two_images( one_prog2.image, one_prog.sdf * (1 - one_prog2.image) )

    print('***', hyper['viz_sim_mode'])
    print(viz_sim_score(empty_prog, empty_prog, hyper))
    print(viz_sim_score(empty_prog, one_prog, hyper))
    print(viz_sim_score(one_prog, one_prog, hyper))
    print(viz_sim_score(one_prog, one_prog2, hyper))
    print(viz_sim_score(empty_prog, full_prog, hyper))
    # print(viz_sim_score(empty_prog, full_image, hyper))

    hyper['viz_sim_mode'] = 'euclidean'

    print('***', hyper['viz_sim_mode'])
    print(viz_sim_score(empty_prog, empty_prog, hyper))  # 1
    print(viz_sim_score(empty_prog, one_prog, hyper))
    print(viz_sim_score(one_prog, one_prog, hyper))  # 1
    print(viz_sim_score(one_prog, one_prog2, hyper))
    print(viz_sim_score(empty_prog, full_prog, hyper))
    # print(viz_sim_score(empty_prog, full_image, hyper))  # -1

    print('***')


test_viz_sim_score()





