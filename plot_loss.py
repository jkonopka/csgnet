
import numpy as np
import matplotlib.pyplot as plt


def parse_loss(s):
    a = s.split()
    ret = []
    label = None
    write_next = label_next = False
    for x in a:
        if write_next:
            write_next = False
            if x != 'nan':
                ret.append(float(x))
        if label_next:
            label_next = False
            label = '{:.6f}'.format(float(x[:-1]))
        if x == 'loss':
            write_next = True
        if x == '"learning_rate":':
            label_next = True
    print(ret, label)
    return (ret, label)


ss = []

ss.append('''
Training started at 2019-05-31 08:36:23.934918
Starting RNN training with hyperparameters
{
    "batch_size": 8,
    "checkpoint_interval": 320,
    "dataset": "postfix_raster_circles_simple",
    "exploration": 2.020570557232563,
    "image_size": 32,
    "l2_regularization": 0,
    "learning_rate": 0.0002340076508672951,
    "max_program_length": 3,
    "normalize_probabilities": false,
    "num_actions": 20,
    "num_discrete_positions_xy": 4,
    "num_images_training": 15040,
    "num_playouts": 64,
    "optimizer": "adam",
    "save_models": false,
    "start_time": "2019-05-31 08:36:23.934918",
    "temperature": 1,
    "training_mode": "mcts_rnn",
    "use_dummy_mcts": true,
    "verbosity": null,
    "viz_sim_mode": "euclidean"
}
   0:01:03  Image      320  |  loss   7.81 = val   0.17 + pdf   7.64  |  cnn0s 206/512  |  train  1.00  test_nn  0.55
   0:02:04  Image      640  |  loss   6.44 = val   0.00 + pdf   6.44  |  cnn0s 105/512  |  train  1.00  test_nn  0.63
   0:03:06  Image      960  |  loss   5.73 = val   0.00 + pdf   5.73  |  cnn0s 139/512  |  train  1.00  test_nn  0.70
   0:04:08  Image     1280  |  loss   5.25 = val   0.00 + pdf   5.25  |  cnn0s  76/512  |  train  1.00  test_nn  0.69
   0:05:10  Image     1600  |  loss   4.91 = val   0.00 + pdf   4.91  |  cnn0s 110/512  |  train  1.00  test_nn  0.71
   0:06:12  Image     1920  |  loss   4.35 = val   0.00 + pdf   4.35  |  cnn0s  56/512  |  train  1.00  test_nn  0.79
   0:07:14  Image     2240  |  loss   3.81 = val   0.00 + pdf   3.81  |  cnn0s  73/512  |  train  1.00  test_nn  0.87
   0:08:16  Image     2560  |  loss   3.57 = val   0.00 + pdf   3.57  |  cnn0s  42/512  |  train  1.00  test_nn  0.92
   0:09:18  Image     2880  |  loss   2.91 = val   0.00 + pdf   2.91  |  cnn0s  49/512  |  train  1.00  test_nn  0.95
   0:10:20  Image     3200  |  loss   2.77 = val   0.00 + pdf   2.77  |  cnn0s  77/512  |  train  1.00  test_nn  0.96
   0:11:22  Image     3520  |  loss   2.80 = val   0.00 + pdf   2.80  |  cnn0s  50/512  |  train  1.00  test_nn  0.96
   0:12:24  Image     3840  |  loss   2.62 = val   0.00 + pdf   2.62  |  cnn0s  79/512  |  train  1.00  test_nn  0.98
   0:13:26  Image     4160  |  loss   2.52 = val   0.00 + pdf   2.52  |  cnn0s 107/512  |  train  1.00  test_nn  0.99
   0:14:27  Image     4480  |  loss   2.57 = val   0.00 + pdf   2.57  |  cnn0s  65/512  |  train  1.00  test_nn  0.98
   0:15:29  Image     4800  |  loss   2.50 = val   0.00 + pdf   2.50  |  cnn0s  56/512  |  train  1.00  test_nn  0.98
   0:16:31  Image     5120  |  loss   2.46 = val   0.00 + pdf   2.46  |  cnn0s  63/512  |  train  1.00  test_nn  0.96
   0:17:34  Image     5440  |  loss   2.20 = val   0.00 + pdf   2.20  |  cnn0s  65/512  |  train  1.00  test_nn  0.96
   0:18:36  Image     5760  |  loss   2.34 = val   0.00 + pdf   2.34  |  cnn0s 136/512  |  train  1.00  test_nn  0.99
   0:19:37  Image     6080  |  loss   2.49 = val   0.00 + pdf   2.49  |  cnn0s  83/512  |  train  1.00  test_nn  0.99
   0:20:39  Image     6400  |  loss   2.09 = val   0.00 + pdf   2.09  |  cnn0s  82/512  |  train  1.00  test_nn  0.98
   0:21:41  Image     6720  |  loss   2.03 = val   0.00 + pdf   2.03  |  cnn0s 173/512  |  train  1.00  test_nn  0.98
   0:22:43  Image     7040  |  loss   2.21 = val   0.00 + pdf   2.21  |  cnn0s  92/512  |  train  1.00  test_nn  0.99
   0:23:45  Image     7360  |  loss   2.14 = val   0.00 + pdf   2.14  |  cnn0s  78/512  |  train  1.00  test_nn  0.99
   0:24:48  Image     7680  |  loss   2.26 = val   0.00 + pdf   2.26  |  cnn0s 223/512  |  train  1.00  test_nn  0.98
   0:25:50  Image     8000  |  loss   2.26 = val   0.00 + pdf   2.26  |  cnn0s 215/512  |  train  1.00  test_nn  0.98
   0:26:52  Image     8320  |  loss   2.15 = val   0.00 + pdf   2.15  |  cnn0s 127/512  |  train  1.00  test_nn  1.00
   0:27:54  Image     8640  |  loss   1.97 = val   0.00 + pdf   1.97  |  cnn0s 220/512  |  train  1.00  test_nn  1.00
''')

ss.append('''
Training started at 2019-05-31 07:21:38.812903
Starting RNN training with hyperparameters
{
    "batch_size": 8,
    "checkpoint_interval": 320,
    "dataset": "postfix_raster_circles_simple",
    "exploration": 1.2320749683526149,
    "image_size": 32,
    "l2_regularization": 0,
    "learning_rate": 9.198767077897468e-05,
    "max_program_length": 3,
    "normalize_probabilities": false,
    "num_actions": 20,
    "num_discrete_positions_xy": 4,
    "num_images_training": 15040,
    "num_playouts": 64,
    "optimizer": "adam",
    "save_models": false,
    "start_time": "2019-05-31 07:21:38.812903",
    "temperature": 1,
    "training_mode": "mcts_rnn",
    "use_dummy_mcts": true,
    "verbosity": null,
    "viz_sim_mode": "euclidean"
}
   0:00:12  Image      320  |  loss   8.39 = val   0.37 + pdf   8.03  |  cnn0s 153/512  |  train  1.00  test_nn -1.00
   0:00:37  Image      640  |  loss   6.94 = val   0.00 + pdf   6.94  |  cnn0s 159/512  |  train  1.00  test_nn  0.54
   0:01:03  Image      960  |  loss   6.54 = val   0.00 + pdf   6.54  |  cnn0s 114/512  |  train  1.00  test_nn  0.75
   0:01:28  Image     1280  |  loss   6.31 = val   0.00 + pdf   6.31  |  cnn0s 108/512  |  train  1.00  test_nn  0.46
   0:01:53  Image     1600  |  loss   6.12 = val   0.00 + pdf   6.12  |  cnn0s 100/512  |  train  1.00  test_nn  0.70
   0:02:19  Image     1920  |  loss   5.94 = val   0.00 + pdf   5.94  |  cnn0s  81/512  |  train  1.00  test_nn  0.72
   0:02:44  Image     2240  |  loss   5.66 = val   0.00 + pdf   5.66  |  cnn0s  63/512  |  train  1.00  test_nn  0.68
   0:03:10  Image     2560  |  loss   5.25 = val   0.00 + pdf   5.25  |  cnn0s  68/512  |  train  1.00  test_nn  0.72
   0:03:35  Image     2880  |  loss   5.14 = val   0.00 + pdf   5.14  |  cnn0s  73/512  |  train  1.00  test_nn  0.73
   0:04:01  Image     3200  |  loss   5.03 = val   0.00 + pdf   5.03  |  cnn0s  56/512  |  train  1.00  test_nn  0.71
   0:04:27  Image     3520  |  loss   4.87 = val   0.00 + pdf   4.87  |  cnn0s  49/512  |  train  1.00  test_nn  0.69
   0:04:52  Image     3840  |  loss   4.77 = val   0.00 + pdf   4.77  |  cnn0s  56/512  |  train  1.00  test_nn  0.73
   0:05:18  Image     4160  |  loss   4.67 = val   0.00 + pdf   4.67  |  cnn0s  57/512  |  train  1.00  test_nn  0.76
   0:05:43  Image     4480  |  loss   4.56 = val   0.00 + pdf   4.56  |  cnn0s  51/512  |  train  1.00  test_nn  0.74
   0:06:09  Image     4800  |  loss   4.26 = val   0.00 + pdf   4.26  |  cnn0s  78/512  |  train  1.00  test_nn  0.79
   0:06:34  Image     5120  |  loss   4.10 = val   0.00 + pdf   4.10  |  cnn0s  78/512  |  train  1.00  test_nn  0.77
   0:07:00  Image     5440  |  loss   4.01 = val   0.00 + pdf   4.01  |  cnn0s  55/512  |  train  1.00  test_nn  0.84
   0:07:25  Image     5760  |  loss   3.62 = val   0.00 + pdf   3.62  |  cnn0s  53/512  |  train  1.00  test_nn  0.80
   0:07:50  Image     6080  |  loss   3.55 = val   0.00 + pdf   3.55  |  cnn0s  57/512  |  train  1.00  test_nn  0.88
   0:08:16  Image     6400  |  loss   3.40 = val   0.00 + pdf   3.40  |  cnn0s  76/512  |  train  1.00  test_nn  0.91
   0:08:42  Image     6720  |  loss   3.23 = val   0.00 + pdf   3.23  |  cnn0s  49/512  |  train  1.00  test_nn  0.91
   0:09:07  Image     7040  |  loss   2.95 = val   0.00 + pdf   2.95  |  cnn0s  50/512  |  train  1.00  test_nn  0.95
   0:09:33  Image     7360  |  loss   2.89 = val   0.00 + pdf   2.89  |  cnn0s  80/512  |  train  1.00  test_nn  0.97
   0:09:58  Image     7680  |  loss   2.88 = val   0.00 + pdf   2.88  |  cnn0s  48/512  |  train  1.00  test_nn  0.96
   0:10:24  Image     8000  |  loss   2.81 = val   0.00 + pdf   2.81  |  cnn0s  74/512  |  train  1.00  test_nn  0.98
   0:10:49  Image     8320  |  loss   2.40 = val   0.00 + pdf   2.40  |  cnn0s  74/512  |  train  1.00  test_nn  0.93
   0:11:15  Image     8640  |  loss   2.58 = val   0.00 + pdf   2.58  |  cnn0s  61/512  |  train  1.00  test_nn  0.98
   0:11:41  Image     8960  |  loss   2.49 = val   0.00 + pdf   2.49  |  cnn0s  80/512  |  train  1.00  test_nn  0.97
   0:12:06  Image     9280  |  loss   2.48 = val   0.00 + pdf   2.48  |  cnn0s  65/512  |  train  1.00  test_nn  0.96
   0:12:32  Image     9600  |  loss   2.50 = val   0.00 + pdf   2.50  |  cnn0s  52/512  |  train  1.00  test_nn  0.96
   0:12:57  Image     9920  |  loss   2.40 = val   0.00 + pdf   2.40  |  cnn0s 112/512  |  train  1.00  test_nn  0.98
   0:13:23  Image    10240  |  loss   2.22 = val   0.00 + pdf   2.22  |  cnn0s  62/512  |  train  1.00  test_nn  0.98
   0:13:48  Image    10560  |  loss   2.35 = val   0.00 + pdf   2.35  |  cnn0s  52/512  |  train  1.00  test_nn  0.98
   0:14:14  Image    10880  |  loss   2.32 = val   0.00 + pdf   2.32  |  cnn0s  56/512  |  train  1.00  test_nn  0.98
   0:14:39  Image    11200  |  loss   2.29 = val   0.00 + pdf   2.29  |  cnn0s  88/512  |  train  1.00  test_nn  1.00
   0:15:05  Image    11520  |  loss   2.48 = val   0.00 + pdf   2.48  |  cnn0s  68/512  |  train  1.00  test_nn  0.99
   0:15:30  Image    11840  |  loss   2.38 = val   0.00 + pdf   2.38  |  cnn0s  62/512  |  train  1.00  test_nn  1.00
   0:15:56  Image    12160  |  loss   2.41 = val   0.00 + pdf   2.41  |  cnn0s 116/512  |  train  1.00  test_nn  1.00
   0:16:22  Image    12480  |  loss   2.25 = val   0.00 + pdf   2.25  |  cnn0s  58/512  |  train  1.00  test_nn  0.99
   0:16:47  Image    12800  |  loss   2.05 = val   0.00 + pdf   2.05  |  cnn0s 122/512  |  train  1.00  test_nn  0.99
   0:17:13  Image    13120  |  loss   2.04 = val   0.00 + pdf   2.04  |  cnn0s  74/512  |  train  1.00  test_nn  0.99
   0:17:39  Image    13440  |  loss   2.20 = val   0.00 + pdf   2.20  |  cnn0s  67/512  |  train  1.00  test_nn  0.99
   0:18:04  Image    13760  |  loss   2.31 = val   0.00 + pdf   2.31  |  cnn0s  55/512  |  train  1.00  test_nn  1.00
   0:18:30  Image    14080  |  loss   2.14 = val   0.00 + pdf   2.14  |  cnn0s  75/512  |  train  1.00  test_nn  0.98
   0:18:55  Image    14400  |  loss   2.29 = val   0.00 + pdf   2.29  |  cnn0s 124/512  |  train  1.00  test_nn  1.00
   0:19:21  Image    14720  |  loss   2.10 = val   0.00 + pdf   2.10  |  cnn0s 130/512  |  train  1.00  test_nn  0.98
   0:19:46  Image    15040  |  loss   2.09 = val   0.00 + pdf   2.09  |  cnn0s  74/512  |  train  1.00  test_nn  0.99
''')

ss.append('''
Training started at 2019-05-31 08:14:25.676949
Starting RNN training with hyperparameters
{
    "batch_size": 8,
    "checkpoint_interval": 320,
    "dataset": "postfix_raster_circles_simple",
    "exploration": 0.3567448370456272,
    "image_size": 32,
    "l2_regularization": 0,
    "learning_rate": 3.418051254030649e-05,
    "max_program_length": 3,
    "normalize_probabilities": false,
    "num_actions": 20,
    "num_discrete_positions_xy": 4,
    "num_images_training": 15040,
    "num_playouts": 64,
    "optimizer": "adam",
    "save_models": false,
    "start_time": "2019-05-31 08:14:25.676949",
    "temperature": 1,
    "training_mode": "mcts_rnn",
    "use_dummy_mcts": true,
    "verbosity": null,
    "viz_sim_mode": "euclidean"
}
   0:00:12  Image      320  |  loss  10.23 = val   1.35 + pdf   8.89  |  cnn0s 204/512  |  train  1.00  test_nn -1.00
   0:00:23  Image      640  |  loss   7.73 = val   0.06 + pdf   7.67  |  cnn0s 148/512  |  train  1.00  test_nn -1.00
   0:00:34  Image      960  |  loss   7.17 = val   0.02 + pdf   7.15  |  cnn0s 187/512  |  train  1.00  test_nn -1.00
   0:00:45  Image     1280  |  loss   6.93 = val   0.02 + pdf   6.92  |  cnn0s 139/512  |  train  1.00  test_nn -1.00
   0:01:11  Image     1600  |  loss   6.75 = val   0.01 + pdf   6.73  |  cnn0s 119/512  |  train  1.00  test_nn  0.57
   0:01:36  Image     1920  |  loss   6.55 = val   0.01 + pdf   6.55  |  cnn0s  87/512  |  train  1.00  test_nn  0.48
   0:02:02  Image     2240  |  loss   6.35 = val   0.00 + pdf   6.35  |  cnn0s  99/512  |  train  1.00  test_nn  0.73
   0:02:27  Image     2560  |  loss   6.09 = val   0.00 + pdf   6.09  |  cnn0s  75/512  |  train  1.00  test_nn  0.73
   0:02:53  Image     2880  |  loss   5.73 = val   0.00 + pdf   5.73  |  cnn0s  51/512  |  train  1.00  test_nn  0.74
   0:03:19  Image     3200  |  loss   5.56 = val   0.00 + pdf   5.56  |  cnn0s  53/512  |  train  1.00  test_nn  0.66
   0:03:44  Image     3520  |  loss   5.46 = val   0.00 + pdf   5.46  |  cnn0s  53/512  |  train  1.00  test_nn  0.70
   0:04:10  Image     3840  |  loss   5.35 = val   0.00 + pdf   5.35  |  cnn0s  66/512  |  train  1.00  test_nn  0.69
   0:04:35  Image     4160  |  loss   5.20 = val   0.00 + pdf   5.20  |  cnn0s  24/512  |  train  1.00  test_nn  0.72
   0:05:01  Image     4480  |  loss   5.09 = val   0.00 + pdf   5.09  |  cnn0s  38/512  |  train  1.00  test_nn  0.70
   0:05:26  Image     4800  |  loss   4.95 = val   0.00 + pdf   4.95  |  cnn0s  28/512  |  train  1.00  test_nn  0.74
   0:05:52  Image     5120  |  loss   4.89 = val   0.00 + pdf   4.89  |  cnn0s  21/512  |  train  1.00  test_nn  0.71
   0:06:18  Image     5440  |  loss   4.77 = val   0.00 + pdf   4.77  |  cnn0s  21/512  |  train  1.00  test_nn  0.74
   0:06:43  Image     5760  |  loss   4.73 = val   0.00 + pdf   4.73  |  cnn0s  20/512  |  train  1.00  test_nn  0.77
   0:07:09  Image     6080  |  loss   4.58 = val   0.00 + pdf   4.58  |  cnn0s  24/512  |  train  1.00  test_nn  0.73
   0:07:34  Image     6400  |  loss   4.63 = val   0.00 + pdf   4.63  |  cnn0s  18/512  |  train  1.00  test_nn  0.75
   0:07:59  Image     6720  |  loss   4.44 = val   0.00 + pdf   4.44  |  cnn0s  16/512  |  train  1.00  test_nn  0.79
   0:08:25  Image     7040  |  loss   4.29 = val   0.00 + pdf   4.29  |  cnn0s  22/512  |  train  1.00  test_nn  0.76
   0:08:51  Image     7360  |  loss   4.18 = val   0.00 + pdf   4.18  |  cnn0s  22/512  |  train  1.00  test_nn  0.81
   0:09:16  Image     7680  |  loss   4.08 = val   0.00 + pdf   4.08  |  cnn0s  22/512  |  train  1.00  test_nn  0.83
   0:09:42  Image     8000  |  loss   3.66 = val   0.00 + pdf   3.66  |  cnn0s  20/512  |  train  1.00  test_nn  0.86
   0:10:07  Image     8320  |  loss   3.70 = val   0.00 + pdf   3.70  |  cnn0s  16/512  |  train  1.00  test_nn  0.84
   0:10:33  Image     8640  |  loss   3.73 = val   0.00 + pdf   3.73  |  cnn0s  16/512  |  train  1.00  test_nn  0.90
   0:10:58  Image     8960  |  loss   3.56 = val   0.00 + pdf   3.56  |  cnn0s  20/512  |  train  1.00  test_nn  0.86
   0:11:24  Image     9280  |  loss   3.48 = val   0.00 + pdf   3.48  |  cnn0s  16/512  |  train  1.00  test_nn  0.88
   0:11:49  Image     9600  |  loss   3.57 = val   0.00 + pdf   3.57  |  cnn0s  12/512  |  train  1.00  test_nn  0.91
   0:12:16  Image     9920  |  loss   3.36 = val   0.00 + pdf   3.36  |  cnn0s   6/512  |  train  1.00  test_nn  0.94
   0:12:41  Image    10240  |  loss   3.26 = val   0.00 + pdf   3.26  |  cnn0s   5/512  |  train  1.00  test_nn  0.93
   0:13:07  Image    10560  |  loss   3.17 = val   0.00 + pdf   3.17  |  cnn0s  11/512  |  train  1.00  test_nn  0.94
   0:13:33  Image    10880  |  loss   3.08 = val   0.00 + pdf   3.08  |  cnn0s   4/512  |  train  1.00  test_nn  0.97
   0:13:58  Image    11200  |  loss   2.93 = val   0.00 + pdf   2.93  |  cnn0s   9/512  |  train  1.00  test_nn  0.93
   0:14:24  Image    11520  |  loss   2.92 = val   0.00 + pdf   2.92  |  cnn0s   4/512  |  train  1.00  test_nn  0.94
   0:14:49  Image    11840  |  loss   3.05 = val   0.00 + pdf   3.05  |  cnn0s   3/512  |  train  1.00  test_nn  0.95
   0:15:15  Image    12160  |  loss   2.83 = val   0.00 + pdf   2.83  |  cnn0s   6/512  |  train  1.00  test_nn  0.96
   0:15:41  Image    12480  |  loss   2.97 = val   0.00 + pdf   2.97  |  cnn0s   9/512  |  train  1.00  test_nn  0.96
   0:16:06  Image    12800  |  loss   2.78 = val   0.00 + pdf   2.78  |  cnn0s   2/512  |  train  1.00  test_nn  0.99
   0:16:32  Image    13120  |  loss   2.81 = val   0.00 + pdf   2.81  |  cnn0s   0/512  |  train  1.00  test_nn  0.97
   0:16:58  Image    13440  |  loss   2.80 = val   0.00 + pdf   2.80  |  cnn0s   2/512  |  train  1.00  test_nn  0.97
   0:17:23  Image    13760  |  loss   2.75 = val   0.00 + pdf   2.75  |  cnn0s   0/512  |  train  1.00  test_nn  0.97
   0:17:49  Image    14080  |  loss   2.78 = val   0.00 + pdf   2.78  |  cnn0s   0/512  |  train  1.00  test_nn  0.97
   0:18:14  Image    14400  |  loss   2.39 = val   0.00 + pdf   2.39  |  cnn0s   0/512  |  train  1.00  test_nn  0.97
   0:18:40  Image    14720  |  loss   2.58 = val   0.00 + pdf   2.58  |  cnn0s   0/512  |  train  1.00  test_nn  0.98
   0:19:06  Image    15040  |  loss   2.47 = val   0.00 + pdf   2.47  |  cnn0s   0/512  |  train  1.00  test_nn  0.98
''')

losses = []
labels = []
xs = []
for s in ss:
    loss, label = parse_loss(s)
    losses.append(loss)
    labels.append(label)
    xs.append(np.arange(0, len(loss), 1))

for i in range(len(losses)):
    plt.plot(xs[i], losses[i], label=str(labels[i]), marker='o')  # markersize=5
plt.legend(loc=1, borderaxespad=1)
plt.show()


