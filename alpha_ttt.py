from copy import deepcopy as copy
from itertools import product
from collections import namedtuple
import random
import math
import os.path
import sys
import time
import json
import functools
import datetime

# THESIS TODO
# - try JanNet instead of OthelloNet

# POSSIBLE NON-THESIS TODO
# - add hyperparameter for dropout
# - temperature/learnrate decay during learning
# - re-use MCTS tree in subsequent move
# - (allow resumption of training (store optimizer))
# - add command line arguments

start_time = time.clock()

# TODO: review how this is reinforcement learning

################### Helpers ###################

getlegals = lambda board: [(i,j) for i,j in product(range(3), repeat=2)
    if not board[i][j] ]
getlegalsmask = lambda board: [el == 0 for row in board for el in row]
board_full = lambda board:  not getlegals(board)
is_win = lambda a, mark:  any(line == 3 * mark for line in
    [a[i][0]+a[i][1]+a[i][2] for i in range(3)] +
    [a[0][i]+a[1][i]+a[2][i] for i in range(3)] +
    [a[0][0]+a[1][1]+a[2][2], a[0][2]+a[1][1]+a[2][0]] )

def kronecker_delta(n, i):
    ret = [0] * n
    ret[i] = 1
    return ret

################### Minimax ###################

Evaluation = namedtuple('Evaluation', 'value moves')

# _verbosity == depth of search to print
def minimax(board, maximize=True, mymark=-1, _verbosity=0):
    if is_win(board, -mymark):  return Evaluation(-1, [])
    if is_win(board, mymark):   return Evaluation( 1, [])
    if not getlegals(board):    return Evaluation( 0, [])  # this must come LAST!

    best = Evaluation(float('-inf') if maximize else float('inf'), None)

    for i,j in getlegals(board):
        board[i][j] = mymark if maximize else -mymark
        rec = minimax(board, not maximize, mymark, _verbosity - 1)
        if ((rec.value > best.value) if maximize else (rec.value < best.value)):
            best = Evaluation(rec.value, [(i,j)] + rec.moves)
        board[i][j] = 0
        if _verbosity > 0: print(i, j, rec)

    return best


tuple_rec = lambda board: tuple(tuple(row) for row in board)
cache = dict()


# values from the perspective of mymark
def minimax_for_nn(board, mymark, maximize=True, strength=50, _verbosity=0, **kwargs):

    if (tuple_rec(board), maximize, mymark, strength) in cache:
        return cache[(tuple_rec(board), maximize, mymark, strength)]

    if is_win(board, -mymark):  return (..., -1)
    if is_win(board, mymark):   return (...,  1)
    if not getlegals(board):    return (...,  0)

    pdf = [0] * 9
    val = -1 if maximize else 1

    for i,j in getlegals(board):
        pdf[3*i+j] = 1
        board[i][j] = mymark if maximize else -mymark
        rec = minimax_for_nn(board, mymark, not maximize, strength, _verbosity - 1)
        if rec[-1] == 0:
            pdf[3*i+j] = strength
            val = max(val, rec[-1]) if maximize else min(val, rec[-1])
        if (rec[-1] == 1) if maximize else (rec[-1] == -1):
            pdf[3*i+j] = strength*strength
            val = max(val, rec[-1]) if maximize else min(val, rec[-1])
        board[i][j] = 0
        if _verbosity > 0: print(i, j, rec)

    pdf = [p/sum(pdf) for p in pdf]

    cache[(tuple_rec(board), maximize, mymark, strength)] = (pdf, val)

    return (pdf, val)


################### Neural Net ###################

import torch
import torch.nn as nn
import torch.nn.functional as F

import torch.optim as optim


# class Net(nn.Module):

#     def __init__(self):
#         super().__init__()
#         # TODO: 1 or 2 or 3 boards ?
#         # 1 input channel, 128 output channels, 3x3 kernel, stride 1, zero padding yes 1, 1)
#         self.inconv = nn.Conv2d(1, 128, 3, 1, 1)

#         # self.convs = []
#         self.conv1 = nn.Conv2d(128, 128, 3, 1, 1)
#         self.conv2 = nn.Conv2d(128, 128, 3, 1, 1)
#         self.conv3 = nn.Conv2d(128, 128, 3, 1, 1)
#         # self.conv4 = nn.Conv2d(128, 128, 3, 1, 1)
#         # self.conv5 = nn.Conv2d(128, 128, 3, 1, 1)

#         # only policy network
#         self.pconv = nn.Conv2d(128,   1, 1, 1, 0)
#         self.pfc = nn.Linear(9, 9)
#         self.psmax = nn.Softmax(0)

#         # only value network
#         self.vconv = nn.Conv2d(128,   1, 1, 1, 0)
#         self.vfc1 = nn.Linear(9, 16)
#         self.vfc2 = nn.Linear(16, 1)


#     def forward(self, x):
#         x = F.relu(self.inconv(x))

#         x = F.relu(self.conv1(x))
#         x = F.relu(self.conv2(x))
#         x = F.relu(self.conv3(x))
#         # x = F.relu(self.conv4(x))
#         # x = F.relu(self.conv5(x))

#         p = F.relu(self.pconv(x))
#         p = p.view(-1)
#         # p = self.pfc(p)
#         # p = F.relu(self.pfc(p))
#         p = self.psmax(self.pfc(p))

#         v = F.relu(self.vconv(x))
#         v = v.view(-1)
#         v = F.relu(self.vfc1(v))
#         v = torch.tanh(self.vfc2(v))

#         return (p, v)

# TODO: Network taken from !
#   https://github.com/suragnair/alpha-zero-general
# To be changed later!

class Net(nn.Module):
    def __init__(self):
        self.board_x, self.board_y = 3, 3
        self.action_size = 9

        super().__init__()
        self.conv1 = nn.Conv2d(1, 128, 3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(128, 128, 3, stride=1, padding=1)
        self.conv3 = nn.Conv2d(128, 128, 3, stride=1, padding=1)
        self.conv4 = nn.Conv2d(128, 128, 3, stride=1, padding=1)

        self.bn1 = nn.BatchNorm2d(128)
        self.bn2 = nn.BatchNorm2d(128)
        self.bn3 = nn.BatchNorm2d(128)
        self.bn4 = nn.BatchNorm2d(128)

        self.fc1 = nn.Linear(128*(self.board_x)*(self.board_y), 1024)
        self.fc_bn1 = nn.BatchNorm1d(1024)

        self.fc2 = nn.Linear(1024, 512)
        self.fc_bn2 = nn.BatchNorm1d(512)

        self.fc3 = nn.Linear(512, self.action_size)

        self.fc4 = nn.Linear(512, 1)

        self.jansoftmax = nn.Softmax(1)

    def forward(self, s):
        print(s)
        s = s.view(-1, 1, self.board_x, self.board_y)
        print(s)
        s = F.relu(self.bn1(self.conv1(s)))
        s = F.relu(self.bn2(self.conv2(s)))
        s = F.relu(self.bn3(self.conv3(s)))
        s = F.relu(self.bn4(self.conv4(s)))
        s = s.view(-1, 128*(self.board_x)*(self.board_y))

        self.fc1(s)
        self.fc_bn1(self.fc1(s))
        s = F.dropout(F.relu(self.fc_bn1(self.fc1(s))), p=.1, training=self.training)
        s = F.dropout(F.relu(self.fc_bn2(self.fc2(s))), p=.1, training=self.training)

        pi = self.fc3(s)
        v = self.fc4(s)

        # return F.log_softmax(pi, dim=1), torch.tanh(v)
        return self.jansoftmax(pi), torch.tanh(v)


################### MCTS ###################

import numpy.random

class MCTSNode:
    def __init__(self, move, parent):
        self.move = move
        self.parent = parent
        self.wins = self.games = 0
        self.probability = 1
        self.children = []
        self.is_terminal = False

is_leaf = lambda n: n.games == 0

normalize_board = lambda board, mymark:  board if mymark == 1 \
    else [[-el for el in row] for row in board]


# MCTS perspective:  i am always 1, even when simulating opponent moves
# NN perspective:  person to play is always 1
# temperature != 0  => training/self-play mode, otherwise testing
def mcts(board, mymark, net=None, temperature=1, _verbosity=0, **kwargs):

    hyper = dict(**kwargs)
    board = normalize_board(board, mymark)

    num_playouts = 1000
    num_playouts = hyper['num_playouts']
    root = MCTSNode(None, None)  # TODO: re-use the chosen part of the tree ?
    original_board = copy(board)

    if _verbosity > 1: print('call of mcts with (board, mymark) = ', board, mymark)

    for _ in range(num_playouts+1):  # 1 to expand children of root

        board = copy(original_board)
        turn = True

        cur_node = root

        if _verbosity > 1: print()
        if _verbosity > 1: print(" *** NEW PLAYOUT *** ")
        if _verbosity > 1: print(board, turn)

        # 1. Selection
        while not is_leaf(cur_node) and not cur_node.is_terminal and cur_node.children:

            exploration = hyper['exploration']
            Q = lambda c: (c.wins + (1e-300 if hyper['init_Q_to_draw'] else 0))/(c.games + 2e-300)
            U = lambda c: exploration * c.probability * cur_node.games**.5 / (1 + c.games)
            cur_node = sorted(cur_node.children, key=lambda c: Q(c) + U(c) )[-1]   # this should almost never tie cos of NN probs

            i,j = cur_node.move
            board[i][j] = 1 if turn else -1
            turn = not turn

        leaf_node_turn = turn

        if not cur_node.is_terminal:

            # 2. Expansion
            # 3. Simulation (= Playout)
            #   => replaced by NN valuation here

            # The sole exception is the noise that is added to the prior policy to ensure exploration (29); this is scaled in proportion to the typical number of legal moves for that game type.

            net.eval()
            board2 = copy(normalize_board(board, 1 if turn else -1))
            probs, value = net(torch.Tensor(board2).unsqueeze(0).unsqueeze(0).cuda())

            probs = probs[0]

            if (True if hyper['add_noise_during_induction'] else temperature != 0):
                if (True if hyper['always_add_noise'] else cur_node == root):

                    if _verbosity > 1: print('old probs', probs)

                    if hyper['dirichlet']:
                        noise = numpy.random.dirichlet([.03] * 9)  # sum to 1
                        probs = .75 * probs + .25 * torch.Tensor(noise).cuda()
                    else:
                        probs += .1

                    if _verbosity > 1: print('new probs', probs)

            if hyper['normalize_probabilities']:
                probs *= torch.Tensor(getlegalsmask(board)).cuda()
            probs /= sum(probs)
            probs = probs.tolist()
            value = value.item()

            if _verbosity > 1: print('final probs', probs)

            outcome = .5 + .5 * value * (1 if turn else -1)

            for i,j in getlegals(board):
                child_node = MCTSNode((i,j), cur_node)
                board[i][j] = 1 if turn else -1
                child_node.is_terminal = (is_win(board, 1) or is_win(board, -1) or board_full(board))
                child_node.probability = probs[3*i+j]
                board[i][j] = 0
                cur_node.children.append(child_node)

        else:
            outcome = .5 + .5 * is_win(board, 1) - .5 * is_win(board, -1)

        if _verbosity > 1: print(board, turn, outcome, 'is random leaf')

        turn = not leaf_node_turn  # 'turn' meant 'who will do the NEXT move' until here
        # turn = leaf_node_turn

        # 4. Backpropagation
        # i DON'T update the child node's values as it is not expanded => lost efficiency?
        while cur_node.parent:
            cur_node.wins += outcome if turn else 1-outcome
            cur_node.games += 1

            turn = not turn
            cur_node = cur_node.parent

        assert cur_node == root
        cur_node.wins += outcome if turn else 1-outcome
        cur_node.games += 1

        # root.wins == wins for the OPPONENT !!  think it through
        if _verbosity > 1: print(root.games, root.wins, turn)


    for c in root.children:
        if _verbosity > 0: print(c.move, c.wins, c.games, c.wins/(c.games+10**-300))


    # updated policy: just the normalized move counts
    max_games = max(c.games for c in root.children)

    pdf = [0] * 9
    for c in root.children:
        i, j = c.move
        if temperature == 0:
            pdf[3*i+j] = c.games == max_games  # if draw, pick random
        else:
            pdf[3*i+j] = c.games**(1/temperature)  # low temps select according to 'max'

    pdf = [p/sum(pdf) for p in pdf]

    return pdf



def play_nn(board, mymark, net=None, hardmax=True, _verbosity=0, **kwargs):

    board2 = copy(normalize_board(board, mymark))
    probs, value = net(torch.Tensor(board2).unsqueeze(0).unsqueeze(0).cuda())

    print(board, probs)

    legs = set(getlegals(board))
    for i in range(9):
        if (i//3,i%3) not in legs:
            probs[0][i] = 0

    if not hardmax:
        # softmax
        probs = probs.detach().cpu()[0].tolist()
        probs = [p / sum(probs) for p in probs]
    else:
        # hardmax
        probs = kronecker_delta(9, probs.argmax(1))

    return probs


def play_mcts(board, mymark, net=None, hardmax=True, _verbosity=0, **kwargs):

    # MCTS never considers illegal moves
    kwargs = copy(kwargs)
    kwargs['temperature'] = 0
    probs = mcts(board, mymark, net=net, _verbosity=_verbosity, **kwargs)
    if _verbosity > 1: print(board, probs)

    return probs


def play_random(board, mymark, **kwargs):

    legs = getlegals(board)
    move = random.choice(legs)
    return kronecker_delta(9, 3*move[0]+move[1])


def get_human_move(board, mymark, **kwargs):
    legs = getlegals(board)
    if len(legs) == 1:
        return kronecker_delta(9, 3*legs[0][0]+legs[0][1])

    while True:
        try:
            row = int(input('Input human move (row, 0-2): '))
            assert 0 <= row <= 2
            col = int(input('Input human move (col, 0-2): '))
            assert 0 <= col <= 2
            assert board[row][col] == 0
            break
        except KeyboardInterrupt as e:
            raise
        except:
            pass

    return kronecker_delta(9, 3*row+col)


Player = namedtuple('Player', 'name getmove')
hmn = Player('Human', get_human_move)

cpu_mcts_train = Player('COM MCTS (Train)', mcts)
cpu_mcts = Player('COM MCTS', play_mcts)
cpu_nn = Player('COM NN', play_nn)
cpu_random = Player('COM Random', play_random)

# cpu_minimax = Player('COM Minimax', functools.partial(minimax_for_nn, strength=500000000)[0] )
# cpu_almost_minimax = Player('COM Almost-Minimax', functools.partial(minimax_for_nn, strength=50)[0] )
cpu_minimax = Player('COM Minimax', lambda *args, **kwargs: minimax_for_nn(*args, strength=500000000, **kwargs)[0] )
cpu_almost_minimax = Player('COM Almost-Minimax', lambda *args, **kwargs: minimax_for_nn(*args, strength=50, **kwargs)[0] )



# TODO: which function? lowering temps sensible?
# 1/ (1+i/10000

# Play a match (and collect training examples which can be ignored)
def play(p1, p2, p1net=None, p2net=None, _verbosity=0, game_number=1, p1_kwargs={}, p2_kwargs={}):

    if p1net: p1net.eval()
    if p2net: p2net.eval()
    training_examples = []
    board = [[0,0,0],[0,0,0],[0,0,0]]

    for p, mark, net, kwargs in zip([p1, p2] * 5, [+1, -1] * 5, [p1net, p2net] * 5, [p1_kwargs, p2_kwargs] * 5):
        pdf = p.getmove(board, mark, net=net, _verbosity=_verbosity, **kwargs)  # kwargs e.g. used for temperature
        # pdf = minimax_for_nn(board, True, mark)[0]

        board2 = copy(normalize_board(board, mark))
        train = [board2, pdf]
        training_examples.append(train)

        if _verbosity > 1 and net:
            pol, v = net(torch.Tensor(board2).unsqueeze(0).unsqueeze(0).cuda())
            print('NN Policy: ', [poli.item() for poli in pol[0]])
            print('NN Value: ', v[0].item())
            print('Move Policy: ', pdf)

        draw = numpy.random.choice(len(pdf), p=pdf)
        move = (draw//3, draw%3)

        # print(board, pdf, move)

        board[move[0]][move[1]] = mark
        if _verbosity > 0:
            print(move); [print(' '.join({1:'X',-1:'O',0:'.'}[m] for m in r)) for r in board]; print()

        elapsed = time.clock() - start_time

        if is_win(board, mark):
            training_examples = [t + [z] for t,z in zip(training_examples, [mark, -mark] * 5)]
            if _verbosity > -1: print(str(game_number).rjust(6), datetime.timedelta(seconds=elapsed), ' ! ' + p.name + ' (' + str(mark) + ') won !')
            break

        if not getlegals(board):
            training_examples = [t + [0] for t in training_examples]
            if _verbosity > -1: print(str(game_number).rjust(6), datetime.timedelta(seconds=elapsed), ' ! Draw !')
            break

    # winner is stored in training_examples[0][-1]
    return training_examples


# Returns score from X's perspective on 100 matches
def score(p1, p2, p1net=None, p2net=None, _verbosity=0, p1_kwargs={}, p2_kwargs={}, num_games=100):

    stats = [0] * 3

    for i in range(num_games):
        winner = play(p1, p2, p1net=p1net, p2net=p2net, _verbosity=_verbosity, game_number=i+1, p1_kwargs=p1_kwargs, p2_kwargs=p2_kwargs)[0][-1]
        stats[1+winner] += 1

    if _verbosity > 0:
        print('Stats from Os perspective (w/d/l): ', stats, stats[0]-stats[-1])

    return (stats[-1] - stats[0]) / num_games


def train(hyper, run_id, SAVE_INTERVAL=1000, NUM_GAMES=30000, _verbosity=0):

    print('Starting training with hyperparameters', hyper)

    net = Net().cuda()
    # TODO use better optimizing algorithm ?
    optimizer = optim.SGD(net.parameters(), lr=hyper['learning_rate'], momentum=0.9)

    BASE_PATH = 'trained_models/alpha_ttt_' + str(run_id)
    running_loss = torch.Tensor([0]).cuda()
    training_buffer = []
    stats = [1e-300] * 3

    # TODO: Ordered Dict !
    with open(BASE_PATH + '.hyper', 'w') as f:
        json.dump(hyper, f, indent=4, sort_keys=True)
    with open(BASE_PATH + '.score', 'w') as f:
        json.dump(hyper, f, indent=4, sort_keys=True)
        f.write('\n')

    for i in range(NUM_GAMES+1):

        net.eval()

        if i % SAVE_INTERVAL == 0:
            SAVE_PATH = BASE_PATH + '_' + str(i) + '.pth'
            torch.save(net.state_dict(), SAVE_PATH)

            performance = -score(cpu_minimax, cpu_mcts, p2net=net, _verbosity=_verbosity-1, p2_kwargs=hyper)
            loss_avg = running_loss.item() / SAVE_INTERVAL
            outstr = 'Self-play game ' + str(i).rjust(6) + ':  win {:.2f}'.format(performance) + '  loss {:5.1f}'.format(loss_avg) + '  Owin {:.3f} draw {:.3f} Xwin {:.3f}'.format(*[s/sum(stats) for s in stats]) + '\n'
            with open(BASE_PATH + '.score', 'a') as f:
                f.write(outstr)

            print('Saved to file', SAVE_PATH)
            print(outstr, end='')
            running_loss = torch.Tensor([0]).cuda()

        training_buffer += play(cpu_mcts_train, cpu_mcts_train, p1net=net, p2net=net, _verbosity=_verbosity, game_number=i, p1_kwargs=hyper, p2_kwargs=hyper)
        winner = training_buffer[0][-1]
        stats[1+winner] += 1

        if len(training_buffer) < hyper['batch_size']:
            continue
        training_examples = training_buffer[:hyper['batch_size']]
        training_buffer = training_buffer[hyper['batch_size']:]

        net.train()
        optimizer.zero_grad()

        board_stack = torch.stack([torch.Tensor(t[0]).unsqueeze(0) for t in training_examples]).cuda()

        nn_probss, nn_values = net(board_stack)

        mc_outcomes = [t[-1] for t in training_examples]
        mc_pdfs = [t[1] for t in training_examples]

        loss = 0

        for nn_probs, nn_value, mc_outcome, mc_pdf in zip(nn_probss, nn_values, mc_outcomes, mc_pdfs):
            # Loss    (v(s) - z)^2  -  pi^T log(p(s))  +  c ||th||^2
            loss += (nn_value - mc_outcome)**2 - torch.Tensor(mc_pdf).cuda() @ torch.log(nn_probs).cuda()
            # loss += ( (nn_value - mc_outcome)**2 - torch.Tensor(mc_pdf).cuda() @ torch.log(nn_probs).cuda() )  / len(outcomes)
            if _verbosity > 1:  print(nn_value, mc_outcome, mc_pdf, nn_probs)

        # with L2 regularization to prevent overfitting
        for p in net.parameters():
            loss += hyper['l2_regularization'] * (p**2).sum()

        loss.backward()
        running_loss += loss
        optimizer.step()

    return (loss_avg, performance)




# * CMD LINE INTERFACE *

if len(sys.argv) <= 1 or sys.argv[1] not in ['train', 'test', 'score']:
    print('', file=sys.stderr)
    print('*AlphaTicTacToe* is an AI implementing the AlphaGo algorithm for the', file=sys.stderr)
    print('game of TicTacToe, largely as a PyTorch and MCTS exercise', file=sys.stderr)
    print('', file=sys.stderr)
    print('Usage 1: alpha_ttt.py [train|test|score] PATH_TO_NEURAL_NETWORK.PTH', file=sys.stderr)
    print('   train:  Trains ENTIRELY NEW network with identical hyperparameters', file=sys.stderr)
    print('   test:   Play 1 human-computer matchup with heavy debug printing', file=sys.stderr)
    print('   score:  Play 100 matches as O against minimax (= perfect play)', file=sys.stderr)
    print('', file=sys.stderr)
    print('Usage 2: alpha_ttt.py train', file=sys.stderr)
    print('   Trains networks with random hyperparameters ad infinitum', file=sys.stderr)
    print('   Protip: If you have n cores start this script n times in parallel', file=sys.stderr)
    print('', file=sys.stderr)
    exit()


# DEFAULT HYPERPARAMETERS -- needed so default values for newly added hyperps present
hyper = {'always_add_noise': True, 'add_noise_during_induction': True, 'dirichlet': False, 'temperature': 1, 'normalize_probabilities': True, 'mcts_old': False, 'init_Q_to_draw': False, 'batch_size': -1,
    'exploration': 3, 'learning_rate': .0001, 'l2_regularization': 0,    'num_playouts': 32}


if len(sys.argv) >= 3:
    LOAD_PATH = sys.argv[2]
    HYPER_PATH = LOAD_PATH[:LOAD_PATH.rfind('_')] + '.hyper'

    print('* Loading hyperparameters from', HYPER_PATH, '\n')

    # .update() allows to load hyper files created by previous versions of this software
    with open(HYPER_PATH) as f:
        hyper.update(json.load(f))

    if sys.argv[1] in ['test', 'score', 'scorenn']:

        print('* Loading neural network from', LOAD_PATH, '\n')

        net = Net().cuda()
        net.load_state_dict(torch.load(LOAD_PATH, map_location='cuda'))
        net.eval()

        if 'test' == sys.argv[1]:
            play(hmn, cpu_mcts, p2net=net, _verbosity=999, p2_kwargs=hyper)

        if 'score' == sys.argv[1]:
            score(cpu_minimax, cpu_mcts, p2net=net, _verbosity=1, p2_kwargs=hyper, num_games=5000)
        if 'scorenn' == sys.argv[1]:
            score(cpu_minimax, cpu_nn, p2net=net, _verbosity=1, p2_kwargs=hyper, num_games=5000)

    # ---- Minimax Scoring ----

    # format: Train-Test, AlphaGo = MCTS-MCTS  =)

    # O = Minimax-NN      120000  X =        Minimax:  [0,100,  0]   0
    # O = MCTSMinimax-NN   10000  X =        Minimax:  [0, 96,  4]  -4
    # O = UntrainedNN-MCTS     0  X =        Minimax:  [0, 23, 77] -77
    # O = Random                  X =        Minimax:  [0,  1, 99] -99

    # O = MCTS-NN          19000  X =        Minimax:  [0, 55, 45] -45  explo = 3  num_playouts = 32  lr = .001
    # O = MCTS-MCTS        19000  X =        Minimax:  [0, 94,  6]  -6  explo = 3  num_playouts = 32  lr = .001

    # O = MCTS-NN         172560  X =        Minimax:  [0, 88, 12] -12  explo = 3  num_playouts = 32  lr = .0001
    # O = MCTS-MCTS       172560  X =        Minimax:  [0,100,  0]   0  explo = 3  num_playouts = 32  lr = .0001
    #           it's weird it's so good esp cos training doesn't have a lot of draws



# * REINFORCEMENT LEARNING BY SELF-PLAY *

if 'train' in sys.argv:

    # hyperparameters -- part of a correct implementation
    hyper['dirichlet'] = True
    hyper['always_add_noise'] = False
    hyper['add_noise_during_induction'] = False
    hyper['init_Q_to_draw'] = True

    # hyperparameters -- unsure
    hyper['normalize_probabilities'] = False

    # random search is superior to grid search because it allows us to also
    #   test potentially unimportant parameters at low cost =)
    # it allows allows us to run multiple processes (limit with my GPU is 3)
    #   which takes advantage of CPU parallelism (almost no parallelism peanlty)

    # TODO: allow dynamic adjustment of ranges, e.g. call self as subprocess or read from file
    def random_hyper_generator():
        while True:
            hyper['exploration'] = 10**random.uniform(0, .3)
            hyper['learning_rate'] = 10**random.uniform(-5, -3.5)
            hyper['l2_regularization'] = 10**random.uniform(-4, -.5)
            hyper['normalize_probabilities'] = random.choice([True, False])
            hyper['batch_size'] = random.choice([64, 128, 256])
            yield hyper

    # first search for few iterations corsely, later finely for more iterations
    # 1h for 10'000 gens  @  32 playouts
    for config in random_hyper_generator():
        run_id = str(datetime.datetime.now()).replace(' ', '_')
        loss_avg, performance = train(config, run_id, NUM_GAMES=50000, _verbosity=0)

        with open('alpha_ttt_results_master', 'a') as f:
            f.write(run_id + "  explo {:.1f}  lr {:.6f}  reg {:.4f}  bs {:3d}  np {:1d} iQd {:1d}  |  loss {:4.1f}  win {:.2f}\n".format( hyper['exploration'], hyper['learning_rate'], hyper['l2_regularization'], hyper['batch_size'], hyper['normalize_probabilities'], hyper['init_Q_to_draw'], loss_avg, performance ))


'''
################ Wikipedia ################

- heuristic search for decisions
    - used eg in AlphaGo
            evaluation               -> NNs
            policy = move selection  -> MCTS
    - converges to minimax

- playouts
    - playout = play game out to very end using random moves
    - final game result is used to weigh nodes
        so better nodes will be more likely to be chosen

- MCTS phases
    - Selection   find leaf L (= has no playouts yet)
    - Expansion   create child(ren) C at leaf L
    - Simulation  = playout  from C
    - Backprop    update info along path from L to root R
                    ie wins+1 (losses+1) and games+1 for path nodes
                    -> score 'wins/games' at each node

- basic / 'Pure' MCTS
    - eval each legal move with same # of playouts

    -> better: assign more playouts to likely-winning moves
        based on previous playouts

        => move with MOST SIMULATIONS made is played !
            NOT best ratio, but highest denominator

- balancing exploration and exploitation
    EXPLORATION of few-playouts moves  vs
    EXPLOITATION of deep variants of promising moves

    => [UCT (Upper Confidence Bound 1 applied to trees)]
            wi / ni  +  c rt(ln Ni / ni)
    EXPLOIT -------     ---------------- EXPLORATION

+/-
    + in pure form:  no need for evaluation function
    + good results in high-branching-factor games
        bc tree grows asymmetrically
    + anytime algorithm
    + parallelization possible

    - can overlook it if only a single path leads to a loss

- light vs heavy playouts
    Light:  random moves
    Heavy:  use heuristics in move choice
        eg expert evaluation / domain knowledge  (what AlphaGo used ?)
        ->  significant improvements possible, fewer its needed

- RAVE (Rapid Action Value Estimation)
    - idea: allow switching move order
    - after a playout, update not just along the path,
        but every node on the tree where moves of the playout are found
            -> weigh 'true' with move-switched playouts

'''
