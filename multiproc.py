import torch
import torch.nn as nn
import torch.multiprocessing

import torch.nn.functional as F

import time


torch.multiprocessing.set_start_method('spawn', force=True)


##########################

class Encoder(nn.Module):
    def __init__(self, dropout=0, invert_input=False):
        super(Encoder, self).__init__()
        self.conv1 = nn.Conv2d(1, 8, 3, padding=(1, 1))
        self.conv2 = nn.Conv2d(8, 16, 3, padding=(1, 1))
        self.conv3 = nn.Conv2d(16, 32, 3, padding=(1, 1))
        self.drop = nn.Dropout(dropout)

    def encode(self, x):
        x = F.max_pool2d(self.drop(F.relu(self.conv1(x))), (2, 2))
        x = F.max_pool2d(self.drop(F.relu(self.conv2(x))), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))
        return x

def kronecker_delta_cpu(n, i):
    return (torch.arange(n) == i).float()

def kronecker_delta(n, i):
    return (torch.arange(n).cuda() == i).float()

def get_new_image(i, j):
    new_image = torch.zeros(8, 8).cuda()
    new_image[i][j] = 1
    return new_image



class MCTSNode:
    def __init__(self, id, parent):
        self.id = id
        self.parent = parent
        self.children = []


##########################

def get_samples(queue, net, id, num_its):
    for i in range(num_its):
        time.sleep(.2)

        draws = [-1, 6, 3]
        target_pdfs = [get_new_image(0, draw) for draw in draws]
        target_score = 1

        # queue.put((draws, target_pdfs, target_score))
        # queue.put(target_pdfs[i].unsqueeze(0).unsqueeze(0))

        net.eval()
        out = net.encode(target_pdfs[i].unsqueeze(0).unsqueeze(0))
        queue.put(out.detach())


    # IMPORTANT! SENDING PROCESS MUST STAY ALIVE WHILE Q NON-EMPTY
    #   OR ELSE TENSOR INTEGRITY NOT GUARANTEED
    # time.sleep(.1)
    while not queue.empty():
        time.sleep(.03)

def make_children(cur_node):
    child1 = MCTSNode(2 * cur_node.id, cur_node)
    child2 = MCTSNode(2 * cur_node.id + 1, cur_node)
    cur_node.children += [child1, child2]



if __name__ == '__main__':

    net = Encoder().cuda()
    net.eval()
    net.share_memory()

    queue = torch.multiprocessing.Queue()
    num_workers = 2
    num_its = 3

    processes = []

    for i in range(num_workers):
        p = torch.multiprocessing.Process(target=get_samples, args=[queue, net, i, num_its])
        p.start()
        processes.append(p)

    # Shared neural network test

    for _ in range(num_workers * num_its):
        e = queue.get()
        print(e)
        # print(net.encode(e))


    # Shared memory test


    root = MCTSNode(1, None)

    p = torch.multiprocessing.Process(target=make_children, args=[root])
    p.start()

    print(root.children)


# This works but takes an inordinate amount of
#   time, GPU mem, and GUI lagginess
# I hope that is only cos of the high startup cost
#   of multiple Python processes etc






# I'm sharing a PyTorch neural network model between a main thread which `trains` the model and a number of worker threads which `eval` the model to generate training samples (à la AlphaGo).

# My question is, do I need to create a separate mutex to lock and unlock when accessing the model in different threads?

# ----

# I'm asking because my main thread gets stuck (deadlocked) on `loss.backward()` or `model.parameters()` on just the 3rd iteration. However my worker threads run to almost-completion (just waiting on the main thread to finish) so I'm very confused how a deadlock should occur here.

# I did not see any mutex in the PyTorch [Hogwild](https://github.com/pytorch/examples/blob/master/mnist_hogwild/main.py) example, however the difference there is that all threads are in `train` mode.

# Apart from the official [tutorial](https://pytorch.org/docs/stable/notes/multiprocessing.html), I'm unable to find even basic information (like this question) online.

# I'm using the `torch.multiprocessing` module for this and I'm using `model.share_memory()` and `torch.multiprocessing.set_start_method('spawn', force=True)`.






# I seem to run into this problem with PyTorch 1.2.0 (8 August 2019).

# I'm sharing a PyTorch neural network model between a main thread which `trains` the model and a number of worker threads which `eval` the model to generate training samples (à la AlphaGo).

# My main thread gets stuck (deadlocked) on `loss.backward()` or `model.parameters()` on just the 3rd iteration. However my worker threads run to almost-completion (just waiting on the main thread to finish) so I'm very confused how a deadlock should occur here.

# More details: This happens every time seed-independently. Ctrl+C only stops the 3 worker threads but not the main thread (strange!), so I cannot get a stack trace. This also seems to happen for >=3 workers only.

# Also see my question on StackOverflow: https://stackoverflow.com/questions/57940151/pytorch-sharing-a-model-between-threads-do-i-need-to-lock-it-myself









# I have worker threads running MCTS with a neural network (essentially the AlphaGo algorithm). The neural network is used by the main thread for training and by worker threads for evaluation and of course shared with .share_memory()

# I went to test the effect of different numbers of workers on runtime and with num_workers=3 I'm always (seed-independently) getting a deadlock during loss.backward() on just the 3rd sample. And that is despite the 3 worker threads running to completion, meaning they cannot be utilizing the net anymore.

# Ctrl+C only stops the 3 worker threads but not the main thread (!) so I'm unable to obtain a stack trace, but thanks to debug printing I know it occurs in loss.backward().


