from copy import deepcopy as copy
from itertools import product
from collections import namedtuple
import random
import math


# Questions:
# - Do we re-use the chosen part of the MC tree? Does that even make much of a difference?
#       We don't re-use it in Minimax so I think re-use might not be too useful

################### Helpers ###################

getlegals = lambda board: [(i,j) for i,j in product(range(3), repeat=2)
    if not board[i][j] ]
board_full = lambda board:  not getlegals(board)
is_win = lambda a, mark:  any(line == 3 * mark for line in
    [a[i][0]+a[i][1]+a[i][2] for i in range(3)] +
    [a[0][i]+a[1][i]+a[2][i] for i in range(3)] +
    [a[0][0]+a[1][1]+a[2][2], a[0][2]+a[1][1]+a[2][0]] )


################### Minmax ###################

Evaluation = namedtuple('Evaluation', 'value moves')

# _verbosity == depth of search to print
def minmax(board, maximize=True, mymark=-1, _verbosity=0):
    if is_win(board, -mymark):  return Evaluation(-1, [])
    if is_win(board, mymark):   return Evaluation( 1, [])
    if not getlegals(board):    return Evaluation( 0, [])  # this must come LAST!

    best = Evaluation(float('-inf') if maximize else float('inf'), None)

    for i,j in getlegals(board):
        board[i][j] = mymark if maximize else -mymark
        rec = minmax(board, not maximize, mymark, _verbosity - 1)
        if ((rec.value > best.value) if maximize else (rec.value < best.value)):
            best = Evaluation(rec.value, [(i,j)] + rec.moves)
        board[i][j] = 0
        if _verbosity > 0: print(i, j, rec)

    return best


################### MCTS ###################

class MCTSNode:
    def __init__(self, move, parent):
        self.move = move
        self.parent = parent
        self.wins = self.games = 0
        self.children = []
        self.is_terminal = False

is_leaf = lambda n: n.games == 0


def mcts(board, mymark=-1, _verbosity=0, explo_vs_explo=True):

    num_playouts = 10000
    root = MCTSNode(None, None)  # TODO: re-use the chosen part of the tree ?
    original_board = copy(board)

    for _ in range(num_playouts+1):  # 1 to expand children of root

        board = copy(original_board)
        turn = True

        cur_node = root

        if _verbosity > 1: print()
        if _verbosity > 1: print(" *** NEW PLAYOUT *** ")
        if _verbosity > 1: print(board, turn)

        # 1. Selection
        while not is_leaf(cur_node) and not cur_node.is_terminal and cur_node.children:

            if explo_vs_explo:
                # https://en.wikipedia.org/wiki/Monte_Carlo_tree_search#Exploration_and_exploitation
                cur_node = sorted(cur_node.children, key=lambda c: c.wins/(c.games + 10**-300) + (2 * math.log(root.games) / (c.games + 10**-300))**.5  )[-1]
            else:
                cur_node = random.choice(cur_node.children)

            i,j = cur_node.move
            board[i][j] = mymark if turn else -mymark
            turn = not turn

        if _verbosity > 1: print(board, "random leaf", turn)
        leaf_node_turn = turn

        if not cur_node.is_terminal:

            for i,j in getlegals(board):
                child_node = MCTSNode((i,j), cur_node)
                board[i][j] = mymark if turn else -mymark
                child_node.is_terminal = (is_win(board, mymark) or is_win(board, -mymark) or board_full(board))
                board[i][j] = 0
                cur_node.children.append(child_node)

            # 2. Expansion
            child_node = random.choice(cur_node.children)
            i,j = child_node.move
            board[i][j] = mymark if turn else -mymark
            turn = not turn

            if _verbosity > 1: print(board, "random child", turn)

            # 3. Simulation (= Playout)
            # random playout from child node
            while not (is_win(board, mymark) or is_win(board, -mymark) or board_full(board)):
                i,j = random.choice(getlegals(board))
                board[i][j] = mymark if turn else -mymark
                turn = not turn

            if _verbosity > 1: print(board, "finished random playout", turn)


        turn = not leaf_node_turn  # 'turn' meant 'who will do the NEXT move' until here
        outcome = .5 + .5 * is_win(board, mymark) - .5 * is_win(board, -mymark)

        # 4. Backpropagation
        # i DON'T update the child node's values as it is not expanded => lost efficiency?
        while cur_node.parent:
            cur_node.wins += outcome if turn else 1-outcome
            cur_node.games += 1

            turn = not turn
            cur_node = cur_node.parent

        assert cur_node == root
        cur_node.wins += outcome if turn else 1-outcome
        cur_node.games += 1

        if _verbosity > 1: print(root.games, root.wins, turn)


    for c in root.children:
        if _verbosity > 0: print(c.move, c.wins, c.games, c.wins/(c.games+10**-300))

    if explo_vs_explo:
        # when using exploration-vs-exploitation, magically choose the road most travelled
        return sorted(root.children, key=lambda c: c.games)[-1].move
    else:
        # return move with best wins-to-games ratio
        return sorted(root.children, key=lambda c: c.wins/(c.games+10**-300))[-1].move


# TODO: fix outcome thingy here too ...


board = [[0,0,0],[0,0,0],[0,0,0]]

Player = namedtuple('Player', 'name mark getmove')
hmn = Player('Human', +1, lambda: tuple(int(input('Input human move ('+s+'): ')) for s in ('row','col')) )
cpu_mm = Player('COM', +1, lambda: minmax(board, mymark = cpu_mm.mark, _verbosity = 1).moves[0] )
cpu_mcts = Player('COM', -1, lambda: mcts(board, mymark = cpu_mcts.mark, _verbosity = 1) )
cpu_mcts2 = Player('COM', +1, lambda: mcts(board, mymark = cpu_mcts2.mark, _verbosity = 1) )

# for p in [hmn, cpu_mcts] * 5:  # reverse to let cpu go first  # also cpu v cpu possible
# for p in [cpu_mcts, cpu_mm] * 5:  # reverse to let cpu go first  # also cpu v cpu possible
# for p in [cpu_mm, cpu_mcts] * 5:  # reverse to let cpu go first  # also cpu v cpu possible
for p in [cpu_mcts2, cpu_mcts] * 5:  # reverse to let cpu go first  # also cpu v cpu possible
    move = (-1, -1)
    while move not in getlegals(board):
        move = p.getmove()
    board[move[0]][move[1]] = p.mark
    print(move); [print(' '.join({1:'X',-1:'O',0:'.'}[m] for m in r)) for r in board]; print()

    if is_win(board, p.mark):  exit('! ' + p.name + ' won !')
    if not getlegals(board):   exit('! Draw !')








'''

################ Wikipedia ################

- heuristic search for decisions
    - used eg in AlphaGo
            evaluation               -> NNs
            policy = move selection  -> MCTS
    - converges to minimax

- playouts
    - playout = play game out to very end using random moves
    - final game result is used to weigh nodes
        so better nodes will be more likely to be chosen

- MCTS phases
    - Selection   find leaf L (= has no playouts yet)
    - Expansion   create child(ren) C at leaf L
    - Simulation  = playout  from C
    - Backprop    update info along path from L to root R
                    ie wins+1 (losses+1) and games+1 for path nodes
                    -> score 'wins/games' at each node

- basic / 'Pure' MCTS
    - eval each legal move with same # of playouts

    -> better: assign more playouts to likely-winning moves
        based on previous playouts

        => move with MOST SIMULATIONS made is played !
            NOT best ratio, but highest denominator

- balancing exploration and exploitation
    EXPLORATION of few-playouts moves  vs
    EXPLOITATION of deep variants of promising moves

    => [UCT (Upper Confidence Bound 1 applied to trees)]
            wi / ni  +  c rt(ln Ni / ni)
    EXPLOIT -------     ---------------- EXPLORATION

+/-
    + in pure form:  no need for evaluation function
    + good results in high-branching-factor games
        bc tree grows asymmetrically
    + anytime algorithm
    + parallelization possible

    - can overlook it if only a single path leads to a loss

- light vs heavy playouts
    Light:  random moves
    Heavy:  use heuristics in move choice
        eg expert evaluation / domain knowledge  (what AlphaGo used ?)
        ->  significant improvements possible, fewer its needed

- RAVE (Rapid Action Value Estimation)
    - idea: allow switching move order
    - after a playout, update not just along the path,
        but every node on the tree where moves of the playout are found
            -> weigh 'true' with move-switched playouts

'''
