from ast import literal_eval as make_tuple
from copy import deepcopy as copy
from collections import namedtuple, Counter
import datetime
import functools
import h5py
import itertools
import json
import math
import os
import os.path
import queue
import random
import sys
import time
import socket

import numpy

import PIL.Image
import PIL.ImageDraw

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import torchvision.transforms
import cv2

import torch.multiprocessing as mp

mp.set_start_method('spawn', force=True)

# local imports

from powell import *
# import alpha_csg_model

# TABLE OF ABBREVIATIONS
# nn        -- Neural network
# viz_sim   -- Visual Similarity


################### Neural Nets ###################

# !!! This section of code is adapted from
# https://github.com/Hippogriff/CSGNet/blob/b45270d83ca281c1bdeb913e392ffe7ee884b994/src/Models/models.py

import torch
import torch.nn.functional as F
import torch.nn as nn
import numpy as np
from torch.autograd.variable import Variable

class Encoder(nn.Module):
    # def __init__(self, dropout=0.2):
    def __init__(self, size, dropout=0):
        """
        Encoder for 2D CSGNet.
        :param dropout: dropout
        output size = [input_width/8, input_height/8]
        """
        super(Encoder, self).__init__()
        self.p = dropout
        self.conv1 = nn.Conv2d(1, 8, 3, padding=(1, 1))
        self.conv2 = nn.Conv2d(8, 16, 3, padding=(1, 1))
        self.conv3 = nn.Conv2d(16, 32, 3, padding=(1, 1))
        self.drop = nn.Dropout(dropout)
        self.size = size

    def encode(self, x):
        x = F.max_pool2d(self.drop(F.relu(self.conv1(x))), (2, 2))
        x = F.max_pool2d(self.drop(F.relu(self.conv2(x))), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


class ImitateJoint(nn.Module):
    def __init__(self,
                 hd_sz,  # 2048
                 input_size,  # 32*(IMAGE_SIZE//8)**2
                 encoder,
                 mode,
                 num_layers=1,
                 time_steps=3,
                 num_draws=None,
                 canvas_shape=[64, 64],
                 dropout=0):
                 # dropout=0.5):
        """
        Defines RNN structure that takes features encoded by CNN and produces program
        instructions at every time step.
        :param num_draws: Total number of tokens present in the dataset or total number of operations to be predicted + a stop symbol = 400
        :param canvas_shape: canvas shape
        :param dropout: dropout
        :param hd_sz: rnn hidden size
        :param input_size: input_size (CNN feature size) to rnn
        :param encoder: Feature extractor network object
        :param mode: Mode of training, RNN, BDRNN or something else
        :param num_layers: Number of layers to rnn
        :param time_steps: max length of program
        """
        super(ImitateJoint, self).__init__()
        self.hd_sz = hd_sz
        self.in_sz = input_size
        self.num_layers = num_layers
        self.encoder = encoder
        self.time_steps = time_steps
        self.mode = mode
        self.canvas_shape = canvas_shape
        self.num_draws = num_draws

        # added by jan -- i have no idea why this is missing or what this is supposed to be
        self.epsilon = 1

        # Dense layer to project input ops(labels) to input of rnn
        self.input_op_sz = 128  # ???
        self.dense_input_op = nn.Linear(
            in_features=self.num_draws + 1, out_features=self.input_op_sz)

        self.rnn = nn.GRU(
            input_size=self.in_sz + self.input_op_sz,
            hidden_size=self.hd_sz,
            num_layers=self.num_layers,
            batch_first=False)

        # adapt logsoftmax and softmax for different versions of pytorch
        self.pytorch_version = torch.__version__[2]
        if self.pytorch_version.startswith('0.1'):
            self.logsoftmax = nn.LogSoftmax()
            self.softmax = nn.Softmax()
        else:
            self.logsoftmax = nn.LogSoftmax(1)
            self.softmax = nn.Softmax(1)

        self.dense_fc_1 = nn.Linear(
            in_features=self.hd_sz, out_features=self.hd_sz)
        self.dense_output = nn.Linear(
            in_features=self.hd_sz, out_features=(self.num_draws + 1))
        self.drop = nn.Dropout(dropout)
        self.sigmoid = nn.Sigmoid()
        self.relu = nn.ReLU()

    def forward(self, x: List):
        """
        Forward pass for  all architecture
        :param x: Has different meaning with different mode of training
        :return:
        """
        if self.mode == 1:
            # jan: supervised training programs available
            #   -> according to paper, inputs (image, program, len(program))
            #
            # => why does mode==2 (RL) take program as input?
            # => why is data.size() related to program_len?
            #       They just (uselessly) keep the whole stack of intermediate images.
            #       They all get thrown away at the 'data[-1' line
            #       => 'class SimulateStack'
            '''
            Variable length training. This mode runs for one
            more than the length of program for producing stop symbol. Note
            that there is no padding as is done in traditional RNN for
            variable length programs. This is done mainly because of computational
            efficiency of forward pass, that is, each batch contains only
            programs of same length and losses from all batches of
            different time-lengths are combined to compute gradient and
            update in the network. This ensures that every update of the
            network has equal contribution coming from programs of different lengths.
            Training is done using the script train_synthetic.py
            '''
            data, input_op, program_len = x

            # assert data.size()[0] == program_len + 1, "Incorrect stack size!!"
            batch_size = data.size()[1]
            h = Variable(torch.zeros(1, batch_size, self.hd_sz)).cuda()
            x_f = self.encoder.encode(data[-1, :, 0:1, :, :])
            x_f = x_f.view(1, batch_size, self.in_sz)
            outputs = []
            for timestep in range(program_len + 1):
                # X_f is always input to the RNN at every time step
                # along with previous predicted label
                input_op_rnn = self.relu(self.dense_input_op(input_op[timestep, :]))
                input_op_rnn = input_op_rnn.view(1, batch_size, self.input_op_sz)
                input = torch.cat((self.drop(x_f), input_op_rnn), 2)  # ??? DID THEY JUST FORGET THE DROPOUT IN THE RL CASE ?
                h, _ = self.rnn(input, h)
                hd = self.relu(self.dense_fc_1(self.drop(h[0])))
                output = self.logsoftmax(self.dense_output(self.drop(hd)))
                outputs.append(output)
            return torch.stack(tuple(o[0] for o in outputs))

        elif self.mode == 2:
            # jan: TODO: what is the input program (input_op) here?
            #   during training, it becomes the program of the last prediction (acc to paper)
            #   but what the paper says is NOT what this code does ???
            # jan: code: for each OPERATION in the program we sample an instruction?
            # jan: won't this be shifted by 1 compared to the supervised case?
            #      input at t =      0  1  2  3
            #       supervised:     a0 a1 a2 a3    actual program a0 a1..
            #               RL:      ? p0 p1 p2    predicted program p0 p1..
            # oh it says the first input is the start-of-sequence symbol
            #      input at t =      0  1  2  3
            #       supervised:  START a0 a1 a2    actual program a0 a1..
            #               RL:  START p0 p1 p2    predicted program p0 p1..
            # but why is it shifted in the opposite direction in the code here ???
            #
            # input_op is 1-el array with just the start symbol
            '''Train variable length RL'''
            # program length in this case is the maximum time step that RNN runs
            data, input_op, program_len = x
            batch_size = data.size()[1]
            h = Variable(torch.zeros(1, batch_size, self.hd_sz)).cuda()
            x_f = self.encoder.encode(data[-1, :, 0:1, :, :])
            x_f = x_f.view(1, batch_size, self.in_sz)
            outputs = []
            samples = []
            curr_input_op = input_op[0]

            # TODO: why is this 1 less?  cos no need to predict start symbol?
            # for timestep in range(program_len):
            for timestep in range(program_len + 1):
                # X_f is the input to the RNN at every time step along with previous
                # predicted label
                input_op_rnn = self.relu(self.dense_input_op(curr_input_op))
                input_op_rnn = input_op_rnn.view(1, batch_size, self.input_op_sz)
                input = torch.cat((x_f, input_op_rnn), 2)
                h, _ = self.rnn(input, h)
                hd = self.relu(self.dense_fc_1(self.drop(h[0])))
                dense_output = self.dense_output(self.drop(hd))
                output = self.logsoftmax(dense_output)
                # output for loss, these are log-probabs
                outputs.append(output)

                # TODO XXX: dont they make this numerically unstable by using softmax ?
                output_probs = self.softmax(dense_output)
                # Get samples from output probabs based on epsilon greedy way
                # Epsilon will be reduced to 0 gradually following some schedule
                if np.random.rand() < self.epsilon:
                    # This is during training
                    sample = torch.multinomial(output_probs, 1)
                else:
                    # This is during testing
                    if self.pytorch_version.startswith('0.1'):
                        sample = torch.max(output_probs, 1)[1]
                    else:
                        sample = torch.max(output_probs, 1)[1].view(batch_size, 1)

                # Stopping the gradient to flow backward from samples
                # jan: ??? if you dont call backward() on it there is no gradient flow ?
                #   -> this is used in 'arr' so maybe through that ?
                sample = sample.detach()
                samples.append(sample)

                # Create next input to the RNN from the sampled instructions
                arr = Variable(torch.zeros(batch_size, self.num_draws + 1).scatter_(1, sample.data.cpu(), 1.0)).cuda()
                arr = arr.detach()
                curr_input_op = arr

            # TODO: what are 'samples' ?
            # return [outputs, samples]    # [with_gradients, without_gradients] ?
            return torch.stack(tuple(o[0] for o in outputs))
        else:
            assert False, "Invalid mode!!"



class ImitateJointStep(nn.Module):
    def __init__(self,
                 hd_sz,  # 2048
                 input_size,  # 32*(IMAGE_SIZE//8)**2
                 encoder,
                 mode,
                 num_layers=1,
                 time_steps=3,
                 num_draws=None,
                 canvas_shape=[64, 64],
                 dropout=0.5,  # TODO XXX they used .2 in their config_synthetic.yml
                 hyper=dict()):
        """
        Defines RNN structure that takes features encoded by CNN and produces program
        instructions at every time step.
        :param num_draws: Total number of tokens present in the dataset or total number of operations to be predicted + a stop symbol = 400
        :param canvas_shape: canvas shape
        :param dropout: dropout
        :param hd_sz: rnn hidden size
        :param input_size: input_size (CNN feature size) to rnn
        :param encoder: Feature extractor network object
        :param mode: Mode of training, RNN, BDRNN or something else
        :param num_layers: Number of layers to rnn
        :param time_steps: max length of program
        """
        super().__init__()
        self.hd_sz = hd_sz
        self.in_sz = input_size
        self.num_layers = num_layers
        self.encoder = encoder
        self.time_steps = time_steps
        self.mode = mode
        self.canvas_shape = canvas_shape
        self.num_draws = num_draws

        # Dense layer to project input ops(labels) to input of rnn
        self.input_op_sz = 128  # ???
        self.dense_input_op = nn.Linear(
            in_features=self.num_draws + 1, out_features=self.input_op_sz)

        self.rnn = nn.GRU(
            input_size=self.in_sz + self.input_op_sz,
            hidden_size=self.hd_sz,
            num_layers=self.num_layers,
            batch_first=False)

        # TODO XXX delme
        self.rnn_bypass = nn.Linear(self.in_sz + self.input_op_sz, self.hd_sz)

        # adapt logsoftmax and softmax for different versions of pytorch
        self.pytorch_version = torch.__version__[2]
        if self.pytorch_version.startswith('0.1'):
            self.logsoftmax = nn.LogSoftmax()
            self.softmax = nn.Softmax()
        else:
            self.logsoftmax = nn.LogSoftmax(1)
            self.softmax = nn.Softmax(1)

        self.dense_fc_1 = nn.Linear(
            in_features=self.hd_sz, out_features=self.hd_sz)
        self.dense_output_p = nn.Linear(
            in_features=self.hd_sz, out_features=(self.num_draws + 1))
        self.dense_output_v = nn.Linear(
            in_features=self.hd_sz, out_features=1)
        #     in_features=self.hd_sz, out_features=2048)
        # self.dense_output_v2 = nn.Linear(
        #     in_features=2048, out_features=256)
        # self.dense_output_v3 = nn.Linear(
        #     in_features=256, out_features=1)
        self.drop = nn.Dropout(dropout)
        self.sigmoid = nn.Sigmoid()
        self.relu = nn.ReLU()

        self.gru_architecture = hyper['gru_architecture']

        self.initial_Q = hyper['initial_Q']

        # net health
        self.cnn_output_running_zeros = 0
        self.rnn_input_running_zeros = 0
        self.rnn_hd_running_zeros = 0
        self.running_train_steps = 0

        self.rolling_avg_reward = 0


    def start(self, image):
        self.batch_size = image.shape[0]
        # TODO: should we use random instead of 0 initialization
        # okay, they define their batch dimension a bit weirdly, it's not the first dimension for them
        hidden_state = torch.zeros(1, self.batch_size, self.hd_sz).cuda()

        # i have no idea why they pointlessly and confusingly shift the batch dimension around...
        self.cnn_output = self.encoder.encode(image).view(1, self.batch_size, self.in_sz)

        if self.training:
            # this is simply to monitor net health, i.e. keep the "dying ReLU problem" in check
            self.cnn_output_running_zeros += sum(torch.sum(self.cnn_output[0][i] == 0) for i in range(self.batch_size))

        return hidden_state


    # 'sample' in one-hot encoding
    def step(self, hidden_state, sample):
        # print(self.cnn_output.shape, hidden_state.shape, sample.shape)
        self.batch_size = hidden_state.shape[1]

        curr_input_op = sample.detach()
        # curr_input_op = sample

        # print(curr_input_op)
        input_op_rnn = self.relu(self.dense_input_op(curr_input_op))
        # input_op_rnn = torch.zeros(self.input_op_sz).cuda()
        # input_op_rnn[:len(curr_input_op)] = curr_input_op
        # print(input_op_rnn)
        input_op_rnn = input_op_rnn.view(1, self.batch_size, self.input_op_sz)
        # print(input_op_rnn)
        # torch.set_printoptions(profile='full')
        # print(self.cnn_output)
        # print(input_op_rnn)
        # input = torch.cat((self.drop(self.cnn_output), input_op_rnn), 2)
        input = torch.cat((self.cnn_output, input_op_rnn), 2)
        # input = (input > 9999).float()  # zeros it out
        # print(input)

        # if random.random() < .01:
        #     print(self.training)
        #     print(self.cnn_output)
        #     print(input_op_rnn)

         # |  Inputs: input, h_0
         # |  Outputs: output, h_n
         # why do they use output as hidden state here instead of the hidden state? =//
         # TODO XXX change back
         # actually i think they calculate the same thing as we only have 1 timestep
        # _, hidden_state = self.rnn(input, hidden_state)  # *CORRECT* version
        hidden_state, _ = self.rnn(input, hidden_state)  # *CSGNet* version
        # hidden_state = self.rnn(input)  # Linear hack

        # TODO XXX delme
        if self.gru_architecture == 'gru_plus_linear':
            hidden_state2 = self.rnn_bypass(input)
            hidden_state = hidden_state + hidden_state2

        # hidden_state = (hidden_state > 9999).float()

        hd = self.relu(self.dense_fc_1(self.drop(hidden_state[0])))
        drop_hd = self.drop(hd)
        self.drop_hd = drop_hd

        # drop_hd = (drop_hd > 9999).float().clone().detach()

        if self.training:
            # this is simply to monitor net health, i.e. keep the "dying ReLU problem" in check
            self.rnn_input_running_zeros += torch.sum(input_op_rnn[0][0] == 0)
            self.rnn_hd_running_zeros += torch.sum(drop_hd[0] == 0)
            self.running_train_steps += 1

        dense_output1 = self.dense_output_p(drop_hd)
        output_probs_log = self.logsoftmax(dense_output1)  # LogSoftmax has better numerics than softmax

        dense_output2 = self.dense_output_v(drop_hd)

        # dense_output3 = self.dense_output_v2(dense_output2)
        # dense_output4 = self.dense_output_v3(dense_output3)

        # dense_output2 = (dense_output2 > 9999).float()

        # output_value = dense_output4
        output_value = dense_output2
        # output_value = torch.tanh(dense_output2)

        return hidden_state, output_probs_log, output_value + self.initial_Q




def load_or_new_rnn_net(hyper):

    encoder = Encoder(hyper['image_size'], dropout=hyper['dropout'][0]).cuda()
    max_program_length = hyper['max_program_length']

    num_draws = hyper['num_actions'] - 1
    # input_size = 32*4**2  # 32*(hyper['image_size']//8)**2
    input_size = 32*(hyper['image_size']//8)**2
    decoder = ImitateJointStep(hd_sz=2048, input_size=input_size, encoder=encoder, mode=None, num_draws=num_draws, time_steps=max_program_length, dropout=hyper['dropout'][1], hyper=hyper).cuda()

    if hyper['net_path'] and hyper['net_path'] != '(new)':
        decoder.load_state_dict(torch.load(hyper['net_path'], map_location='cuda'))

    decoder = decoder.share_memory()

    return decoder



# TODO XXX test this

# Function 'chamfer' taken from
# https://github.com/Hippogriff/CSGNet/blob/b45270d83ca281c1bdeb913e392ffe7ee884b994/src/utils/train_utils.py
def chamfer(images1, images2):
    """
    Chamfer distance on a minibatch, pairwise.
    :param images1: Bool Images of size (N, 64, 64). With background as zeros
    and forground as ones
    :param images2: Bool Images of size (N, 64, 64). With background as zeros
    and forground as ones
    :return: pairwise chamfer distance
    """
    # print('call to chamfer with', images1, images2, images1.shape)

    # Convert in the opencv data format
    try:
        images1 = numpy.array(images1.cpu()).astype(np.uint8)
        images2 = numpy.array(images2.cpu()).astype(np.uint8)
    except:
        pass

    images1 = images1.astype(np.uint8)
    images2 = images2.astype(np.uint8)

    images1 = images1 * 255
    images2 = images2 * 255

    N = images1.shape[0]
    size = images1.shape[-1]

    D1 = np.zeros((N, size, size))
    E1 = np.zeros((N, size, size))

    D2 = np.zeros((N, size, size))
    E2 = np.zeros((N, size, size))

    summ1 = np.sum(images1, (1, 2))
    summ2 = np.sum(images2, (1, 2))

    # sum of completely filled image pixels
    filled_value = int(255 * size**2)
    defaulter_list = []
    for i in range(N):
        img1 = images1[i, :, :]
        img2 = images2[i, :, :]

        if (summ1[i] == 0) or (summ2[i] == 0) or (summ1[i] == filled_value) or (summ2[i] == filled_value):
            # just to check whether any image is blank or completely filled
            defaulter_list.append(i)
            continue
        edges1 = cv2.Canny(img1, 1, 3)
        sum_edges = np.sum(edges1)
        if (sum_edges == 0) or (sum_edges == size**2):
            defaulter_list.append(i)
            continue
        dst1 = cv2.distanceTransform(
            ~edges1, distanceType=cv2.DIST_L2, maskSize=3)

        edges2 = cv2.Canny(img2, 1, 3)
        sum_edges = np.sum(edges2)
        if (sum_edges == 0) or (sum_edges == size**2):
            defaulter_list.append(i)
            continue

        dst2 = cv2.distanceTransform(
            ~edges2, distanceType=cv2.DIST_L2, maskSize=3)
        D1[i, :, :] = dst1
        D2[i, :, :] = dst2
        E1[i, :, :] = edges1
        E2[i, :, :] = edges2

    distances = np.sum(D1 * E2, (1, 2)) / (
        np.sum(E2, (1, 2)) + 1) + np.sum(D2 * E1, (1, 2)) / (np.sum(E1, (1, 2)) + 1)   # jan: why +1 ??
    # # TODO make it simpler
    # jan: CSGNet reports SL values that are about *2 my values, I think they might have forgotten this line...
    distances = distances / 2.0
    # print(distances)
    # This is a fixed penalty for wrong programs
    distances[defaulter_list] = 16
    return distances








################### CSG ###################



# # TODO: adapt this to be >= 2*image_size, i.e. pre-compute this in the main loop, not here
# SIZE = 130
# CENTER = SIZE//2
# RAD = 3
# sdf_map_circle = torch.zeros(SIZE, SIZE).cuda()
# x_range = torch.arange(SIZE).float()
# for y in range(SIZE):
#     sdf_map_circle[y] = torch.clamp( ((x_range - CENTER)**2 + (y - CENTER)**2).sqrt() - RAD , min=0)

# image_circle = (sdf_map_circle > 0).float()


def kronecker_delta_cpu(n, i):
    return (torch.arange(n) == i).float()

def kronecker_delta(n, i):
    return (torch.arange(n).cuda() == i).float()


# def print_two_images(image1, image2=None, downsample_factor=2, headline='    Target image:                       Predicted image:'):
def print_two_images(image1, image2=None, downsample_factor=1, headline='    Target image:                       Predicted image:'):
    print(headline)
    clamp = lambda x: int( min(9, max(0, x)) )
    image1 = image1[::downsample_factor, ::downsample_factor]
    if image2 is not None:
        image2 = image2[::downsample_factor, ::downsample_factor]
        for line, line2 in zip(image1, image2):
            print(''.join(str(clamp(i)) for i in line) + '    ' + ''.join(str(clamp(i)) for i in line2))
    else:
        for line in image1:
            print(''.join(str(clamp(i)) for i in line))

def print_sdf(program1):
    print_two_images(program1.images[-1], program1.sdfs[-1], headline='           Image:                                   SDF:')


# - Program defined in Postfix form
#       eg  c(x,y,r)s(x,y,r)+t(x,y,r)*c(x,y,r)-
#       shapes:    c circle  s square  t triangle
#       operations:   + union  * intersection  - difference
# - currently (May 7) we just do union on a bunch of circles

# TODO: partial program to string repr ?
# return self.type + '(' + str(self.x) + ',' + str(self.y) + ',' + str(self.r) + ')'

def basic_csg_image_old(hyper, x, y, r=3, shape='circle', sdf=True):
    # x, y = 2*r*(x+1), 2*r*(y+1)
    x, y = round(x), round(y)
    assert shape == 'circle'
    sdf   = sdf_map_circle[CENTER-y:CENTER-y+hyper['image_size'], CENTER-x:CENTER-x+hyper['image_size']]
    image =   image_circle[CENTER-y:CENTER-y+hyper['image_size'], CENTER-x:CENTER-x+hyper['image_size']]
    return (image, sdf)


# TODO XXX pre-computed for efficiency
# TODO XXX /
def basic_csg_image(hyper, x, y, r=3, shape='circle', sdf=True):
    x, y = round(x), round(y)
    assert hyper['image_size'] == 64

    if shape == 'circle':
        sdf_map_circle = torch.zeros(SIZE, SIZE).cuda()

        x_range = torch.arange(SIZE).float()
        for yy in range(SIZE):
            sdf_map_circle[yy] = torch.clamp( ((x_range - CENTER)**2 + (yy - CENTER)**2).sqrt() - r , min=0)

        image_circle = (sdf_map_circle > 0).float()

        sdf   = sdf_map_circle[CENTER-y:CENTER-y+hyper['image_size'], CENTER-x:CENTER-x+hyper['image_size']]
        image =   image_circle[CENTER-y:CENTER-y+hyper['image_size'], CENTER-x:CENTER-x+hyper['image_size']]

        return (image, sdf)


    # TODO XXX triangle
    if shape == 'square' or shape == 'triangle':
        sdf_map_square = torch.zeros(SIZE, SIZE).cuda()

        x_range = torch.arange(SIZE).float()
        for yy in range(SIZE):
            sdf_map_square[yy] = (torch.clamp(torch.abs(x_range - CENTER) - r, min=0)**2 + max(abs(yy - CENTER) - r, 0)**2).sqrt()

        image_square = (sdf_map_square > 0).float()

        sdf   = sdf_map_square[CENTER-y:CENTER-y+hyper['image_size'], CENTER-x:CENTER-x+hyper['image_size']]
        image =   image_square[CENTER-y:CENTER-y+hyper['image_size'], CENTER-x:CENTER-x+hyper['image_size']]

        return (image, sdf)


    if shape == 'triangle':
        raise NotImplementedError()


    exit("Uh oh, bad shape in 'basic_csg_image'")



def basic_csg_image_from_index_old(hyper, index):
    y_index, x_index = index//hyper['num_discrete_positions_xy'], index%hyper['num_discrete_positions_xy']
    y = hyper['image_size'] * (y_index + 1) / (hyper['num_discrete_positions_xy'] + 1)
    x = hyper['image_size'] * (x_index + 1) / (hyper['num_discrete_positions_xy'] + 1)
    return basic_csg_image(hyper, x, y, 3)


def basic_csg_image_from_index_mine(hyper, index):
    rest, shape_index = index//hyper['shape_actions'][3], index%hyper['shape_actions'][3]
    rest, size_index  =  rest//hyper['shape_actions'][2],  rest%hyper['shape_actions'][2]
    y_index, x_index  =  rest//hyper['shape_actions'][1],  rest%hyper['shape_actions'][1]
    y = hyper['image_size'] * (y_index + 1) / (hyper['shape_actions'][1] + 1)
    x = hyper['image_size'] * (x_index + 1) / (hyper['shape_actions'][0] + 1)
    # size = 4 * (size_index + 2)
    size = 2 * (size_index + 2)
    shape = ['circle', 'square', 'triangle'][shape_index]
    print(hyper, x, y, size, shape)
    return basic_csg_image(hyper, x, y, size, shape)


def basic_csg_image_from_index(hyper, index):
    rest, shape_index = index//hyper['shape_actions'][3], index%hyper['shape_actions'][3]
    rest, size_index  =  rest//hyper['shape_actions'][2],  rest%hyper['shape_actions'][2]
    y_index, x_index  =  rest//hyper['shape_actions'][1],  rest%hyper['shape_actions'][1]
    y = hyper['image_size'] * (y_index + 1) / (hyper['shape_actions'][1] + 1)
    x = hyper['image_size'] * (x_index + 1) / (hyper['shape_actions'][0] + 1)
    size = 4 * (size_index + 2)
    # shape = ['circle', 'square', 'triangle'][shape_index]
    shape_fun = [Draw().draw_circle, Draw().draw_square, Draw().draw_triangle][shape_index]
    ret = shape_fun([x, y], size)
    # ret = torch.from_numpy(ret).float().cuda()
    ret = torch.from_numpy(np.array(ret, dtype=np.float32))
    return (ret, ret)  # SDF unused so fake it



def copy_partial_program(partial_program):
    copied_partial_program = copy(partial_program)
    copied_partial_program.images = copy(partial_program.images)  # TODO: XXX: maybe just point back to parent? #efficiency
    copied_partial_program.sdfs = copy(partial_program.sdfs)
    return copied_partial_program

def new_blank_image(hyper):
    # return torch.ones(hyper['image_size'], hyper['image_size']).cuda()
    return torch.ones(hyper['image_size'], hyper['image_size'])

def new_blank_sdf(hyper):
    # return torch.full((hyper['image_size'], hyper['image_size']), hyper['image_size']).cuda()
    return torch.full((hyper['image_size'], hyper['image_size']), hyper['image_size'])

# TODO XXX
# TODO: return if valid program
def program_add_instruction_SLOW(hyper, partial_program, pos, i):
    # stop symbol
    if i == hyper['num_actions'] - 1 or i == -1:

        if len(partial_program.images) == 0:
            partial_program.images  = [new_blank_image(hyper)]
            partial_program.sdfs    = [new_blank_sdf(hyper)]

        return len(partial_program.images) == 1

    elif hyper['moveset'] == 'union_of_raster_circles':
        partial_program[pos][i] = 1
        # TODO: 16 is just a no-op, not a stop symbol, rn  (=> score_nn)
        # TODO: use torch.min(a, 0)
        other_image, other_sdf = basic_csg_image_from_index(hyper, i)  # TODO: this can be a more complex shape later
        partial_program.images = [torch.min(partial_program.images[0], other_image)]  # TOOD: maybe dont store this but calc it from SDF !
        partial_program.sdfs   = [torch.min(partial_program.sdfs  [0], other_sdf  )]

    elif hyper['moveset'].startswith('postfix'):
        partial_program[pos][i] = 1
        if i < hyper['num_actions'] - 4:
            other_image, other_sdf = basic_csg_image_from_index(hyper, i)
            partial_program.images.append(other_image)  # TOOD: maybe dont store this but calc it from SDF !
            partial_program.sdfs.append(other_sdf)
        else:
            combined_image = combined_sdf = None
            if len(partial_program.images) <= 1:
                # this is just a no-op for now...
                return False

            fun = [torch.min, torch.max, lambda a, b: torch.max(a, hyper['image_size'] - b)][i - hyper['num_actions'] + 4]

            combined_image = fun(partial_program.images[-2], partial_program.images[-1])
            combined_sdf   = fun(partial_program.sdfs  [-2], partial_program.sdfs  [-1])

            partial_program.images = partial_program.images[:-2] + [combined_image]
            partial_program.images = partial_program.sdfs  [:-2] + [combined_sdf]

    else:
        exit("Bad hyper['moveset'] in 'program_add_instruction'")


def program_add_instruction(hyper, partial_program, pos, i):

    partial_program[pos][i] = 1

    if i == hyper['num_actions'] - 1 or i == -1:
        return

    if i < hyper['num_actions'] - 4:
        other_image, other_sdf = basic_csg_image_from_index(hyper, i)
        partial_program.images.append(other_image)  # TOOD: maybe dont store this but calc it from SDF !

    else:
        if len(partial_program.images) <= 1:  # could happen during @score_nn
            return

        #        union    intersect           difference
        fun = [torch.max, torch.min, lambda a, b: torch.min(a, 1 - b)][i - hyper['num_actions'] + 4]
        combined_image = fun(partial_program.images[-2], partial_program.images[-1])

        partial_program.images = partial_program.images[:-2] + [combined_image]


def draws_to_partial_program(draws, hyper):

    partial_program = new_partial_program(hyper, hyper['max_program_length'] + 1)

    for i in range(len(draws)):
        program_add_instruction(hyper, partial_program, i, draws[i])

    return partial_program


def new_partial_program(hyper, program_len):
    # partial_program         = torch.zeros(program_len, hyper['num_actions']).cuda()
    partial_program         = torch.zeros(program_len, hyper['num_actions'])
    if hyper['moveset'] == 'union_of_raster_circles':
        partial_program.images  = [new_blank_image(hyper)]
        partial_program.sdfs    = [new_blank_sdf(hyper)]
    else:
        partial_program.images  = []
        partial_program.sdfs    = []
    return partial_program


def jitter_image(target, hyper):

    if hyper['jitter_images']:
        x_jitter = 3 + random.randint(-3, 3)
        y_jitter = 3 + random.randint(-3, 3)
        scaling = random.randint(-2, 2)
        scale_shift = 1 + scaling//2
        a1 = target.images[-1]
        a2 = torchvision.transforms.ToPILImage()(a1.unsqueeze(0))
        # a3 = torchvision.transforms.functional.resize(a2, 64 + random.randint())
        # a3 = torchvision.transforms.functional.resized_crop(a2, scale_shift, scale_shift, 64 + scaling, 64 + scaling, 64)
        a3 = torchvision.transforms.functional.resize(a2, hyper['image_size'] + scaling)
        a4 = torchvision.transforms.Pad(4)(a3)
        a5 = torchvision.transforms.ToTensor()(a4)[0]
        a6 = a5[scale_shift+x_jitter:hyper['image_size']+scale_shift+x_jitter,
                scale_shift+y_jitter:hyper['image_size']+scale_shift+y_jitter]
        target.images[-1] = a6
        # print(x_jitter, y_jitter, scaling, scale_shift)
        # print_two_images(a1, a6, 1)


# TODO rework example generator into the new system. or maybe delete altogether
def next_train_program(hyper, train_set=None, index=None):

    program_one_hot_encoding = new_partial_program(hyper, hyper['max_program_length'] + 1)   # TODO: modify
    program_class_encoding = []
    stop_symbol = hyper['num_actions'] - 1

    # Options enumerated:   0 X0Y0  1 X0Y1  2 X1Y0  3 X1Y1  4 +  5 *  6 \  6 STOP
    # TODO: allow overlap by using bigger circles
    # TODO: in MCTS, zero probabilities based on stack size

    if hyper['dataset_path'].endswith('.h5'):

        # program_one_hot_encoding = object()   # dummy
        program_one_hot_encoding.images = [train_set[index % len(train_set)]]

    elif hyper['dataset_path'].endswith('.txt'):

        program_class_encoding = copy(train_set[index % len(train_set)])

        program_one_hot_encoding = draws_to_partial_program(program_class_encoding, hyper)
        jitter_image(program_one_hot_encoding, hyper)

    else:
        raise NotImplementedError()

    program_class_encoding.append(stop_symbol)
    program_add_instruction(hyper, program_one_hot_encoding, -1, -1)

    program_one_hot_encoding.class_encoding = program_class_encoding

    # little trick to generate samples for the value net
    # during supervised training we would only get the ground truth program
    #   which would lead to a value of 1.
    #   however if we take 2 images and their programs used to generate them
    #   and interpret the programs as attempts to generate the OTHER image
    #   => swap around 2/3 of images to generate images with values < 1
    #   (assumes random data set)
    if hyper['training_mode'] == 'supervised' and hyper['value_loss_weight'] != 0 and index % 3:
        swap_index = index + (1 if index % 3 == 1 else -1)
        program_one_hot_encoding.class_encoding = copy(train_set[swap_index % len(train_set)]) + [stop_symbol]

    return program_one_hot_encoding


def image_to_image_distance(program1, program2, hyper):

    # invalid postfix program
    # 16 is the maximum distance as defined by CSGNet
    if len(program1.images) != 1 or len(program2.images) != 1:
        return 16

    if torch.all(program1.images[-1] == program2.images[-1]):
        return 0

    if hyper['viz_sim_mode'] == 'euclidean_domain_specific':
        if hyper['moveset'].startswith('postfix_raster_circles'):
            dist = torch.dist(program1.images[-1], program2.images[-1], p=1)
            max_shapes = (hyper['max_program_length'] + 1) // 2
            dist = dist / (2 * 29 * max_shapes)
            return dist.item()

    if hyper['viz_sim_mode'] == 'euclidean':
        # Euclidean distance (Frobenius norm of difference) of pixel values
        dist = torch.dist(program1.images[-1], program2.images[-1]) / hyper['image_size']
        return dist.item()

    if hyper['viz_sim_mode'] == 'sdf':
        # assuming program1 is target and program2 is prediction
        dist = torch.Tensor([0])
        dist += ((1 - program2.images[-1]) * program1.sdfs[-1]).sum()
        dist += ((1 - program1.images[-1]) * program2.sdfs[-1]).sum()
        dist /= hyper['image_size']**3
        return dist.item()

    if hyper['viz_sim_mode'] == 'sdf_dist':
        # Euclidean distance (Frobenius norm of difference) of the SDFs (Signed Distance Functions)
        dist = torch.dist(program1.sdfs[-1], program2.sdfs[-1]) / hyper['image_size']**2
        return dist.item()
        # return dist.item()

    if hyper['viz_sim_mode'] == 'chamfer':
        # TODO
        # https://www.google.com/search?q=calculate+chamfer+distance
        # https://www.cnczone.com/forums/general-metalwork-discussion/130314-chamfer-calculation-2-a.html
        # http://www.eurecom.fr/en/publication/163/download/mm-marcst-990301.pdf

        # step 1: find points on EDGE of each shape  (ie not just corner)
        #   => is this just the pixels on the edge ?  => edge detection!
        #   -> how do we find correspondences ???

        cd = chamfer(program1.images[-1].unsqueeze(0), program2.images[-1].unsqueeze(0))
        return cd.item()


# ideally in range -1..1 for MCTS
def viz_sim_score(program1, program2, hyper):
    return  1 - 2 * image_to_image_distance(program1, program2, hyper) * hyper['viz_sim_scale']

def viz_sim_to_dist(viz_sim, hyper):
    # y = 1 - 2 x vzs
    # 2 x vzs = 1 - y
    # x = (1 - y) / 2 vzs
    return  (1 - viz_sim) / (2 * hyper['viz_sim_scale'])




# same as get_draws_beam_search with k=1
# i only kept this method because this one has good debug printing for the greedy case
def get_draws_greedy(target, net=None, hyper={}):
    partial_program = new_partial_program(hyper, hyper['max_program_length'])
    hidden_state = net.start(target.images[-1].unsqueeze(0).unsqueeze(0).cuda())
    draw = -1
    draws = []

    for i in range(hyper['max_program_length']):
        hidden_state, probs_log, value = net.step(hidden_state, kronecker_delta(hyper['num_actions'], draw))
        draw = torch.argmax(probs_log).item()
        draws.append(draw)
        program_add_instruction(hyper, partial_program, i, draw)

        if hyper['verbosity'] >= 2:
            probs_log_val, probs_log_ind = torch.topk(probs_log[0], 5)
            print('        NProb Argmax')
            for j in range(len(probs_log_ind)):
                print('#{:4d}:  {:5.3f}  {:.7}'.format(probs_log_ind[j], float(math.e**probs_log_val[j]), ('* {:5.2f}'.format(value.item())) * bool(draw == probs_log_ind[j]) ))

        if draw == hyper['num_actions'] - 1:
            break  # stop symbol

    return draws


def get_draws_beam_search(target, net=None, hyper={}):
    hidden_state = net.start(target.images[-1].unsqueeze(0).unsqueeze(0).cuda())

    sample = torch.stack([kronecker_delta(hyper['num_actions'], -1) for b in range(1)])
    top_k = [ (1, hidden_state, [-1]) ]
    k = hyper['beam_search_k']

    for i in range(hyper['max_program_length']):
        net.cnn_output = torch.stack([ net.cnn_output[0][0] for _ in range(len(top_k)) ]).unsqueeze(0)

        hidden_state, probs_log, value = net.step(hidden_state, sample)
        new_top_k = []

        # TODO: how to handle completed sequences ?
        for j in range(len(top_k)):
            old = top_k[j]
            if old[2][-1] == hyper['num_actions'] - 1:
                new_top_k.append(old)   # completed program, do not overwrite
                continue

            draw_val, draw_ind = torch.topk(probs_log[j], k)  # optimization to speed up sorting later

            # TODO XXX use torch.topk for a speedup here
            # for l in range(hyper['num_actions']):
            for dv, di in zip(draw_val, draw_ind):
                new_score = old[0] * math.e**dv    # TODO: use logs
                new_draws = old[2] + [int(di)]
                new_top_k.append( (new_score, hidden_state[0][j], new_draws) )

        new_top_k.sort(key=lambda x: x[0], reverse=True)
        top_k = new_top_k[:k]

        hidden_state    = torch.stack([t[1] for t in top_k]).unsqueeze(0)
        sample          = torch.stack([kronecker_delta(hyper['num_actions'], int(t[2][-1])) for t in top_k])

        # TODO XXX debug printing for top_k

        # if hyper['verbosity'] >= 2:
        #     probs_log_val, probs_log_ind = torch.topk(probs_log[0], 5)
        #     print('        NProb Argmax')
        #     for j in range(len(probs_log_ind)):
        #         print('#{:4d}:   {:4.2f}  {:.7}'.format(probs_log_ind[j], float(math.e**probs_log_val[j]), ('* {:5.2f}'.format(value.item())) * bool(draw == probs_log_ind[j]) ))

        # if draw == hyper['num_actions'] - 1:
        #     break  # stop symbol

    return top_k[0][2][1:]


def score_nn_one_image(target, net=None, hyper={}):
    net.eval()

    if hyper['beam_search_k'] > 1:
        draws = get_draws_beam_search(target, net, hyper)
    else:
        draws = get_draws_greedy(target, net, hyper)

    partial_program = draws_to_partial_program(draws, hyper)

    # if True:  print_two_images(target.images[-1], partial_program.images[-1])
    if hyper['verbosity'] >= 1:
        print()
        if hasattr(target, 'class_encoding'):
            print('Ground truth program:', target.class_encoding)
        print('Program predicted:', draws)
        if len(partial_program.images) == 1:
            print_two_images(target.images[-1], partial_program.images[-1])
        else:
            print('[Illegal image generated by AI]')
            print_two_images(target.images[-1])

    if hyper['powell']:
        # program_as_str = 'c(16,16,16)c(16,16,16)+'
        program_as_str = parse_program_into_str(draws, hyper)

        img, cd = optimize_expression(program_as_str, target.images[-1], metric='chamfer')

        print(draws, program_as_str, cd, img)

        # s = SimulateStack(99, [64, 64])
        # s.generate
        # predict = type('', (), {})   # create empty dummy object
        # predict.images = [torch.Tensor(img)]
        # print(target.images[-1], predict.images[-1])
        # return image_to_image_distance(target, predict, hyper), len(draws)


        prog = parse2000(img)
        print(prog)
        s = SimulateStack(99, [64, 64])
        s.generate_stack(prog)

        image = torch.Tensor(s.stack_t[-1][0])
        print(image.shape)
        print_two_images(target.images[-1], image)

        # shape_fun = [Draw().draw_circle, Draw().draw_square, Draw().draw_triangle][shape_index]
        # ret = shape_fun([x, y], size)
        # ret = torch.from_numpy(np.array(ret, dtype=np.float32))

        # #        union    intersect           difference
        # fun = [torch.max, torch.min, lambda a, b: torch.min(a, 1 - b)][i - hyper['num_actions'] + 4]
        # combined_image = fun(partial_program.images[-2], partial_program.images[-1])



    else:
        # return viz_sim_score(target, partial_program, hyper), i + 1
        cd = image_to_image_distance(target, partial_program, hyper)

    return cd, len(draws)



def score_nn(net=None, hyper={}, test_set=None, mcts=False):

    # TODO factor out common code between these 2 cases
    # TODO: sort into buckets by length of target programs, for synthetic dataset only

    # TRAIN-TEST SCORING
    if hyper['dataset_path'] != '(generate)':

        scores = []
        prog_lens = []

        for i, prog in enumerate(test_set):

            if hyper['dataset_path'].endswith('.h5'):
                target = type('', (), {})   # create empty dummy object
                target.images = [prog]

            elif hyper['dataset_path'].endswith('.txt'):
                target = draws_to_partial_program(prog, hyper)
                target.class_encoding = list(prog)

            if i < 5 and False:
                _verbosity_old = hyper['verbosity']
                hyper['verbosity'] = 3

            if mcts:
                _, draws, _, viz_sim = target_mcts(target, net=net, netv=None, game_number=i, hyper=hyper, temperature=0)
                score = viz_sim_to_dist(viz_sim, hyper)
                program_len = len(draws)
            else:
                score, program_len = score_nn_one_image(target, net=net, hyper=hyper)

            scores.append(score)
            prog_lens.append(program_len)

            if hyper['verbosity'] >= .5:
                print('Test', str(i).rjust(len(str(len(test_set)))), '/', len(test_set), ': {:5.2f}'.format(score))

            if i < 5 and False:
                hyper['verbosity'] = _verbosity_old

        perf = sum(scores) / len(scores)
        perf_perfect = int(sum(x == 0 for x in scores) / len(scores) * 100)
        avg_prog_len = sum(prog_lens) / len(scores)
        # perf_string = '[μ={:4.2f} {:3.0f}% perf]'.format(avg_prog_len, perf_perfect)
        perf_string = '@{:5.2f} {:3.0f}%'.format(avg_prog_len, perf_perfect)


    # ALL-COMBINATION SCORING
    else:
        raise NotImplementedError()

        # perf_string_format = [int(100 * total_scores[i]/total_games[i])/100 for i in range(max_shapes+1)]
        # perf_string = ('[' + ' {:5.2f}' * len(perf_string_format) + ']').format(*perf_string_format)

    return perf, perf_string, scores


















# # TODO: add debug printing for parallel case back in

#         if hyper['verbosity'] >= 2:
#             print('      NProb Argmax')
#             for j in range(len(probs_log[0])):
#                 print('#{:2d}:   {:4.2f}  {:.7}'.format(j, float(math.e**probs_log[0][j]), ('* {:5.2f}'.format(value.item())) * (draw == j) ))

#         if draw == hyper['num_actions'] - 1:
#             break  # stop symbol

#     if hyper['verbosity'] >= 1:
#         print('Actual program:', target.class_encoding)
#         print('Program predicted:', draws)
#         if len(partial_program.images) == 1:
#             print_two_images(target.images[-1], partial_program.images[-1])
#         else:
#             print('[Illegal image generated by AI]')
#             print_two_images(target.images[-1])

def score_nn_NEW_UNDER_CONSTRUCTION(net=None, hyper={}, test_set=None):

    # TODO factor out common code between these 2 cases
    # TODO: sort into buckets by length of target programs, for synthetic dataset only

    # TRAIN-TEST SCORING
    if hyper['dataset_path'] != '(generate)':

        net.eval()

        scores = []
        prog_lens = []


        for i in range(0, len(test_set), hyper['batch_size']):

            target_images = []

            for i_sub in range(min(hyper['batch_size'], len(test_set) - i)):

                print(i_sub)
                prog = test_set[i+i_sub]

                target = draws_to_partial_program(prog, hyper)
                target.class_encoding = list(prog)

                target_image = target.images[-1].unsqueeze(0)
                target_images.append(target_image)

                # score, program_len = score_nn_one_image(target, net=net, hyper=hyper)
                # if hyper['verbosity'] >= 1:  print('! Test image   {:4d} @ len={:4d}   viz_sim: {:5.2f} !'.format(i+1, len(prog), score))


            target_image_stack = torch.stack(target_images)

            partial_program = new_partial_program(hyper, hyper['max_program_length'])
            hidden_state = net.start(target_image_stack)


            draws = []
            sample = [kronecker_delta(hyper['num_actions'], -1)] * hyper['batch_size']
            samples = [torch.stack(sample)] * (hyper['max_program_length'] + 1)
            samples = torch.stack(samples)

            for x in range(hyper['max_program_length']):
                print(x, samples[x].shape)
                hidden_state, probs_log, value = net.step(hidden_state, samples[x])

                draw = torch.argmax(probs_log, dim=1)
                draws.append(draw)

                sample = []
                for d in draw:
                    sample.append(kronecker_delta(hyper['num_actions'], d))
                sample = torch.stack(sample)
                samples[x+1] = sample

                print(probs_log)
                print(sample)

                # draws.append(draw)
                # program_add_instruction(hyper, partial_program, i, draw)



            # viz_sim_score(target, partial_program, hyper), i + 1

            # scores.append(score)
            # prog_lens.append(program_len)


        perf = sum(scores) / len(scores)
        perf_perfect = int(sum(x == 1 for x in scores) / len(scores) * 100)
        if hyper['verbosity'] >= 1:  print('Scores (raw):', scores)
        avg_prog_len = sum(prog_lens) / len(scores)
        perf_string = '[μ={:4.2f} {:3.0f}% perf]'.format(avg_prog_len, perf_perfect)


    # ALL-COMBINATION SCORING
    else:
        raise NotImplementedError()

        # perf_string_format = [int(100 * total_scores[i]/total_games[i])/100 for i in range(max_shapes+1)]
        # perf_string = ('[' + ' {:5.2f}' * len(perf_string_format) + ']').format(*perf_string_format)

    return perf, perf_string





class TrainingError(Exception):
    pass


################### MCTS ###################

class MCTSNode:
    def __init__(self, move, parent):
        self.move = move
        self.parent = parent
        self.games = 0
        self.children = []
        self.is_terminal = False

    def __repr__(self):
        s = ''
        c = self
        while c:
            s += str(c.move) + ' '
            c = c.parent
        return s

MCTSNodeDummy = MCTSNode(None, None)



def print_children(root, recursive=False, depth=0, print_all=False):
    if print_all:
        children = root.children
    else:
        children = [c for c in root.children if c.games > 0]
    root.children_max = torch.zeros(len(root.children))
    if depth == 0:
        print('        NProb PProb  Game WinAvg WinMax  NNVal')
    for c in children:
        j = c.move
        # TODO XXX
        # win_avg = float(c.wins)/c.games if c.games > 0 else float('nan')   # float(root.wins)/root.games
        # # nn_val = float(c.nn_value) if c.games > 1 else float('nan')
        win_avg = float((root.children_wins/root.children_games)[j])
        # if win_avg < -100000:  print(j, win_avg);  exit('Last batch partial')
        win_max = float((root.children_max)[j])
        nn_val = float(c.nn_value) if hasattr(c, 'nn_value') else float('nan')
        if c.games > 0 or depth == 0:
            print((depth * '  ') + '#{:4d}:   {:4.2f}  {:4.2f}   {:3d}  {:5.2f}  {:5.2f}  {:5.2f}'.format(j, root.nn_probs[j], root.children_probs[j], int(root.children_games[j]), win_avg, win_max, nn_val))
        if recursive and c.games > 1:  print_children(c, True, depth+1)
    if depth == 0:
        print('                     --------------------')
        # if not root.is_terminal:
        print('                     {:4d}  {:5.2f}  {:5.2f}  {:5.2f}'.format(root.games - 1, float(sum(root.children_wins) + root.nn_value)/root.games, float(max(root.children_max)), float(root.nn_value) ))

        assert root.games - 1 == sum(int(root.children_games[j]) for j in range(len(root.children_games)))




# MCTS Phase  1. Selection
# Add virtual loss along path to discourage the same leaf selected multiple times
def mcts_selection(cur_node, root, exploration, i0, leaves_nn, target, hyper):

    while not cur_node.games == 0 and not cur_node.is_terminal and cur_node.children:

        # Q = lambda c: (c.wins + init_Q * 10e-30)/(c.games + 10e-30) + (-2) * (c.probability == 0).float()
        # U = lambda c: hyper['exploration'] * c.probability * cur_node.games**.5 / (1 + c.games)
        # cur_node = max(cur_node.children, key=lambda c: Q(c) + U(c))

        # TODO XXXXXX the exploration rate actually grows
        # C(s) is the exploration rate, which grows slowly with search time,
        # C(s) = log ((1 + N (s) + c base )/c base ) + c init
        # what are c_base and c_init ??

        if hyper['utc_bad']:
            q_plus_u = cur_node.children_wins/cur_node.children_games + exploration * (cur_node.children_probs - cur_node.children_games / cur_node.games)
        elif hyper['utc_different']:
            q_plus_u = cur_node.children_wins/cur_node.children_games + exploration * cur_node.children_probs * cur_node.games / (1 + cur_node.children_games)
            # q_plus_u = cur_node.children_wins/cur_node.children_games + exploration * cur_node.children_probs * (cur_node.games / (1 + cur_node.children_games))**.5
        else:
            q_plus_u = cur_node.children_wins/cur_node.children_games + exploration * cur_node.children_probs * cur_node.games**.5 / (1 + cur_node.children_games)
            # q_plus_u = cur_node.children_wins/cur_node.children_games + exploration * cur_node.children_probs * math.log(hyper['exploration_log'] + cur_node.games) * cur_node.games**.5 / (1 + cur_node.children_games)

        argmax = torch.argmax(q_plus_u)

        cur_node.children_wins[argmax] += -1

        cur_node = cur_node.children[argmax]   # this should almost never tie cos of NN probs

        i0 += 1

    # print('+1', cur_node)

    if cur_node.is_terminal:

        attach_outcome(cur_node, i0, target, hyper)
        mcts_backprop(cur_node, root, cur_node.outcome, hyper)

        if hyper['verbosity'] >= 3:
            print('MCTS node (TERM):', cur_node)

        return False

    else:
        leaves_nn.append((cur_node, i0))

        if hyper['verbosity'] >= 3:
            print('MCTS node (neur):', cur_node)

        return True


def stop_selection(cur_node, root, exploration, i0, leaves_nn, target, hyper):

    if cur_node.children_probs[-1] > 0:

        attach_outcome(cur_node.children[-1], i0, target, hyper)
        mcts_backprop(cur_node.children[-1], root, cur_node.children[-1].outcome - 1, hyper)  # '-1' as virtual loss

        if hyper['verbosity'] >= 3:
            print('MCTS node (stOP):', cur_node, 'val', cur_node.children[-1].outcome)


    if cur_node.children_probs[-2] > 0:

        for move in range(-4, -1):
            node = cur_node.children[move]
            leaves_nn.append((node, i0 + 1))

        # add virtual loss
        cur_node.children_wins[move] += -1

        while cur_node != root:
            cur_node.parent.children_wins[cur_node.move] += -1
            cur_node = cur_node.parent


# only calculate CSG image once bottom of MCTS (stop symbol or maxlen) reached
def rec_gen_prog(cur_node, depth, hyper):
    if not hasattr(cur_node.parent, 'partial_program'):
        rec_gen_prog(cur_node.parent, depth-1, hyper)
    cur_node.partial_program = copy_partial_program(cur_node.parent.partial_program)
    program_add_instruction(hyper, cur_node.partial_program, depth, cur_node.move)

def attach_outcome(cur_node, i0, target, hyper):
    if not hasattr(cur_node, 'outcome'):
        rec_gen_prog(cur_node, i0 - 1, hyper)
        cur_node.outcome = viz_sim_score(target, cur_node.partial_program, hyper)



# MCTS Phase  2. Expansion
def make_children(cur_node, root, i0, leaves_nn, target, hyper):
    for j in range(hyper['num_actions']):
        child_node = MCTSNode(j, cur_node)
        child_node.is_terminal = (j == hyper['num_actions'] - 1) or (i0+1 == hyper['max_program_length'])
        # if we put on a shape we get stack_size + 1, if we do a union we remove 2 shapes and add the result (-1)
        child_node.stack_size = cur_node.stack_size + (-1 if (j >= hyper['num_actions'] - 4) else +1)
        cur_node.children.append(child_node)

    # stop_selection(cur_node, root, hyper['exploration'], i0, leaves_nn, target, hyper)


# MCTS Phase  4. Backpropagation
def mcts_backprop(cur_node, root, outcome, hyper):

    # print('-1', cur_node)

    # while cur_node.parent:
    while cur_node != root:
        cur_node.games += 1

        # hack: prevent commonly visited node from EVER being picked again
        if cur_node.games >= hyper['num_playouts_max'] and cur_node.move < hyper['num_actions'] - 4:
            cur_node.parent.children_wins[cur_node.move] -= 10

        cur_node.parent.children_wins[cur_node.move] += outcome + 1  # '+1' to undo virtual loss
        cur_node.parent.children_games[cur_node.move] += 1
        cur_node = cur_node.parent

    cur_node.games += 1







# Zero out options leading to invalid postfix programs (e.g. "++", "AAA", etc)
# Nota bene: Even after zero-ing the probabilities, Q might be higher, leading to a 0% option being selected !
def probs_zero_illegal_options(cur_node, i0, probs, hyper, mask_operation_bad, mask_stop_bad, mask_shape_bad):

    if cur_node.stack_size <= 1:
        probs *= mask_operation_bad
    else:
        probs *= mask_stop_bad

    left = hyper['max_program_length'] - i0 - 1
    if not left >= cur_node.stack_size:
        # adding a shape not ok. reason: new_ss will be ss+1 and to get down to 1 that needs ss unions
        probs *= mask_shape_bad

    if hyper['normalize_probabilities']:
        probs /= torch.sum(probs)

    return probs



# Add noise to ensure exploration during training, disabled during testing (temperature == 0)
def probs_add_noise(cur_node, root, probs, game_number, hyper, temperature):

    if (cur_node == root) and (temperature != 0):
        if torch.isnan(probs).any():
            sys.stderr.write(str(probs) + '\n')
            sys.stderr.write(str(cur_node) + '\n')
            sys.stderr.write('Game number ' + str(game_number) + '\n')
            raise TrainingError('RNN outputted NaN probabilities! Reduce learning rate or add/increase L1/L2 regularization')

        # due to a bug in numpy.random.dirichlet, workers occasionally die during training
        # https://github.com/numpy/numpy/issues/5851
        while True:
            try:
                noise = numpy.random.dirichlet([hyper['dirichlet_alpha']] * torch.sum(probs > 0).item() )  # assert sum(noise) == 1
                break
            except ZeroDivisionError:
                pass

        # only add noise at nonzero probabilities
        # probs[probs > 0] = (1 - hyper['noise_weight']) * probs[probs > 0] + hyper['noise_weight'] * torch.Tensor(noise).cuda()
        # TODO XXX reverse
        probs[probs > 0] = (1 - hyper['noise_weight']) * probs[probs > 0] + hyper['noise_weight'] * torch.Tensor(noise).cuda()

    return probs




# Tree (AlphaGo analogy) = tree of possible CHOICES at each stage (eg operation, parameterized shape, stop symbol)

# `with torch.no_grad():` has a -1% speed increase...

# TODO: stop at stop symbol instead of treating it like a no-op
# TODO: get training/temperature thing sorted out

# Return training samples for Reinforcement Learning using MCTS ("AlphaCSG")
def target_mcts(target, net=None, netv=None, game_number=1, hyper={}, temperature=None):

    if hyper['maximax']:
        raise NotImplementedError()

    # TODO
    if hyper['training_mode'] == 'mcts_pure':
        raise NotImplementedError()

    if temperature is None:
        temperature = hyper['temperature']

    if hyper['verbosity'] >= 1 and hasattr(target, 'class_encoding'):
        print('Ground truth program for MCTS:', target.class_encoding)


    bool_value_loss_weight = bool(hyper['value_loss_weight'])
    mask_operation_bad = (torch.arange(hyper['num_actions']) < hyper['num_actions'] - 4).float().cuda() + (torch.arange(hyper['num_actions']) == hyper['num_actions'] - 1).float().cuda()
    mask_shape_bad = (torch.arange(hyper['num_actions']) >= hyper['num_actions'] - 4).float().cuda()
    mask_stop_bad = (torch.arange(hyper['num_actions']) != hyper['num_actions'] - 1).float().cuda()


    next_root_parent = MCTSNode(-1, None)
    next_root = MCTSNode(-1, next_root_parent)
    next_root.partial_program = new_partial_program(hyper, hyper['max_program_length'])  # rename this to 'sampled_program'
    next_root.stack_size = 0

    target_pdfs = []
    draws = [-1]


    target_image_cpu = target.images[-1].unsqueeze(0)
    target_image = target_image_cpu.unsqueeze(0).cuda()

    if net:
        net.eval()
        with torch.no_grad():
            next_root.parent.hidden_state = net.start(target_image)
        next_root.hidden_state = None
        if hyper['separate_value_net']:
            netv.eval()
            with torch.no_grad():
                next_root.parent.hidden_statev = netv.start(target_image)

        # ugly hack...
        cnn_output_batch = torch.stack([net.cnn_output[0]] * max(40, hyper['parallel_playouts'])).permute(1, 0, 2)



    for i in range(hyper['max_program_length']):

        if hyper['verbosity'] >= 3:
            print(i, 'i')

        if hyper['keep_root']:
            root = next_root
            root.parent.parent = None  # prevents un-necessary backpropagation of scores
        else:
            # might have to discard root cos we only want to add noise at the current move (= root)
            root_parent = MCTSNode(next_root.parent.move, None)
            root_parent.hidden_state = next_root.parent.hidden_state

            root = MCTSNode(next_root.move, root_parent)
            root.stack_size = next_root.stack_size
            root.hidden_state = next_root.hidden_state
            if hyper['separate_value_net']:
                root_parent.hidden_statev = next_root.parent.hidden_statev
                root.hidden_statev = next_root.hidden_statev
            root.partial_program = next_root.partial_program
            # implicitly, the new root has no children and 0 games

        root.is_terminal = False   # TODO: delme

        # if we get too few with keep_root this can deteriorate results...
        # playouts_to_do = max(hyper['num_playouts'] - root.games, hyper['num_playouts']//2)
        playouts_to_do = max(hyper['num_playouts'] - root.games, hyper['num_playouts_min'])

        leaves_nn = []
        if not hyper['keep_root'] or i == 0:
            leaves_nn = [(root, i)]

        if hyper['verbosity'] >= 1: print('Expanding', playouts_to_do, 'more times')


        # tricky setting to skip inner loop on first iteration and just net.eval the root stored in leaves_nn
        playout_batch_start = - hyper['parallel_playouts'] - 1
        playout_i = 0


        # '+1' to expand children of root
        # if deep in the tree, we subtract games already played by higher levels, so our game total is num_playouts  TODO verify
        # while playouts_to_do > 0:
        while playout_i < playouts_to_do:

            while playout_i - playout_batch_start < hyper['parallel_playouts'] and playout_i < playouts_to_do:

                leaf_hit = mcts_selection(root, root, hyper['exploration'], i, leaves_nn, target, hyper)

                if leaf_hit:
                    playout_batch_start = min(playout_batch_start, playout_i)

                playout_i += 1


            if hyper['verbosity'] >= 3:
                print('Evaluating/Remaining:', str(len(leaves_nn)) + '/' + str(playouts_to_do))


            # TODO: Optimization: Do not evaluate interior nodes if search through exterior nodes is already finished (ok since only total visit count matters)  => only do backprop

            new_leaves_nn = []

            if leaves_nn:

                hidden_states_in = []
                samples_in = []

                for (cur_node, _) in leaves_nn:
                    hidden_states_in.append(cur_node.parent.hidden_state[0][0])
                    samples_in.append(kronecker_delta(hyper['num_actions'], cur_node.move))

                hidden_states_in = torch.stack(hidden_states_in).unsqueeze(0)
                samples_in = torch.stack(samples_in)

                # print(hidden_states_in.shape)
                # print(samples_in.shape)

                net.cnn_output = cnn_output_batch[:, :len(leaves_nn), :]

                with torch.no_grad():
                    hidden_states, probs_logs, values = net.step(hidden_states_in, samples_in)
                    # TODO XXX add support for separate_value_net

                # print(hidden_states.shape, probs_logs.shape, values.shape)


                for ind, (cur_node, i0) in enumerate(leaves_nn):

                    if cur_node.children:
                        mcts_backprop(cur_node, root, cur_node.nn_value, hyper)
                        continue

                    # MCTS Phase  3. Simulation (= Playout)
                    #   => replaced by NN valuation here

                    # cur_node.hidden_state = cur_node.parent.hidden_state
                    # probs = torch.tensor([1/2**(i+1) for i in range(hyper['num_actions'])]).cuda();  outcome = 0

                    cur_node.hidden_state, probs_log, value = hidden_states[:, ind:ind+1], probs_logs[ind], values[ind]
                    # print(cur_node.hidden_state.shape, probs_log.shape, value.shape)

                    probs = math.e**probs_log
                    outcome = bool_value_loss_weight * value.item()

                    cur_node.nn_probs = probs.clone().detach()  # for debug printing
                    cur_node.nn_value = outcome


                    probs = probs_zero_illegal_options(cur_node, i0, probs, hyper, mask_operation_bad, mask_stop_bad, mask_shape_bad)
                    probs = probs_add_noise(cur_node, root, probs, game_number, hyper, temperature)

                    cur_node.children_probs     = probs
                    cur_node.children_games     = torch.full([len(probs)], 1e-30).cuda()
                    cur_node.children_wins      = torch.full([len(probs)], hyper['initial_Q'] * 1e-30).cuda()
                    if hyper['jumpstart_completion'] == True or game_number < hyper['jumpstart_completion']:
                        cur_node.children_wins[-4:] = 1 * 1e-30           # give preference to operations and stop
                    cur_node.children_wins += (-7e-30) * (probs == 0).float().cuda()

                    make_children(cur_node, root, i0, new_leaves_nn, target, hyper)

                    mcts_backprop(cur_node, root, outcome, hyper)

            leaves_nn = new_leaves_nn
            playout_batch_start = playouts_to_do + 1


        if hyper['verbosity'] >= 1:
            print_children(root)
            if hyper['verbosity'] >= 2:
                print_children(root, True)

        # updated policy: just the normalized move counts

        if temperature == 0:
            pdf = root.children_games == torch.max(root.children_games)
        else:
            pdf = root.children_games**(1/temperature)

        pdf = pdf.cpu().double()  # OMG i have to convert to double() otherwise numpy complains about pdf not summing to 1 =_=
        the_sum = torch.sum(pdf)
        if the_sum < 1e-300:  print('Error: PDF summing to 0...', pdf, [c.games for c in root.children], root.partial_program)
        pdf /= the_sum



        # print([c.games for c in root.children])
        # train = [root.nn_probs, root.nn_value, pdf]
        target_pdfs.append(pdf.float())

        draw = numpy.random.choice(len(pdf), p=pdf)
        draws.append(draw)

        next_root = root.children[draw]

        # we don't actually need the copy here since we will never go back to the old root
        # next_root.partial_program = copy_partial_program(root.partial_program)
        next_root.partial_program = root.partial_program
        program_add_instruction(hyper, next_root.partial_program, i, draw)

        if hyper['verbosity'] >= 1:
            print('MCTS random draw:', draw)

        if next_root.is_terminal:
            break

        # If we keep_root we did not add during expansion so we add during descend
        # if hyper['keep_root'] and hasattr(next_root, 'probs'):
        if hyper['keep_root']:
            next_root.children_probs = probs_add_noise(next_root, next_root, next_root.children_probs, game_number, hyper, temperature)



    if not hasattr(next_root, 'outcome'):
        next_root.outcome = viz_sim_score(target, next_root.partial_program, hyper)
    viz_sim = next_root.outcome

    if hyper['verbosity'] >= 1:
        print('MCTS result (1 image prediction):', draws)
        if hyper['verbosity'] >= 2:
            if len(next_root.partial_program.images) == 1:
                print_two_images(target.images[-1], next_root.partial_program.images[-1])
            else:
                print('[Invalid lengths]')

        try:
            elapsed = datetime.datetime.now() - hyper['start_time']  # TODO this prints "1 day, ..."
        except TypeError:
            elapsed = 0

        score = viz_sim_to_dist(viz_sim, hyper)

        print('! Image', str(game_number).rjust(7), elapsed, ' viz_sim: {:.2f}'.format(score), '!')
        print('-------------------------------------------------------------------------------------\n')


    # *** TRAINING DATA COLLECTION ***
    # Separate pass with net.train() so we get variables with autograd(=backprop) and also dropout being different
    # I think autograd keeps track of how often we go through the RNN and backprop the appropriate number of times

    # TODO: draws and target_pdf should output a stop symbol next if they hit maxlen, in theory
    return [target_image_cpu, draws, target_pdfs, viz_sim]



# Function partly adapted from 'class Reinforce' of CSGNet
# Return training samples for Reinforcement Learning using Policy Gradients
def target_policy_gradients(target, net=None, hyper={}, *args, **kwargs):

    return [target.images[-1].unsqueeze(0)]



def train_policy_gradients(target_images, net=None, hyper={}):

    net.train()
    partial_programs = []
    for b in range(hyper['batch_size']):
        partial_programs.append( new_partial_program(hyper, hyper['max_program_length']) )
    hidden_state = net.start(target_images)

    pdfs_log = []

    draw = torch.full((hyper['batch_size'],), -1).long().cuda()
    draws = [draw]
    sample = torch.stack([kronecker_delta(hyper['num_actions'], -1) for b in range(hyper['batch_size'])])

    # torch.set_printoptions(threshold=9999999999999)

    for i in range(hyper['max_program_length']):

        # print('In:', sample.shape)
        hidden_state, probs_log, _ = net.step(hidden_state, sample)
        # print('Out:', hidden_state.shape, probs_log.shape)
        pdfs_log.append(probs_log)

        # pdf = (math.e ** probs_log)
        pdf = torch.ones_like(probs_log) / hyper['num_actions']   # uniform for TESTING  # XXX DEL
        draw = torch.multinomial(pdf, 1)#.cuda()  # idk why this throws
        draw = draw.permute(1, 0)[0].cpu().detach()

        draws.append(draw)

        for b in range(hyper['batch_size']):
            program_add_instruction(hyper, partial_programs[b], i, draw[b])
            # sample = torch.stack([kronecker_delta_cpu(hyper['num_actions'], draw[b]) for b in range(hyper['batch_size'])]).cuda()

        sample = torch.stack([kronecker_delta_cpu(hyper['num_actions'], draw[b]) for b in range(hyper['batch_size'])]).detach().cuda()


    # TODO vectorize
    # distance = torch.tensor([
    #     16.0 if len(partial_programs[b].images) != 1 else
    #     chamfer( target_images[b], partial_programs[b].images[-1].unsqueeze(0) ).item()
    #     for b in range(hyper['batch_size'])
    # ])
    distance = torch.tensor([
        16.0 if ( len(partial_programs[b].images) != 1 or draws[1][b] >= 16 or 19 > draws[2][b] >= 16 ) else
        chamfer( target_images[b], partial_programs[b].images[-1].unsqueeze(0) ).item()
        for b in range(hyper['batch_size'])
    ])

    # distance = torch.tensor([
    #     16.0 if ( draws[1][b] not in [0,1,2,3,4,5] or draws[2][b] not in [0,1,2,3,4,5] or draws[3][b] not in [0,1,2,3,4,5] ) else 0
    #     for b in range(hyper['batch_size'])
    # ])
    # distance = torch.tensor([
    #     16.0 if ( draws[1][b] not in [0,1,2,3,4,5] or draws[2][b] not in [6,7,8,9,10,11] or draws[3][b] not in [12,13,14,15,16,17] ) else 0
    #     for b in range(hyper['batch_size'])
    # ])


    # "normalize the distance by the diagonal of the image" (? why? i assume to be in range [0,1] ?)
    R = (1.0 - distance / hyper['image_size'] / (2**0.5))   # reward R
    R = np.clip(R, a_min=0.0, a_max=1.0)**hyper['reward_power']
    R = R.detach()

    # TODO XXX
    net.rolling_avg_reward = hyper['reward_alpha'] * net.rolling_avg_reward + (1 - hyper['reward_alpha']) * R
    advantage = R - net.rolling_avg_reward
    advantage = advantage.detach()

    # print(R)
    # print(advantage)
    # print(net.rolling_avg_reward)

    if hyper['verbosity'] >= 3: print(advantage, R, net.rolling_avg_reward, draws, target.class_encoding)


    # TODO XXX combined rolling average


    # -------------

    # loss = 0
    # for pdf_log, draw in zip(pdfs_log, draws[1:]):
    #     /

    # loss.backward()

    # return None


    # TODO: do not learn past stop symbol

    loss = 0
    for pdf_log, draw in zip(pdfs_log, draws[1:]):
        assert pdf_log.requires_grad
        # print(pdf_log.shape, draw.shape, advantage.shape)
        draw = draw.cuda()

        # print(advantage.cuda())
        # print(pdf_log)
        # print(draw)
        # print( pdf_log.gather(1, draw.view(-1,1)).view(-1) )
        # print( advantage.cuda() @ pdf_log.gather(1, draw.view(-1,1)).view(-1) )

        loss -= advantage.cuda() @ pdf_log.gather(1, draw.view(-1,1)).view(-1)

        # for b in range(hyper['batch_size']):
        #     loss -= advantage[b].cuda() * pdf_log[b][draw[b]]

    # for b in range(hyper['batch_size']):
    #     if R[b] > .5:
    #     # if True:
    #         ii = lambda i: str(int(i)).rjust(2)
    #         ff = lambda f: str(float(round(100 * float(f)) / 100)).ljust(4)
    #         print('[', ii(draws[1][b]), ii(draws[2][b]), ii(draws[3][b]), ']', ff(R[b]), ff(advantage[b]))

    loss /= hyper['batch_size']

    # optimizer.zero_grad()
    loss.backward()
    # optimizer.step()

    # it always goes for 10 10 16 ...
    # - adam/sgd didn't help
    # - lr/hyper adjustment didn't help
    # - i monitor CNN and RNN dying ReLUs but no issue
    # - ...
    # - batch size not tried but prob no help



    # # TODO: This is probably very wrong
    # target_pdfs = []
    # for pdf_log, draw in zip(pdfs_log, draws[1:]):
    #     target_pdf = (math.e ** pdf_log).cpu().detach()
    #     target_pdf[draw] += advantage
    #     target_pdfs.append( target_pdf )


    # temp = []
    # for i in range(batch_size):
    #     neg_log_prob = Variable(torch.zeros(1)).cuda()
    #     # Only summing the probs before stop symbol
    #     for j in range(len_programs[i]):
    #         neg_log_prob = neg_log_prob + probs[j][i, samples[j][i, 0]]
    #     temp.append(neg_log_prob)

    # loss = -torch.cat(temp).view(batch_size, 1)
    # loss = loss.mul(advantage)
    # return loss


    # target_image = target.images[-1].unsqueeze(0)
    # return [target_image, draws, target_pdfs, R]
    return None



# Return training samples for Supervised Learning
def target_supervised(target, hyper={}, *args, **kwargs):

    # target_image = target.images[-1].unsqueeze(0).cuda()
    target_image = target.images[-1].unsqueeze(0)
    draws = [-1] + list(target.class_encoding)[:-1]
    target_pdfs = [kronecker_delta_cpu(hyper['num_actions'], draws[i+1]) for i in range(len(draws) - 1)]

    if hyper['value_loss_weight'] == 0:
        target_score = 1
    else:
        pseudo_target = draws_to_partial_program(target.class_encoding, hyper)
        target_score = viz_sim_score(target, pseudo_target, hyper)

    return [target_image, draws, target_pdfs, target_score]




def target_worker(target_queue, comm_queue, termination_mutex, target_program, net=None, netv=None, train_set=[], worker_id=0, num_workers=0, num_its=9999999999, hyper={}):

    # # TODO XXX
    # if num_workers >= 1:
    #     torch.cuda.set_device(worker_id % 2)

    # # See how many devices are around
    # torch.cuda.device_count()
    # # Set it to a particular device
    # torch.cuda.set_device(1)
    # # Check which device you are on
    # torch.cuda.current_device()

    for i in range(num_its):
        try:
            new_hyper = comm_queue.get_nowait()
            hyper = new_hyper
            if worker_id != 0:  hyper['verbosity'] = 0  # usable debug printing for parallel computing
        except queue.Empty:
            pass

        game_number = worker_id + max(num_workers, 1) * i
        program = next_train_program(hyper, train_set, game_number)
        target = target_program(program, net=net, netv=netv, game_number=game_number, hyper=hyper)
        target_queue.put(target)

    # Make sure all tensors are read before exiting, can cause socket error otherwise
    if num_workers >= 1:
        # termination_mutex.acquire()
        # termination_mutex.release()
        torch.cuda.synchronize()
        target_queue.close()
        torch.cuda.synchronize()
        target_queue.join_thread()
        torch.cuda.synchronize()
        del net
        time.sleep(9999999)  # still SegFaults =(





def parse_program_into_str(p, hyper):

    s = ''

    for op in p:
        if op == hyper['num_actions'] - 1:
            break

        if op >= hyper['num_actions'] - 4:
            s += '+*-'[op + 4 - hyper['num_actions']]
            continue

        s += 'cst'[op % 3] + '('
        s += str(8 + 8 * ((op % 147) // 21)) + ','  # x
        s += str(8 + 8 * (op // 147)) + ','  # y
        s += str(8 + 4 * ((op % 21) // 3)) + ')'  # size

    return s




def parse_str_into_program(s, hyper):

    program = []
    ops_offset = hyper['num_actions'] - 4
    i = 0

    while i < len(s):
        # Example:  t(32,32,32)c(32,40,20)*s(16,16,24)-
        # Format:   shape_type(x, y, size) postfix_operation
        #   x/y are from 8 to 56 in steps of 8  (total 7 levels)
        #   size is from 8 to 32 in steps of 4  (total 7 levels)
        # in the dataset extreme values seem to be rare
        #   -> drawn from normal distribution so most near center !?
        #       Counter({8: 26035, 16: 127670, 24: 274143, 32: 304619, 40: 234361, 48: 85502, 56: 10777})
        #       ['0.0245', '0.1201', '0.2579', '0.2865', '0.2204', '0.0804', '0.0101']

        # Outputs numbered by the following for-loop hierarchy

        # y
        #     x
        #         size
        #             shape

        if s[i] in 'cst':
            # TODO: support t and s
            j = s[i:].find(')')
            x, y, size = make_tuple(s[i+1:i+j+1])

            if hyper['shape_actions'] == [4, 4, 1, 1]:
                shp = 4*round(y/18) + round(x/18)   # XXX ugly hack
            elif hyper['shape_actions'] == [7, 7, 1, 1]:
                shp = 7 * (y//8 - 1) + (x//8 - 1)
            elif hyper['shape_actions'] == [7, 7, 7, 1]:
                shp = 49 * (y//8 - 1) + 7 * (x//8 - 1) + (size//4 - 2)
            elif hyper['shape_actions'] == [7, 7, 7, 3]:
                shp = 147 * (y//8 - 1) + 21 * (x//8 - 1) + 3 * (size//4 - 2) + 'cst'.index(s[i])
            else:
                exit("hyper['shape_actions'] invalid")

            program.append( shp )
            i = i+j+1

        elif s[i] in '+*-':
            program.append( ops_offset + '+*-'.index(s[i]) )
            i += 1

        else:
            exit('Parsing error in file', hyper['dataset_path'], '(line: ' + s + ')' )

    # add stop symbol
    if len(program) < hyper['max_program_length']:
        program.append(hyper['num_actions'] - 1)

    # print('Parsed program', str(s), 'into', str(program))
    return program


# syn_ops2.txt  etc
# TODO XXX  also allow a holdout set
def load_sets_from_file(hyper):

    if hyper['dataset_path'] == '(generate)':
        return None, None, None

    if hyper['dataset_path'].endswith('.h5'):

        with h5py.File(hyper['dataset_path'], 'r') as hf:
            data_set = torch.from_numpy(np.array(hf.get("val_images"), dtype=np.float32))

    elif hyper['dataset_path'].endswith('.txt'):

        with open(hyper['dataset_path']) as f:
            data_set = [parse_str_into_program(line.rstrip(), hyper) for line in f]

    test_set_size = hyper['test_set_size']
    holdout_set_size = hyper['holdout_set_size']

    # interpret as percentage
    if test_set_size < 1:
        test_set_size = int(test_set_size * len(data_set))
    if holdout_set_size < 1:
        holdout_set_size = int(holdout_set_size * len(data_set))

    # test_set    = torch.stack(data_set[:test_set_size])
    # train_set   = torch.stack(data_set[test_set_size:-holdout_set_size])
    # holdout_set = torch.stack(data_set[-holdout_set_size:])

    test_set    = data_set[:test_set_size]
    train_set   = data_set[test_set_size:-holdout_set_size]
    holdout_set = data_set[-holdout_set_size:]

    # see 'little trick to generate samples for the value net'
    # if hyper['training_mode'] == 'supervised' and hyper['value_loss_weight'] != 0:
    #     for i in range(0, len(train_set), 3):
    #         train_set[i].class_encoding, train_set[i+1].class_encoding = train_set[i+1].class_encoding, train_set[i].class_encoding

    return test_set, train_set, holdout_set
    # return data_set[:test_set_size], data_set[test_set_size:-holdout_set_size], data_set[-holdout_set_size:]



def train(hyper, run_id):

    if hyper['checkpoint_interval'] % hyper['batch_size']:
        exit('Checkpoint interval must be multiple of batch size!')

    if hyper['num_workers_game'] and hyper['checkpoint_interval'] % hyper['num_workers_game']:
        exit('Checkpoint interval must be multiple of num_workers_game!')

    sys.stderr.write('[' + str(hyper['start_time']) + ']\n')
    sys.stderr.write('* Loading dataset ' + str(hyper['dataset_path']) + ' now, please be patient...\n')

    test_set, train_set, holdout_set = load_sets_from_file(hyper)
    sys.stderr.write('* Set sizes (test train holdout): ' + str(len(test_set)) + ' ' + str(len(train_set)) + ' ' + str(len(holdout_set)) + '\n' )

    # Set up network

    net = load_or_new_rnn_net(hyper)
    netv = None
    if hyper['separate_value_net']:
        netv = load_or_new_rnn_net(hyper)

    if hyper['optimizer'] == 'sgd':
        optimizer = optim.SGD(net.parameters(), lr=hyper['learning_rate'], momentum=0.9)
        if hyper['separate_value_net']:
            optimizerv = optim.SGD(netv.parameters(), lr=hyper['learning_rate'], momentum=0.9)
    elif hyper['optimizer'] == 'adam':
        optimizer = optim.Adam(net.parameters(), lr=hyper['learning_rate'])
        if hyper['separate_value_net']:
            optimizerv = optim.Adam(netv.parameters(), lr=hyper['learning_rate'])
    else:
        raise Exception('No valid optimizer chosen!')

    # Set up logging

    os.makedirs('trained_models', exist_ok=True)
    BASE_PATH = 'trained_models/alpha_csg_rnn_' + str(run_id)
    running_loss_val = float('inf')
    running_loss_pdf = float('inf')
    running_score = float('nan')
    convergence_at = float('inf')
    training_buffer = []
    losses = []
    scores = []
    last_lr_decay = 0

    intro_str = 'Training started at ' + str(hyper['start_time']) + '\nStarting RNN training with hyperparameters\n' + json.dumps(hyper, indent=4, sort_keys=True, default=str) + '\n'
    # header = '              Image  |    loss =  value +    pdf  |      cnn0s  |  train   test\n'
    header = '              Image  |    loss =  value +    pdf  |      cnn0s   rnn0shd  |  train   test   μlen perf  mcts\n'
    intro_str_header = intro_str + header
    sys.stderr.write(intro_str_header)
    if hyper['verbosity'] >= .5:  print(intro_str_header, end='')

    with open(BASE_PATH + '.hyper', 'w') as f:
        json.dump(hyper, f, indent=4, sort_keys=True, default=str)
        f.write('\n')
    with open(BASE_PATH + '.score', 'w') as f:
        s = hyper['net_path']
        if s and s != '(new)':
            score_path = s[:s.rfind('_')] + '.score'
            with open(score_path) as f2:
                f.write(f2.read())
        f.write(intro_str_header)

    # Set up multiprocessing

    # termination_mutex = mp.Lock()
    # termination_mutex.acquire()
    # ^ this is cursed and i hate it. do not use
    termination_mutex = None
    processes = []

    comm_queues = []
    for i in range(max(1, hyper['num_workers_game'])):
        comm_queues.append( mp.Queue() )

    target_queue = mp.Queue()
    target_program = {
        'supervised':       target_supervised,
        'mcts_rnn':         target_mcts,
        'policy_gradients': target_policy_gradients,
    }[hyper['training_mode']]

    # NB: num_workers_game=1 means 1 main process and 1 subprocess is different from num_workers_game=0 doing everything in the main process
    if hyper['num_workers_game']:
        for worker_id in range(hyper['num_workers_game']):
            p = mp.Process(target=target_worker, args=[target_queue, comm_queues[worker_id], termination_mutex, target_program, net, netv, train_set, worker_id, hyper['num_workers_game'], hyper['num_images_training']//hyper['num_workers_game'], hyper])
            processes.append(p)
            p.start()



    # Main loop

    for i in range(0, hyper['num_images_training']+1, hyper['batch_size']):

        # *** TEST + SCORE + SAVE MODEL ***

        if i % hyper['checkpoint_interval'] == 0:
            # save model to .pth file
            if hyper['save_models']:
                if hyper['save_models'] == 'all':
                    SAVE_PATH = BASE_PATH + '_' + str(i).zfill(len(str(hyper['num_images_training']+1))) + '.pth'
                else:
                    SAVE_PATH = BASE_PATH + '_latest.pth'
                torch.save(net.state_dict(), SAVE_PATH)  # TODO XXX does this also save the Encoder ?
                if hyper['verbosity'] >= .5 or i <= 1:  print('Saving neural networks to file ', SAVE_PATH)

            # score model
            score_avg = float(running_score) / hyper['checkpoint_interval']
            score_avg = viz_sim_to_dist(score_avg, hyper)  # TODO make this cleaner
            score_avg_nn = score_avg_mcts = float('nan')
            score_avg_nn, score_avg_str, _ = score_nn(net=net, hyper=hyper, test_set=test_set)
            score_avg_nn = int(100 * score_avg_nn) / 100
            scores.append(score_avg_nn)

            # every N (5-10) checkpoints, also test NN+MCTS
            mcts_interval = max(hyper['num_workers_game'] + 2, 8)
            if hyper['test_mcts'] and i % (hyper['checkpoint_interval'] * mcts_interval) == 0:
                score_avg_mcts, _, _ = score_nn(net=net, hyper=hyper, test_set=test_set, mcts=True)
                score_avg_mcts = viz_sim_to_dist(score_avg_mcts, hyper)
                score_avg_mcts = int(100 * score_avg_mcts) / 100
                score_avg_str += ' {:5.2f}'.format(score_avg_mcts)

            # collect other data about model
            loss_val_avg = float(running_loss_val) / hyper['checkpoint_interval']
            loss_pdf_avg = float(running_loss_pdf) / hyper['checkpoint_interval']
            loss_total_avg = loss_val_avg + loss_pdf_avg
            losses.append(loss_total_avg)

            convergence_at = min(convergence_at, i) if score_avg_nn == 1.0 else float('inf')
            elapsed = int((datetime.datetime.now() - hyper['start_time']).total_seconds())

            # TODO maybe add max stddev etc ?
            # cnn_output_len = cnn_output_zeros = rnn_input_len = rnn_input_zeros = rnn_hd_len = rnn_hd_zeros = 0
            cnn_output_len = len(net.cnn_output[0][0])
            cnn_output_zeros = net.cnn_output_running_zeros // hyper['checkpoint_interval']
            rnn_input_len = net.input_op_sz
            rnn_input_zeros = net.rnn_input_running_zeros // max(net.running_train_steps, 1)
            rnn_hd_len = len(net.drop_hd[0])
            rnn_hd_zeros = net.rnn_hd_running_zeros // max(net.running_train_steps, 1)
            net.cnn_output_running_zeros = net.rnn_input_running_zeros = net.rnn_hd_running_zeros = net.running_train_steps = 0

            # TODO XXX convert this into table format so 'loss' etc is on top, saving a lot of vertical space
            outstr = '{:3d}:{:02d}:{:02d}  Image{:9d}  |  loss{:7.2f} = val{:7.2f} + pdf{:7.2f}  |  cnn0s {:4d}/{:4d}  |  train {:5.2f}  test {:5.2f}'.format(*[elapsed//3600, (elapsed//60) % 60, elapsed % 60, i, loss_total_avg, loss_val_avg, loss_pdf_avg, cnn_output_zeros, cnn_output_len, score_avg, score_avg_nn]) + ' ' + score_avg_str + '\n'

            # header = '             Image   |     loss =     val +     pdf  |    cnn0s  |  train   test\n'
            outstr = '{:3d}:{:02d}:{:02d} {:9d}  | {:7.2f} ={:7.2f} +{:7.2f}  |  {:4d}/{:4d} {:4d}/{:4d}  |  {:5.2f}  {:5.2f}'.format(*[elapsed//3600, (elapsed//60) % 60, elapsed % 60, i, loss_total_avg, loss_val_avg, loss_pdf_avg, cnn_output_zeros, cnn_output_len, rnn_hd_zeros, rnn_hd_len, score_avg, score_avg_nn]) + ' ' + score_avg_str + ' \n'


            # POTENTIAL LEARNING RATE DROP  -> not very appropriate for RL scenario ?

            # if len(losses) % 10 == 0 and len(losses) >= 20:
            #     if sum(losses[-10:]) >= sum(losses[-20:-10]):
            #         hyper['learning_rate'] /= 2
            #         outstr = outstr[:-1] + ' LR↓\n'

            # Cut learning rate if loss doesn't reach a new min for `patience` checkpoints
            #   no need to broadcast it to workers_game because they only eval, not train!
            if len(losses) > last_lr_decay + hyper['lr_patience']:

                # if all(losses[-i-1] >= losses[-i-2] for i in range(hyper['lr_patience'])):
                if min(losses[-hyper['lr_patience']:]) > min(losses):
                    hyper['learning_rate'] *= hyper['lr_decay_factor']

                    outstr = outstr[:-1] + 'L\n'

                    last_lr_decay = len(losses) - 1


            # HOTPATCH SETTINGS WITH DIFFERENT HYPERPARAMETERS

            # this should mainly be used for debug printing (set _verbosity higher)
            #   or to drop the learning_rate interactively
            _verbosity_old = hyper['verbosity']
            try:
                with open('.hyper') as f:
                    d = json.load(f)
                    hyper.update(d)
                    if max(_verbosity_old, hyper['verbosity']) >= .5:  print('Changed hyperparameters using', d)
                    if max(_verbosity_old, hyper['verbosity']) >= .5:  print('New hyperparameters:', hyper)

                for i in range(hyper['num_workers_game']):
                    comm_queues[i].put(hyper)

                outstr = outstr[:-1] + 'H\n'  # indicate hyperparameter change with an 'H'
                os.remove('.hyper')
                os.rename('.hyper.next', '.hyper')  # TODO does this work
            except IOError:
                pass
            except json.decoder.JSONDecodeError as e:
                with open('.hyper.out', 'w') as f:
                    print('JSONDecodeError while trying to hotpatch .hyper settings:')
                    print(str(e))
                    f.write('JSONDecodeError while trying to hotpatch .hyper settings:\n')
                    f.write(str(e))


            # supports both interactive and manual change of lr
            for param_group in optimizer.param_groups:  # TODO XXX optimizerv netv
                param_group['lr'] = hyper['learning_rate']


            # OUTPUT + RESET FOR NEXT ITERATION

            sys.stderr.write(outstr)
            with open(BASE_PATH + '.score', 'a') as f:
                f.write(outstr)
            if _verbosity_old >= .5:  print(outstr, end='', flush=True)  # flush as we might change verbosity!

            running_score = running_loss_val = running_loss_pdf = 0

            if elapsed > hyper['time_limit_seconds']:
                break
            if i >= hyper['num_images_training']:
                break


        # *** COLLECT TRAINING SAMPLES ***

        # TODO XXX https://pytorch.org/docs/stable/nn.html#torch.nn.utils.rnn.pad_packed_sequence

        while target_queue.qsize() < hyper['batch_size']:
            if hyper['num_workers_game']:
                # Wait for threads to write to target_queue
                time.sleep(.03)
            else:
                # if num_workers_game=0 we have to descend ourselves as main thread
                target_worker(target_queue, comm_queues[0], termination_mutex, target_program, net=net, netv=netv, train_set=train_set, worker_id=i, num_workers=0, num_its=hyper['batch_size'], hyper=hyper)

        training_examples = []
        target_images = []

        targets = []
        draws = []

        samples = []
        target_pdfs = []
        target_scores = []

        for i_sub in range(hyper['batch_size']):
            # program = next_train_program(hyper, train_set, i+i_sub)
            # target_image = program.images[-1].unsqueeze(0).cuda()

            # TODO maybe just pass index number instead of whole image ?
            target = target_queue.get()
            target_image = target[0]

            target_images.append(target_image.cuda())

            if len(target) == 1:
                assert hyper['training_mode'] == 'policy_gradients'
                continue

            target_image, draws, target_pdf, target_score = target

            padding_size = (hyper['max_program_length'] + 1 - len(draws))
            draws += [-1] * padding_size
            target_pdf += [kronecker_delta_cpu(hyper['num_actions'], -1)] * padding_size

            new_samples = torch.stack([kronecker_delta(hyper['num_actions'], draw) for draw in draws])
            samples.append(new_samples)
            target_pdfs.append(torch.stack(target_pdf))
            target_scores.append( torch.tensor([target_score]).float().cuda() )



        # *** FORWARD-FEED THRU NETWORK ***

        # TODO XXX: handle lengths shorter than max_len
        # draws, target_pdfs, target_score = targets

        target_image_stack = torch.stack(target_images)

        if hyper['training_mode'] == 'policy_gradients':
            optimizer.zero_grad()
            train_policy_gradients(target_image_stack, net=net, hyper=hyper)
            optimizer.step()
            continue
            # TODO: catch loss

        samples = torch.stack(samples)
        samples = samples.permute(1, 0, 2)
        target_pdfs = torch.stack(target_pdfs)
        target_pdfs = target_pdfs.permute(1, 0, 2)
        # target_scores = torch.tensor(target_scores).float().cuda()
        target_scores = torch.stack(target_scores)
        # print(samples, '\n', target_pdfs)


        net.train()
        hidden_state = net.start(target_image_stack)
        if hyper['separate_value_net']:
            netv.train()
            hidden_statev = netv.start(target_image_stack)

        for x in range(hyper['max_program_length']):
            hidden_state, nn_probs_log, nn_value = net.step(hidden_state, samples[x])
            if hyper['separate_value_net']:
                hidden_statev, _, nn_value = netv.step(hidden_statev, sample)
            training_examples.append( [nn_probs_log, nn_value, target_pdfs[x], target_scores] )

        running_score += sum(target_scores)



        # *** LOSS + BACKPROP ***

        optimizer.zero_grad()
        if hyper['separate_value_net']:
            optimizerv.zero_grad()

        loss = loss_val = loss_pdf = 0

        # NB the nn_* variables are direct output from the NN and REQUIRE BACKPROP!
        # mcts_* variables are the output from the MCTS and thus the 'desired'/'target' values
        for nn_probs_log, nn_value, mc_pdf, mc_outcome in training_examples:
            assert nn_value.requires_grad and nn_probs_log.requires_grad
            # Loss    (v(s) - z)^2  -  pi^T log(p(s))  +  c ||th||^2    # TODO: div by program len ?
            loss_val += sum( hyper['value_loss_weight'] * abs(nn_value - mc_outcome)**hyper['value_loss_power'] )
            loss_pdf += - torch.Tensor(mc_pdf).contiguous().view(-1).cuda() @ nn_probs_log.contiguous().view(-1)
            # Numerics consideration: if any probability here is exactly 0, then log(0) = -inf !

            # TODO XXX: adapt this to batch processing
            # if hyper['verbosity'] >= 1:  print('Loss:   nn_val {:5.2f}   mc_val {:5.2f}'.format(nn_value.item(), mc_outcome ))
            # if hyper['verbosity'] >= 1:  print('Loss:   nn_pdf ', math.e**nn_probs_log, '  mc_pdf ', torch.Tensor(mc_pdf) )

        loss += loss_val + loss_pdf


        # with L2 regularization to prevent overfitting
        # TODO add this to total loss
        if hyper['l2_regularization']:
            for p in net.parameters():
                loss += hyper['l2_regularization'] * (p**2).sum()

        if hyper['verbosity'] >= 1:  print('************* LOSS BACKWARD **************:   loss {:6.4f}   loss_val {:6.4f}   loss_pdf {:6.4f}'.format( float(loss), float(loss_val), float(loss_pdf) ))

        loss.backward()
        running_loss_val += loss_val
        running_loss_pdf += loss_pdf

        optimizer.step()
        if hyper['separate_value_net']:
            optimizerv.step()

    # termination_mutex.acquire()

    torch.cuda.synchronize()
    del net

    for p in processes:
        p.terminate()

    return (loss_total_avg, score_avg, score_avg_nn, convergence_at)



# 0:18:24      4480  |    1.97 =   0.00 +   1.97  |   825/2048  67/128 1388/2048  |   0.56   0.95 [μ=2.66  82% perf]

# cat trained_models/*2019-09-11_01:25:11.873685*.score





# TODO: image folder and cross-validation or test/train distinction
# TODO: namespace this
hyper_template = {
    'num_playouts': 64, 'exploration': 1.4, 'normalize_probabilities': True, 'initial_Q': 1, 'keep_root': False, 'noise_weight': .25, 'maximax': False,  # MCTS
    'image_size': 64, 'max_program_length': 15, 'viz_sim_mode': 'chamfer', 'moveset': 'postfix',   # CSG
    'dataset_path': '(generate)', 'test_set_size': 100, 'holdout_set_size': 1000, 'net_path': '(new)',   # Datset File Loading
    'gru_architecture': 'gru', 'dropout': [.2, .5], 'separate_value_net': False,   # Network Architecture
    'training_mode': ['mcts_rnn', 'mcts_pure', 'supervised', 'policy_gradients'][0], 'optimizer': 'adam', 'value_loss_weight': 1, 'value_loss_power': 2, 'viz_sim_scale': 1, 'learning_rate': 1e-3, 'lr_patience': 8, 'lr_decay_factor': .2, 'l2_regularization': 0, 'beam_search_k': 1,  # Training
    'save_models': True, 'checkpoint_interval': 320, 'num_images_training': 999999999, 'time_limit_seconds': 999999999, 'verbosity': 0,  # main loop control
}


if len(sys.argv) <= 1 or sys.argv[1] not in ['train', 'test', 'holdout', 'debug']:
    print('', file=sys.stderr)
    print('[TODO] *AlphaCSG* takes a raster image as input and ouputs CSG,', file=sys.stderr)
    print('this uses Neural Nets and the AlphaGo algorithm.', file=sys.stderr)
    print('', file=sys.stderr)
    print('Usage 1: alpha_csg.py [train|test|score] PATH_TO_NEURAL_NETWORK.PTH  [TODO]', file=sys.stderr)
    print('   train:  Trains ENTIRELY NEW network with identical hyperparameters', file=sys.stderr)
    print('   test:   Play 1 human-computer matchup with heavy debug printing', file=sys.stderr)
    print('   score:  Play 100 matches as O against minimax (= perfect play)', file=sys.stderr)
    print('', file=sys.stderr)
    print('Usage 2: alpha_csg.py train  [TODO]', file=sys.stderr)
    print('   Trains networks with random hyperparameters ad infinitum', file=sys.stderr)
    print('   Protip: If you have n cores start this script n times in parallel', file=sys.stderr)
    print('', file=sys.stderr)
    # Verbosities:  -1 silent  0 just chekpoints  1 debug  2 heavy debug
    # TODO: -1 silent or -2 silent and -1 stdout-silent
    # TODO: add 1x30000-type argument parsing
    # exit()


# * LOAD PREVIOUS MODEL OR PARAMETERS *

# not in use -> use hyper[] to load





# ONLY FOR THE DEVELOPER TO TEST FUNCTIONALITY

if 'debug' in sys.argv:

    print('Starting epic gamer debug mode')

    # Debug just the value output.
    # Trying to do hyperparam search here

    hyper = copy(hyper_template)

    hyper['image_size'] = 64
    hyper['num_actions'] = 20
    hyper['viz_sim_scale'] = 1
    hyper['gru_architecture'] = 'gru_plus_linear'

    NUM_IMAGE_CLASSES = 1
    NUM_IMAGE_CLASSES = 2
    NUM_IMAGE_CLASSES = 16

    # target_mode = 'ladder'
    target_mode = '.5 and 1'

    # test distance metrics
    # a = basic_csg_image_from_index(hyper, 2)
    # b = basic_csg_image_from_index(hyper, 3)
    a = new_partial_program(hyper, 3)

    print(viz_sim_score(a, a, hyper))

    print('Result chamfer', chamfer(a.images[-1].unsqueeze(0), a.images[-1].unsqueeze(0)))

    Draw()
    c = Draw().draw_circle([8, 8], 8)
    t = Draw().draw_triangle([8, 8], 8)
    s = Draw().draw_square([8, 8], 8)
    print_two_images(c, t)
    print_two_images(s)

    hyper['shape_actions'] = [7, 7, 7, 3]
    for i in range(9999999999):
        aaa = basic_csg_image_from_index(hyper, i)
        print(aaa)
        print_two_images(aaa[0], aaa[1])
        print()


    for x in range(3):
    # for x in range(9999999):

        hyper['learning_rate'] = 10**random.uniform(-6, -4.1)
        hyper['learning_rate'] = 8e-6   # BEST VALUE for 1 class and **1 acc to hyperparam search
        hyper['learning_rate'] = 8e-4   # BEST VALUE for 1 class and **1 acc to hyperparam search
        hyper['learning_rate'] = 4e-5   # BEST VALUE for 1 class and **1 acc to hyperparam search
        hyper['learning_rate'] = 6e-5   # BEST VALUE for (0,.5) and 2 classes
        # hyper['learning_rate'] = 2e-6   # other seems to kill idk. check gradients ?

        # hyper['learning_rate'] = 10**random.uniform(-6, -3)

        net = load_or_new_rnn_net(hyper)
        params = net.parameters()
        # params = list(net.parameters()) + list(net.encoder.parameters())
        # optimizer = optim.SGD(params, lr=hyper['learning_rate'], momentum=0.9)
        # optimizer = optim.SGD(params, lr=hyper['learning_rate'], momentum=0)
        optimizer = optim.Adam(params, lr=hyper['learning_rate'])

        # for epoch in range(5*(x+1)):
        # for epoch in range(9999999):
        # for epoch in range(300):
        for epoch in range(55):
        # for epoch in range(30 if x == 0 else 80):

            # 1 for const img, 16 for set
            for i in range(NUM_IMAGE_CLASSES):

                for j in range(16):

                    target_image = basic_csg_image_from_index(hyper, i)[0]

                    net.train()
                    optimizer.zero_grad()

                    hidden_state = net.start(target_image.unsqueeze(0).unsqueeze(0))
                    training_examples = []

                    hidden_state, _, value = net.step(hidden_state, kronecker_delta(hyper['num_actions'], j))
                    training_examples.append(value)

                    loss = 0

                    # TODO XXX
                    # - maybe add a layer ?
                    # - see what happens to 1-hot input ?
                    # - train 16-way
                    # - see if we zero optimizer correctly in usual code !!!???

                    # all these below are with dropout=0
                    if target_mode == 'ladder':
                        target = j/16   # RNN differentiation test -- passing, 20 gens @ 4e-5, passable after 10 gens
                    if target_mode == '.5 and 1':
                        # target = (1 if i == j else .5)   # full test -- failing (outputs .75 for 0 and 1 on BOTH images)
                        target = (.5 if i == j else 0)
                        # target = (1 if i == j else 0)
                            # failing after 200 gens
                            # hm seems to start working after... 300 gens
                            # if we pick (0, .5) or (0, 1) as targets instead of (.5, 1) we even get it in 100 gens =)
                            # if we remove tanh we go 100 -> 75 gens
                            # but we need (.5, 1) in the actual algo. without tanh let's say 225 (!?) gens
                            # WHY IS ADDING A BIAS OF +.5 SO MUCH WORSE
                            # i feel like it's batchnorm time lol
                        # target = (1 if i == 1 else .5)   # CNN differentiation test -- passing, 20 gens @ 4e-5
                        # target = .5   # simple bias test -- passing easily

                    for nn_value in training_examples:
                        assert nn_value.requires_grad
                        loss += hyper['value_loss_weight'] * abs(nn_value - target)**hyper['value_loss_power']

                    # target .5, all over .3, avg of multiple, lr 6e-5:
                    # GRU=2048:       [54gens 52gens 58gens]
                    # GRU=128:        [80+gens 80+gens]
                    # GRU=Linear:     [33gens 25gens 29gens]
                    # GRU=2048+Lin:   [37gens 34gens 31gens]

                    loss.backward()
                    optimizer.step()

                    value = float(value)
                    if target > 0:
                        print('{:4d} {:2d} {:2d} {:5.2f} {:5.2f} {:5.2f} '.format(epoch, i, j, target, value, (value - target)**2 ))

        mse = 0

        for i in range(NUM_IMAGE_CLASSES):

            for j in range(16):

                target_image = basic_csg_image_from_index(hyper, i)[0]

                net.eval()

                hidden_state = net.start(target_image.unsqueeze(0).unsqueeze(0))
                hidden_state, _, value = net.step(hidden_state, kronecker_delta(hyper['num_actions'], j))

                value = float(value)

                if target_mode == 'ladder':
                    target = j/16
                if target_mode == '.5 and 1':
                    target = (.5 if i == j else 0)

                mse += (value - target)**2 / (16 * NUM_IMAGE_CLASSES)

                print('   T {:2d} {:2d} {:5.2f} {:5.2f} {:5.2f} '.format(i, j, target, value, (value - target)**2 ))


        print('{:3d} {:.7f}: {:5.3f} {:5.3f}'.format(epoch + 1, hyper['learning_rate'], mse, mse**.5))




if sys.argv[1] in ['test', 'holdout'] and __name__ == '__main__':

    # TODO XXX

    print('Test mode. Usage:')
    print('python3 alpha_csg.py test [net_path] [dataset_path=train_path] [beam_search_k=1] [verbosity=.5]')
    print('CUDA_VISIBLE_DEVICES=2 python3 alpha_csg.py test trained_models/alpha_csg_rnn_2019-10-27_04:22:47.384581_latest.pth cad.h5 10 3')

    LOAD_PATH = sys.argv[2]
    HYPER_PATH = LOAD_PATH[:LOAD_PATH.rfind('_')] + '.hyper'

    print('[' + str(datetime.datetime.now()) + ']')
    print('* Loading hyperparameters from', HYPER_PATH)

    # .update() allows to load hyper files created by previous versions of this software
    hyper = copy(hyper_template)
    with open(HYPER_PATH) as f:
        hyper.update(json.load(f))

    print('* Loading neural network from', LOAD_PATH)
    hyper['net_path'] = LOAD_PATH

    net = load_or_new_rnn_net(hyper)

    if len(sys.argv) >= 4:  hyper['dataset_path'] = sys.argv[3]
    print('* Loading dataset ' + str(hyper['dataset_path']) + ' now, please be patient...')

    hyper['test_mcts'] = False
    if len(sys.argv) >= 5:
        if sys.argv[4] == 'm':
            hyper['test_mcts'] = True
        else:
            hyper['beam_search_k'] = int(sys.argv[4])

    hyper['verbosity'] = max(hyper['verbosity'], .5)
    if len(sys.argv) >= 6:  hyper['verbosity'] = float(sys.argv[5])

    test_set, train_set, holdout_set = load_sets_from_file(hyper)
    if sys.argv[1] == 'holdout':  test_set = holdout_set


    # TODO XXX: test all beam sizes and mcts
    hyper['powell'] = True
    score_avg_nn, score_avg_str, scores = score_nn(net=net, hyper=hyper, test_set=test_set, mcts=hyper['test_mcts'])

    print('Scores (raw):', ' '.join('{:.2f}'.format(s) for s in scores) )

    scores.sort()
    print('Scores (sorted):', ' '.join('{:.2f}'.format(s) for s in scores) )

    print('Quartiles:', numpy.percentile(scores, [0, 25, 50, 75, 100]) )
    print('Average:', score_avg_nn, score_avg_str)

    print('TODO: Histogram ?')






# * REINFORCEMENT LEARNING BY SELF-PLAY *

if 'train' == sys.argv[1] and __name__ == '__main__':
    # random search is superior to grid search because it allows us to also
    #   test potentially unimportant parameters at low cost =)
    # it allows allows us to run multiple processes (limit with my GPU is 3)
    #   which takes advantage of CPU parallelism (almost no parallelism peanlty)

    def random_hyper_generator():
        hyper = copy(hyper_template)
        for i in range(9999999999):
        # for i in range(1):

            try:
                with open(str(i) + '.hyper') as f:
                    hyper.update(json.load(f))

                yield hyper
                continue
            except:
                pass

            # # * RANDOM SEARCH *
            # hyper['exploration'] = 10**random.uniform(0, 1)
            # hyper['learning_rate'] = 10**random.uniform(-5, -3.5)
            # hyper['l2_regularization'] = 10**random.uniform(-4, -.5)
            # # hyper['normalize_probabilities'] = random.choice([True, False])
            # hyper['batch_size'] = random.choice([64, 128, 256])
            # hyper['temperature'] = 10**random.uniform(-.15, 0)
            # hyper['dirichlet_alpha'] = random.choice([.03, .3])
            # hyper['num_playouts'] = random.choice([64, 64, 256])
            # hyper['initial_Q'] = random.choice([0, 0, 1])

            # *** BASICS ***

            hyper['viz_sim_scale'] = {'euclidean_domain_specific': 1, 'euclidean': 1, 'sdf': 10, 'sdf_dist': 1/.7, 'chamfer': 2.5/16}[hyper['viz_sim_mode']]
            hyper['viz_sim_scale'] = 1/32

            hyper['num_images_training'] = 999999999
            # hyper['num_images_training'] = 440 * 18
            hyper['time_limit_seconds'] = 999999999
            # hyper['time_limit_seconds'] = 100 *60

            hyper['shape_actions'] = [4, 4, 1, 1]
            hyper['shape_actions'] = [7, 7, 7, 3]
            hyper['num_actions'] = int(numpy.prod(hyper['shape_actions'])) + 3 + 1   # 3 for union intersection difference  1 for stop symbol
            #
            # hyper['dataset_path'] = 'syn_ops1.txt'
            # hyper['dataset_path'] = 'syn_ops12.txt'
            # hyper['dataset_path'] = 'syn_ops123.txt'
            # hyper['dataset_path'] = 'syn_ops1234.txt'
            # hyper['dataset_path'] = 'syn_ops12345.txt'
            # hyper['dataset_path'] = 'syn_ops_equal_123.txt'
            # hyper['dataset_path'] = 'syn_ops_equal_1234.txt'
            # hyper['dataset_path'] = 'syn_ops_1234567.txt'  # ALL syn ops
            # hyper['dataset_path'] = 'syn_ops_sample_1234567.txt'  # sample to debug
            hyper['dataset_path'] = 'cad.h5'
            #
            # TODO XXX raise by 1 ?
            hyper['max_program_length'] = 16
            if hyper['dataset_path'].startswith('syn_ops'):
                hyper['max_program_length'] = 2 * int(hyper['dataset_path'][hyper['dataset_path'].find('.') - 1]) + 2


            # *** NETWORK ***

            hyper['training_mode'] = 'supervised'
            hyper['training_mode'] = 'mcts_rnn'
            # hyper['training_mode'] = 'policy_gradients'

            hyper['lr_patience'] = {
                'supervised':       5,
                'mcts_rnn':         99999999999,
                'policy_gradients': 99999999999,
            }[hyper['training_mode']]

            # good FIRST STAGE lr, Jan 1e-4, Karpathy 3e-4, CSGNet 1e-3
            hyper['learning_rate'] = {
                'supervised':       1e-3,
                # 'supervised':       3e-4,
                'mcts_rnn':         1e-4,   # 1e-3 alwyas, 3e-4 often  kills the net here
                'policy_gradients': 5e-6    # i don't even know what is happening here  # CSGNet: SGD + 1e-2 (??)
            }[hyper['training_mode']]
            # hyper['learning_rate'] = 0

            hyper['l2_regularization'] = 1e-3
            hyper['l2_regularization'] = 0

            hyper['dropout'] = [.2, .2]  # CSGNet uses [.2, .2]
            hyper['dropout'] = [ 0,  0]

            hyper['value_loss_weight'] = 0
            hyper['value_loss_weight'] = 1


            hyper['save_models'] = False
            hyper['net_path'] = '(new)'
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-10-27_04:22:47.384581_8659200.pth'   # SL syn* 0val
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-07_05:48:05.810155_latest.pth'   # AC 7x7x7x3
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-10_04:25:36.631331_latest.pth'   # SL syn1 --> deleted
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-10_05:18:17.888635_latest.pth'   # SL syn1 RL
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-10_08:13:28.145002_latest.pth'   # SL syn*
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-13_17:30:15.006603_6406400_usable.pth'   # SL syn12
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-14_07:51:39.985724_latest.pth'   # SL syn12 double pretrain
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-15_02:09:43.082644_0.97.pth'   # SL syn123
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-15_02:09:43.082644_0.78.pth'   # SL syn123 *  better
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-15_02:09:43.082644_4787200-0.58.pth'   # SL syn123 *  mb better ?
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-15_22:04:26.170835_4153600-0.28.pth'   # SL syn123equal (16x syn1, 3x syn2, 1x syn3)
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-17_00:35:59.931308_3097600-0.61.pth'   # SL syn123equal jitter
            # hyper['net_path'] = 'trained_models/alpha_csg_rnn_2019-11-17_23:03:16.746106_3168000-0.98.pth'   # SL syn123 jitter


            # *** ALPHACSG ***

            if hyper['training_mode'] == 'mcts_rnn' or True:

                # TODO should be initialized to parent (nn) value probably
                # hyper['initial_Q'] = -1   # aka 'first play urgency', set to -1 in alphazero
                hyper['initial_Q'] = 0    # choose '0' as it is the default NN output
                # hyper['initial_Q'] = .8   # based on a working run from earlier...

                # noise should be more even (higher alpha) the more playouts are available
                hyper['dirichlet_alpha'] = 10 / hyper['num_actions']
                hyper['exploration'] = 2    # jan: lr+exploration should scale with num_playouts
                # hyper['exploration'] = random.choice([4, 8])   # TODO: even lower ?
                hyper['exploration'] = [.333, .5, .75][1]
                hyper['temperature'] = 1

                hyper['keep_root'] = True
                hyper['jumpstart_completion'] = (not hyper['net_path'] or hyper['net_path'] == '(new)')

                hyper['num_playouts'] = 100
                hyper['num_playouts'] = 300
                # hyper['num_playouts'] = 500   # no way around this for num_actions ~ 1000
                # hyper['num_playouts'] = 1000


            # *** PARALLELIZATION ***

            if hyper['training_mode'] == 'mcts_rnn' or True:
                hyper['parallel_playouts'] = 1   # 1 # 4

            hyper['num_workers_game'] = 10 if 'rwth-aachen' in socket.gethostname() and hyper['training_mode'] == 'mcts_rnn' else 0
            hyper['num_workers_game'] =  8 if 'rwth-aachen' in socket.gethostname() and hyper['training_mode'] == 'mcts_rnn' else 0
            # hyper['num_workers_game'] =  5 if 'rwth-aachen' in socket.gethostname() and hyper['training_mode'] == 'mcts_rnn' else 0
            # hyper['num_workers_game'] =  4 if 'rwth-aachen' in socket.gethostname() and hyper['training_mode'] == 'mcts_rnn' else 0
            # hyper['num_workers_game'] = 0
                # memory is enough for 5 8 10 11 depending on settings

            # batch_size: should be <=64 even if GPU mem allows more, else CPU bottleneck
            hyper['batch_size'] = {
                'supervised':       32,
                'mcts_rnn':          1,
                'policy_gradients':  8
            }[hyper['training_mode']]


            # *** POLICY GRADIENTS ***

            if hyper['training_mode'] == 'policy_gradients':

                hyper['reward_power'] = 20
                # hyper['reward_power'] = 2
                hyper['reward_alpha'] = .95   # .7 in CSGNet but they used minibatches


            # *** PRE-PROCESSING OF INSTRUCTIONS ***

            # # hyper['num_playouts'] = 64 if hyper['shape_actions'] == [4, 4, 1, 1] else (256 if hyper['shape_actions'] == [7, 7, 1, 1] else 2048)
            # hyper['num_playouts'] = int(hyper['num_actions'] * math.log(hyper['num_actions']))
            # hyper['num_playouts'] = int(hyper['num_actions'] * 6)
            # # hyper['num_playouts'] = random.choice([64, 128, 256])
            hyper['num_playouts_min'] = hyper['num_playouts'] // 4
            hyper['num_playouts_max'] = hyper['num_playouts'] // 4
            hyper['num_playouts_max'] = 999999999999

            # hyper['checkpoint_interval'] = 110   # 11*10*4
            # hyper['checkpoint_interval'] = 384   # div by 2**7 and 3
            # hyper['checkpoint_interval'] = 440   # 11*10*4
            # hyper['checkpoint_interval'] = 2200   # long trainings
            hyper['checkpoint_interval'] = 2200 if hyper['training_mode'] == 'supervised' else 440

            hyper['test_set_size'] = max(100, hyper['checkpoint_interval'])  # about 5% of train time in syn_ops_1234567
            hyper['test_mcts'] = False
            if hyper['dataset_path'] == 'cad.h5':
                hyper['test_set_size'] = .1
            if hyper['checkpoint_interval'] >= 2200:
                hyper['save_models'] = True
                # if hyper['training_mode'] == 'mcts_rnn':
                #     hyper['test_mcts'] = True
            if hyper['training_mode'] != 'mcts_rnn':
                hyper['checkpoint_interval'] *= 32
            hyper['holdout_set_size'] = .05

            # hyper['value_loss_weight'] = 1 if hyper['training_mode'] == 'mcts_rnn' else 0
            hyper['optimizer'] = 'sgd' if hyper['training_mode'] == 'policy_gradients' else 'adam'
            # hyper['optimizer'] = 'sgd' if i % 4 >= 2 else 'adam'
            hyper['optimizer'] = 'adam'

            hyper['verbosity'] = 0   #0 if 'rwth-aachen' in socket.gethostname() else 3
            # hyper['verbosity'] = 1
            hyper['verbosity'] = 2

            hyper['commit'] = 'put commit hash here to ensure reproducibility!'
            hyper['commit'] = '2387367 + stuff'
            hyper['commit'] = 'TBD'
            hyper['comment'] = 'put a comment here to identify the purpose of this experiment!'
            hyper['comment'] = 'supervised pretraining for AlphaCSG w/ jitter'
            # hyper['comment'] = 'AlphaCSG beating 1.14'
            hyper['comment'] = 'AlphaCSG tabula rasa'

            hyper['jitter_images'] = True

            hyper['jumpstart_completion'] = True
            # hyper['jumpstart_completion'] = False   # <--- better at syn_ops12
            # hyper['jumpstart_completion'] = 100
            hyper['max_program_length'] = 4
            hyper['num_workers_game']  = 0
            # hyper['exploration'] = 3  # utc_bad
            hyper['exploration'] = 1  # utc_different
            hyper['initial_Q'] = .3

            # hyper['num_workers_game']  = [0, 1, 2, 4, 8, 10,  1, 1, 1,  1,  1,  1,  8, 8, 8,  8,  8,  8][i]
            # hyper['parallel_playouts'] = [1, 1, 1, 1, 1,  1,  2, 4, 8, 16, 32, 64,  2, 4, 8, 16, 32, 64][i]
            # hyper['checkpoint_interval'] = 160
            # hyper['num_images_training'] = 2400  # 15x 160  1h50m at n=8
            hyper['checkpoint_interval'] = 40

            # hyper['utc_bad'] = True
            hyper['utc_bad'] = False
            hyper['utc_different'] = False

            hyper['powell'] = True


            # TODO XXX  COPY OVER EXPERIMENT RESULTS SO NOT DELETED !!

            hyper['save_models'] = True

            yield hyper


    # first search for few iterations corsely, later finely for more iterations
    # 1h for 10'000 gens  @  32 playouts
    for hyper in random_hyper_generator():
        hyper['start_time'] = datetime.datetime.now()
        run_id = str(hyper['start_time']).replace(' ', '_')
        loss_avg, viz_sim_train, viz_sim_test, convergence_at = train(hyper, run_id)
        # TODO: put checkpoint updates to stderr so we can pipe conveniently!

        with open('alpha_csg_results_master', 'a') as f:
            f.write(run_id + "  explo {:.1f}  temp {:.1f}  lr {:.6f}  reg {:.6f}  dir {:.2f}  val {:4.1f}  initQ {:3.1f}  play {:3d}  bs {:2d}  len {:1d}  viz {:>9s}  |  loss {:4.1f}  train {:4.2f}  test {:4.2f}  conv {:6.0f}\n".format( hyper['exploration'], hyper['temperature'], hyper['learning_rate'], hyper['l2_regularization'], hyper['dirichlet_alpha'], hyper['value_loss_weight'], hyper['initial_Q'], hyper['num_playouts'], hyper['batch_size'], hyper['max_program_length'], hyper['viz_sim_mode'][:9], float(loss_avg), viz_sim_train, viz_sim_test, convergence_at ))


# TODO: automatically write output to .log file

# TODO XXX: how to get final convergence ? lr drop does not seem to work well. neither does removing noise etc
# TODO: add MCTS test accuracy

# https://medium.com/oracledevs/lessons-from-alphazero-part-3-parameter-tweaking-4dceb78ed1e5

# Now note that Dir(1) is uniform. This means that sampling from it will produce either of the above two vectors (or any other valid vector) with equal chance. As ɑ gets smaller, Dirichlet starts to prefer vectors near the basis vectors: (0.98, 0.01, 0.01) or (0.1, 0.9, 0) will be more likely to be drawn than (0.3, 0.3, 0.4). For values greater than 1, the opposite is true — the basis vectors are de-emphasized, and more-balanced vectors are preferred.


# CUDA_VISIBLE_DEVICES=1,2 python3 f.py train




# # ISSUE WITH POLICY GRADIENTS

#       NProb Argmax
# # 0:   0.02
# # 1:   0.01
# # 2:   0.02
# # 3:   0.02
# # 4:   0.02
# # 5:   0.03
# # 6:   0.04
# # 7:   0.02
# # 8:   0.02
# # 9:   0.04
# #10:   0.05
# #11:   0.03
# #12:   0.02
# #13:   0.02
# #14:   0.03
# #15:   0.02
# #16:   0.32  *  0.03
# #17:   0.04
# #18:   0.20
# #19:   0.03
#       NProb Argmax
# # 0:   0.00
# # 1:   0.00
# # 2:   0.00
# # 3:   0.00
# # 4:   0.00
# # 5:   0.00
# # 6:   0.00
# # 7:   0.00
# # 8:   0.00
# # 9:   0.00
# #10:   0.00
# #11:   0.00
# #12:   0.00
# #13:   0.00
# #14:   0.00
# #15:   0.00
# #16:   0.79  *  0.10
# #17:   0.00
# #18:   0.20
# #19:   0.00
#       NProb Argmax
# # 0:   0.00
# # 1:   0.00
# # 2:   0.00
# # 3:   0.00
# # 4:   0.00
# # 5:   0.00
# # 6:   0.00
# # 7:   0.00
# # 8:   0.00
# # 9:   0.00
# #10:   0.00
# #11:   0.00
# #12:   0.00
# #13:   0.00
# #14:   0.00
# #15:   0.00
# #16:   0.96  *  0.21
# #17:   0.00
# #18:   0.04
# #19:   0.00
# Ground truth program: [6, 5, 18]
# Program predicted: [16, 16, 16]


# => cannot learn position (16/18 being useles at 0th/1st index)
    # => maybe it only learns last index?


# dummy testing with groups of 6

#       NProb Argmax
# # 0:   0.02
# # 1:   0.02
# # 2:   0.02
# # 3:   0.02
# # 4:   0.02
# # 5:   0.02
# # 6:   0.06
# # 7:   0.05
# # 8:   0.04
# # 9:   0.04
# #10:   0.06
# #11:   0.04
# #12:   0.08
# #13:   0.12  *  0.03
# #14:   0.11
# #15:   0.12
# #16:   0.08
# #17:   0.10
# #18:   0.00
# #19:   0.00
#       NProb Argmax
# # 0:   0.00
# # 1:   0.00
# # 2:   0.00
# # 3:   0.00
# # 4:   0.00
# # 5:   0.00
# # 6:   0.01
# # 7:   0.01
# # 8:   0.00
# # 9:   0.00
# #10:   0.01
# #11:   0.00
# #12:   0.07
# #13:   0.24
# #14:   0.16
# #15:   0.28  *  0.20
# #16:   0.08
# #17:   0.15
# #18:   0.00
# #19:   0.00
#       NProb Argmax
# # 0:   0.00
# # 1:   0.00
# # 2:   0.00
# # 3:   0.00
# # 4:   0.00
# # 5:   0.00
# # 6:   0.00
# # 7:   0.00
# # 8:   0.00
# # 9:   0.00
# #10:   0.00
# #11:   0.00
# #12:   0.02
# #13:   0.30
# #14:   0.11
# #15:   0.43  *  0.58
# #16:   0.03
# #17:   0.11
# #18:   0.00
# #19:   0.00
# Program predicted: [13, 15, 15]

# the hell





# TODO

# PG partial learning seems to occur but VERY SLOWLY even for 4x4x1x1
# PG problem is we get 100% a lot
# PG fake average slightly too high (.01) so current policy is unlearned ?


# TODO patience drop on LOWERING of CD



# when /2 removed:

# SL-SL:   1.72 (exp 1.69)
# SL-CAD:  6.42 (exp 2.30)


# 376 / 550  perf but 1.87 distance =(












# MCTS node (neur): 1029 1029 1030 1029 1029 1030 367 174
# Evaluating/Remaining: 1/409
# MCTS node (neur): 1031 1031 1031 1030 1031 1029 367 174
# Evaluating/Remaining: 1/409
# MCTS node (neur): 1029 1029 1030 1030 1031 1030 367 174
# Evaluating/Remaining: 1/409
# MCTS node (neur): 1031 1031 1031 1030 1029 1031 367 174
# Evaluating/Remaining: 1/409
# MCTS node (neur): 1029 1029 1030 1031 1030 1030 367 174
# Evaluating/Remaining: 1/409
# MCTS node (neur): 1031 1031 1031 1029 1030 1029 367 174
# Evaluating/Remaining: 1/409
# MCTS node (neur): 1029 1029 1030 1030 1029 1030 367 174
# Evaluating/Remaining: 1/409


# maybe force going into stop symbol once ? if prob > 0 ?
# at all depths or just top level ?
# IMO top level okay and it will catch on for lower levels



# Training started at 2019-10-03 12:46:15.447845
# good MCTS_RNN without value tho



# subtle effect of initial Q and schlupf


  #   1:   0.05  0.05     7   1.02   0.00   0.45
    #  16:   0.27  0.27     2   0.61   0.00   0.91
      #  19:   0.00  1.00     1   0.32   0.00    nan
    #  17:   0.12  0.12     2   0.51   0.00   0.98
      #  19:   0.00  1.00     1   0.04   0.00    nan
    #  18:   0.61  0.61     2   0.72   0.00   0.91
      #  19:   0.00  1.00     1   0.53   0.00    nan
  #   2:   0.05  0.05     7   1.01   0.00   0.48
    #  16:   0.26  0.26     2   0.61   0.00   0.88
      #  19:   0.00  1.00     1   0.33   0.00    nan
    #  17:   0.11  0.11     2   0.47   0.00   0.94
      #  19:   0.00  1.00     1   0.00   0.00    nan
    #  18:   0.63  0.63     2   0.70   0.00   0.89
      #  19:   0.00  1.00     1   0.51   0.00    nan
  #   3:   0.06  0.06     7   0.99   0.00   0.46




# BIG ERROR WHEN LOADING A SL-TRAINED FILE IN RL MODE
#   -> this didn't happen with 4x4x1x1 tho ?
#   -> testing without problems
#   maybe model incorrectly shared ?

# Traceback (most recent call last):
#   File "rl_pretest.py", line 2864, in <module>
#     loss_avg, viz_sim_train, viz_sim_test, convergence_at = train(hyper, run_id)
#   File "rl_pretest.py", line 2309, in train
#     target = target_queue.get()
#   File "/usr/lib/python3.7/multiprocessing/queues.py", line 113, in get
#     return _ForkingPickler.loads(res)
#   File "/home/jkonopka/.local/lib/python3.7/site-packages/torch/multiprocessing/reductions.py", line 284, in rebuild_storage_fd
#     fd = df.detach()
#   File "/usr/lib/python3.7/multiprocessing/resource_sharer.py", line 57, in detach
#     with _resource_sharer.get_connection(self._id) as conn:
#   File "/usr/lib/python3.7/multiprocessing/resource_sharer.py", line 87, in get_connection
#     c = Client(address, authkey=process.current_process().authkey)
#   File "/usr/lib/python3.7/multiprocessing/connection.py", line 492, in Client
#     c = SocketClient(address)
#   File "/usr/lib/python3.7/multiprocessing/connection.py", line 619, in SocketClient
#     s.connect(address)
# FileNotFoundError: [Errno 2] No such file or directory





# in SL -> RL

# Process Process-10:
# Traceback (most recent call last):
#   File "/usr/lib/python3.7/multiprocessing/process.py", line 297, in _bootstrap
#     self.run()
#   File "/usr/lib/python3.7/multiprocessing/process.py", line 99, in run
#     self._target(*self._args, **self._kwargs)
#   File "/home/jkonopka/csgnet/rl_pretest3.py", line 1945, in target_worker
#     target = target_program(program, net=net, netv=netv, game_number=game_number, hyper=hyper)
#   File "/home/jkonopka/csgnet/rl_pretest3.py", line 1707, in target_mcts
#     next_root.children_probs = probs_add_noise(next_root, next_root, next_root.children_probs, hyper)
#   File "/home/jkonopka/csgnet/rl_pretest3.py", line 1470, in probs_add_noise
#     noise = numpy.random.dirichlet([hyper['dirichlet_alpha']] * torch.sum(probs > 0).item() )  # assert sum(noise) == 1
#   File "mtrand.pyx", line 4772, in mtrand.RandomState.dirichlet
#   File "mtrand.pyx", line 4779, in mtrand.RandomState.dirichlet
# ZeroDivisionError: float division


# https://github.com/numpy/numpy/issues/5851
# "Due to a bug in numpy.random.dirichlet, one of the ten workers died during training"


# cat syn_ops4.txt syn_ops3.txt syn_ops3.txt syn_ops2.txt syn_ops2.txt syn_ops2.txt syn_ops2.txt syn_ops2.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt syn_ops1.txt | shuf > syn_ops_equal_1234.txt





# FINAL RESULTS

# -- pretrained --

# jitterbug.py   -- train cad with jitter
# pow.py  -- powell's method
# trained_models/alpha_csg_rnn_2019-11-19_09:06:05.159559_latest.pth
# trained_models/alpha_csg_rnn_2019-11-17_23:08:42.584144_latest.pth
#       1.47 |  1.40 |    1.48 | 1.45 | 1.41 || 2.78 |   X.XX |   2.12
#     greedy | train | holdout | beam | mcts || sl   | slbeam | slmcts[explo=0.5]

# TRY SLMCTS BUT WITH HIGHER EXPLO / DIFF INITIAL Q


# trained_models/alpha_csg_rnn_2019-11-19_08:43:32.046455_latest.pth
#       1.53 |  1.48 |    1.63 |      X
#     greedy | train | holdout | search


# -- rasa --

# [compare 4 UTC approaches, also note explo used]
# "2019-11-21 09:42:38.406189"


# -- parallelization --

# old:
# XXX

# new:
# "2019-11-21 09:45:42.839604"




# scp  jkonopka@dmark.informatik.rwth-aachen.de:"~/csgnet/trained_models/alpha_csg_rnn_2019-11-19_09:06:05.159559_*" trained_models/




# CUDA_VISIBLE_DEVICES=1 p jitterbug.py holdout trained_models/alpha_csg_rnn_2019-11-17_23:08:42.584144_latest.pth cad.h5 m 2

