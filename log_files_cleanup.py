
# Delete .score and .hyper files of aborted training sessions
# Usage:  python3 log_files_cleanup.py trained_models/*.score  (pls backup!)
# DO NOT RUN THIS SCRIPT WHILE A TRAINING IS RUNNING! IT MIGHT DELETE THE TRAINING'S LOG!

import os
import sys
import glob


EXAMINE_FILES = glob.glob('trained_models/*.score')
try:
    EXAMINE_FILES = sys.argv[1:]
except:
    pass

print('Files to examine for deletion:', EXAMINE_FILES)

num_removed = 0
num_total = 0

for fn in EXAMINE_FILES:

    if not fn.endswith('.score'):
        continue

    num_total += 1
    num_checkpoints = float('-inf')

    with open(fn) as f:

        for line in f:
            line = line.rstrip()
            if '}' in line:
                num_checkpoints = -1  # accounts for newline at end of file
            num_checkpoints += 1

    # Delete files with fewer than 8 checkpoints
    # This should also handle multi-part trainings (with 2 headers)
    if num_checkpoints < 8:
        print('rm', fn)
        os.remove(fn)
        try:
            os.remove(fn.replace('.score', '.hyper'))
            os.remove(fn.replace('.score', '_latest.pth'))
        except:
            pass
        # TODO: also remove .pth pytorch model files
        num_removed += 1

print('Removed', num_removed, 'out of', num_total, 'files')


