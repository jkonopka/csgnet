# CSGNet

Attempted improvement of the CSGNet framework for my Master's thesis.
CSGNet takes as input a 2D/3D shape and uses Deep Learning to output a CSG program generating that shape.
Tags: Neural Nets, Reinforcement Learning, Monte Carlo Tree Search
