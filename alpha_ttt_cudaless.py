
# Transform the script into one running without CUDA (ie on the CPU)
#   - sub-ideal for training, mostly for testing and scoring purposes

with open('alpha_ttt.py') as f:
    s = f.read()

s = s.replace("map_location='cuda'", "map_location='cpu'")
s = s.replace('.cuda()', '')

exec(s)
