
# Transform the script into one running without CUDA (ie on the CPU)

# TODO: automatically detect this in the main script and proceed with warning message

with open('alpha_csg.py') as f:
    s = f.read()

s = s.replace("map_location='cuda'", "map_location='cpu'")
s = s.replace('.cuda()', '')

exec(s)
