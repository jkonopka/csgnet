# long code solely for applying a library method for powell's method...
#   and Draw()-ing the actual geometry
# code is entirely copied from CSGNet

import numpy as np
import string
from scipy.optimize import minimize
from typing import List





from skimage import draw

class Draw:
    def __init__(self, canvas_shape=[64, 64]):
        """
        Helper function for drawing the canvases.
        :param canvas_shape: shape of the canvas on which to draw objects
        """
        self.canvas_shape = canvas_shape

    def draw_circle(self, center: List, radius: int):
        """
        Draw a circle
        :param center: center of the circle
        :param radius: radius of the circle
        :return:
        """
        arr = np.zeros(self.canvas_shape, dtype=bool)
        xp = [center[0] + radius, center[0], center[0], center[0] - radius]
        yp = [center[1], center[1] + radius, center[1] - radius, center[1]]

        rr, cc = draw.circle(*center, radius=radius, shape=self.canvas_shape)
        arr[cc, rr] = True
        return arr

    def draw_triangle(self, center: List, length: int):
        """
        Draw a triangle
        :param center: center of the triangle
        :param radius: radius of the triangle
        :return:
        """
        arr = np.zeros(self.canvas_shape, dtype=bool)
        length = 1.732 * length
        rows = [
            int(center[1] + length / (2 * 1.732)),
            int(center[1] + length / (2 * 1.732)),
            int(center[1] - length / 1.732)
        ]
        cols = [
            int(center[0] - length / 2.0),
            int(center[0] + length / 2.0), center[0]
        ]

        rr_inner, cc_inner = draw.polygon(rows, cols, shape=self.canvas_shape)
        rr_boundary, cc_boundary = draw.polygon_perimeter(
            rows, cols, shape=self.canvas_shape)

        ROWS = np.concatenate((rr_inner, rr_boundary))
        COLS = np.concatenate((cc_inner, cc_boundary))
        arr[ROWS, COLS] = True
        return arr

    def draw_square(self, center: list, length: int):
        """
        Draw a square
        :param center: center of square
        :param length: length of square
        :return:
        """
        arr = np.zeros(self.canvas_shape, dtype=bool)
        length *= 1.412
        # generate the row vertices
        rows = np.array([
            int(center[0] - length / 2.0),
            int(center[0] + length / 2.0),
            int(center[0] + length / 2.0),
            int(center[0] - length / 2.0)
        ])
        cols = np.array([
            int(center[1] + length / 2.0),
            int(center[1] + length / 2.0),
            int(center[1] - length / 2.0),
            int(center[1] - length / 2.0)
        ])

        # generate the col vertices
        rr_inner, cc_inner = draw.polygon(rows, cols, shape=self.canvas_shape)
        rr_boundary, cc_boundary = draw.polygon_perimeter(
            rows, cols, shape=self.canvas_shape)

        ROWS = np.concatenate((rr_inner, rr_boundary))
        COLS = np.concatenate((cc_inner, cc_boundary))

        arr[COLS, ROWS] = True
        return arr




class Parser:
    """
    Parser to parse the program written in postfix notation
    """

    def __init__(self):
        self.shape_types = ["c", "s", "t"]
        self.op = ["*", "+", "-"]

    def parse(self, expression: string):
        """
        Takes an empression, returns a serial program
        :param expression: program expression in postfix notation
        :return program:
        """
        program = []
        for index, value in enumerate(expression):
            if value in self.shape_types:
                # draw shape instruction
                program.append({})
                program[-1]["value"] = value
                program[-1]["type"] = "draw"
                # find where the parenthesis closes
                close_paren = expression[index:].index(")") + index
                program[-1]["param"] = expression[index + 2:close_paren].split(
                    ",")
            elif value in self.op:
                # operations instruction
                program.append({})
                program[-1]["type"] = "op"
                program[-1]["value"] = value
            elif value == "$":
                # must be a stop symbol
                program.append({})
                program[-1]["type"] = "stop"
                program[-1]["value"] = "$"
        return program


class PushDownStack(object):
    """Simple Stack implements in the form of array"""

    def __init__(self, max_len, canvas_shape):
        """
        Simulates a push down stack for canvases. Idea can be taken to build
        generic stacks.
        :param max_len: Max length of stack
        :param canvas_shape: shape of canvas
        """
        _shape = [max_len] + canvas_shape
        self.max_len = max_len
        self.canvas_shape = canvas_shape
        self.items = []
        self.max_len = max_len

    def push(self, item):
        if len(self.items) >= self.max_len:
            assert False, "exceeds max len for stack!!"
        self.items = [item.copy()] + self.items

    def pop(self):
        if len(self.items) == 0:
            assert False, "below min len of stack!!"
        item = self.items[0]
        self.items = self.items[1:]
        return item

    def get_items(self):
        # In this we create a fixed shape tensor amenable for further usage
        # we basically create a fixed length stack and fill up the empty
        # space with zero elements
        zero_stack_element = [
            np.zeros(self.canvas_shape, dtype=bool)
            for _ in range(self.max_len - len(self.items))
        ]
        items = np.stack(self.items + zero_stack_element)
        return items.copy()

    def clear(self):
        """Re-initializes the stack"""
        _shape = [self.max_len] + self.canvas_shape
        self.items = []


class SimulateStack:
    def __init__(self, max_len, canvas_shape):
        """
        Takes the program and simulate stack for it.
        :param max_len: Maximum size of stack
        :param canvas_shape: canvas shape, for elements of stack
        """
        self.draw_obj = Draw(canvas_shape=canvas_shape)
        self.draw = {
            "c": self.draw_obj.draw_circle,
            "s": self.draw_obj.draw_square,
            "t": self.draw_obj.draw_triangle
        }
        self.canvas_shape = canvas_shape
        self.op = {"*": self._and, "+": self._union, "-": self._diff}
        # self.stack = CustomStack(max_len, canvas_shape)
        self.stack = PushDownStack(max_len, canvas_shape)
        self.stack_t = []
        self.stack.clear()
        self.stack_t.append(self.stack.get_items())

    def generate_stack(self, program: list, start_scratch=True):
        """
        Executes the program step-by-step and stores all intermediate stack
        states.
        :param program: List with each item a program step
        :param start_scratch: whether to start creating stack from scratch or
        stack already exist and we are appending new instructions. With this
        set to False, stack can be started from its previous state.
        """
        # clear old garbage
        if start_scratch:
            self.stack_t = []
            self.stack.clear()
            self.stack_t.append(self.stack.get_items())

        for index, p in enumerate(program):
            if p["type"] == "draw":
                # normalize it so that it fits for every dimension multiple
                # of 64, because the programs are generated for dimension of 64
                x = int(p["param"][0]) * self.canvas_shape[0] // 64
                y = int(p["param"][1]) * self.canvas_shape[1] // 64
                scale = int(p["param"][2]) * self.canvas_shape[0] // 64
                # Copy to avoid over-write
                layer = self.draw[p["value"]]([x, y], scale)
                self.stack.push(layer)

                # Copy to avoid orver-write
                # self.stack_t.append(self.stack.items.copy())
                self.stack_t.append(self.stack.get_items())
            else:
                # operate
                obj_2 = self.stack.pop()
                obj_1 = self.stack.pop()
                layer = self.op[p["value"]](obj_1, obj_2)
                self.stack.push(layer)
                # Copy to avoid over-write
                # self.stack_t.append(self.stack.items.copy())
                self.stack_t.append(self.stack.get_items())

    def _union(self, obj1: np.ndarray, obj2: np.ndarray):
        return np.logical_or(obj1, obj2)

    def _and(self, obj1: np.ndarray, obj2: np.ndarray):
        return np.logical_and(obj1, obj2)

    def _diff(self, obj1: np.ndarray, obj2: np.ndarray):
        return (obj1 * 1. - np.logical_and(obj1, obj2) * 1.).astype(np.bool)



class ParseModelOutput:
    def __init__(self, unique_draws: List, stack_size: int, steps: int,
                 canvas_shape: List):
        """
        This class parses complete output from the network which are in joint
        fashion. This class can be used to generate final canvas and
        expressions.
        :param unique_draws: Unique draw/op operations in the current dataset
        :param stack_size: Stack size
        :param steps: Number of steps in the program
        :param canvas_shape: Shape of the canvases
        """
        self.canvas_shape = canvas_shape
        self.stack_size = stack_size
        self.steps = steps
        self.Parser = Parser()
        self.sim = SimulateStack(self.stack_size, self.canvas_shape)
        self.unique_draws = unique_draws

    def get_final_canvas(self,
                         outputs: List,
                         if_just_expressions=False,
                         if_pred_images=False):
        """
        Takes the raw output from the network and returns the predicted
        canvas. The steps involve parsing the outputs into expressions,
        decoding expressions, and finally producing the canvas using
        intermediate stacks.
        :param if_just_expressions: If only expression is required than we
        just return the function after calculating expressions
        :param outputs: List, each element correspond to the output from the
        network
        :return: stack: Predicted final stack for correct programs
        :return: correct_programs: Indices of correct programs
        """
        batch_size = outputs[0].size()[0]

        # Initialize empty expression string, len equal to batch_size
        correct_programs = []
        expressions = [""] * batch_size
        labels = [
            torch.max(outputs[i], 1)[1].data.cpu().numpy()
            for i in range(self.steps)
        ]

        for j in range(batch_size):
            for i in range(self.steps):
                expressions[j] += self.unique_draws[labels[i][j]]

        # Remove the stop symbol and later part of the expression
        for index, exp in enumerate(expressions):
            expressions[index] = exp.split("$")[0]
        if if_just_expressions:
            return expressions
        stacks = []
        for index, exp in enumerate(expressions):
            program = self.Parser.parse(exp)
            if validity(program, len(program), len(program) - 1):
                correct_programs.append(index)
            else:
                if if_pred_images:
                    # if you just want final predicted image
                    stack = np.zeros((self.canvas_shape[0],
                                      self.canvas_shape[1]))
                else:
                    stack = np.zeros(
                        (self.steps + 1, self.stack_size, self.canvas_shape[0],
                         self.canvas_shape[1]))
                stacks.append(stack)
                continue
                # Check the validity of the expressions

            self.sim.generate_stack(program)
            stack = self.sim.stack_t
            stack = np.stack(stack, axis=0)
            if if_pred_images:
                stacks.append(stack[-1, 0, :, :])
            else:
                stacks.append(stack)
        if len(stacks) == 0:
            return None
        if if_pred_images:
            stacks = np.stack(stacks, 0).astype(dtype=np.bool)
        else:
            stacks = np.stack(stacks, 1).astype(dtype=np.bool)
        return stacks, correct_programs, expressions

    def expression2stack(self, expressions: List):
        """Assuming all the expression are correct and coming from
        groundtruth labels. Helpful in visualization of programs
        :param expressions: List, each element an expression of program
        """
        stacks = []
        for index, exp in enumerate(expressions):
            program = self.Parser.parse(exp)
            self.sim.generate_stack(program)
            stack = self.sim.stack_t
            stack = np.stack(stack, axis=0)
            stacks.append(stack)
        stacks = np.stack(stacks, 1).astype(dtype=np.float32)
        return stacks

    def labels2exps(self, labels: np.ndarray, steps: int):
        """
        Assuming grountruth labels, we want to find expressions for them
        :param labels: Grounth labels batch_size x time_steps
        :return: expressions: Expressions corresponding to labels
        """
        if isinstance(labels, np.ndarray):
            batch_size = labels.shape[0]
        else:
            batch_size = labels.size()[0]
            labels = labels.data.cpu().numpy()
        # Initialize empty expression string, len equal to batch_size
        correct_programs = []
        expressions = [""] * batch_size
        for j in range(batch_size):
            for i in range(steps):
                expressions[j] += self.unique_draws[labels[j, i]]
        return expressions


class Optimize:
    """
    Post processing visually guided search using Powell optimizer.
    """

    def __init__(self, query_expression, metric="iou", stack_size=7, steps=15):
        """
        Post processing visually guided search.
        :param query_expression: expression to be optimized
        :param metric: metric to be minimized, like chamfer
        :param stack_size: max stack size required in any program
        :param steps: max tim step of any program
        """
        self.parser = ParseModelOutput(canvas_shape=[64, 64], stack_size=stack_size, unique_draws=None, steps=steps)
        self.query_expression = query_expression
        self.get_graph_structure(query_expression)
        self.metric = metric
        self.errors = []

    def get_target_image(self, image: np.ndarray):
        """
        Gets the target image.
        :param image: target image
        :return:
        """
        self.target_image = image

    def get_graph_structure(self, expression):
        """
        returns the nodes (terminals) of the program
        :param expression: input query expression
        :return:
        """
        program = self.parser.Parser.parse(expression)
        self.graph_str = []
        for p in program:
            self.graph_str.append(p["value"])

    def make_expression(self, x: np.ndarray):
        expression = ""
        index = 0
        for e in self.graph_str:
            if e in ["c", "s", "t"]:
                expression += e + "({},{},{})".format(x[index], x[index + 1], x[index + 2])
                index += 3
            else:
                expression += e
        return expression

    def objective(self, x: np.ndarray):
        """
        Objective to minimize.
        :param x: input program parameters in numpy array format
        :return:
        """
        x = x.astype(np.int)
        x = np.clip(x, 8, 56)

        query_exp = self.make_expression(x)
        query_image = self.parser.expression2stack([query_exp])[-1, 0, 0, :, :]

        if self.metric == "iou":
            error = -np.sum(
                np.logical_and(self.target_image, query_image)) / np.sum(
                np.logical_or(self.target_image, query_image))
        elif self.metric == "chamfer":
            error = chamfer(np.expand_dims(self.target_image, 0),
                            np.expand_dims(query_image, 0))
        return error


def validity(program, max_time, timestep):
    """
    Checks the validity of the program.
    :param program: List of dictionary containing program type and elements
    :param max_time: Max allowed length of program
    :param timestep: Current timestep of the program, or in a sense length of
    program
    # at evey index
    :return:
    """
    num_draws = 0
    num_ops = 0
    for i, p in enumerate(program):
        if p["type"] == "draw":
            # draw a shape on canvas kind of operation
            num_draws += 1
        elif p["type"] == "op":
            # +, *, - kind of operation
            num_ops += 1
        elif p["type"] == "stop":
            # Stop symbol, no need to process further
            if num_draws > ((len(program) - 1) // 2 + 1):
                return False
            if not (num_draws > num_ops):
                return False
            return (num_draws - 1) == num_ops

        if num_draws <= num_ops:
            # condition where number of operands are lesser than 2
            return False
        if num_draws > (max_time // 2 + 1):
            # condition for stack over flow
            return False
    if (max_time - 1) == timestep:
        return (num_draws - 1) == num_ops
    return True


def optimize_expression(query_exp: string, target_image: np.ndarray, metric="iou", stack_size=7, steps=15, max_iter = 100):
    """
    A helper function for visually guided search. This takes the target image (or test
    image) and predicted expression from CSGNet and returns the final chamfer distance
    and optmized program with least chamfer distance possible.
    :param query_exp: program expression
    :param target_image: numpy array of test image
    :param metric: metric to minimize while running the optimizer, "chamfer"
    :param stack_size: max stack size of the program required
    :param steps: max number of time step present in any program
    :param max_iter: max iteration for which to run the program.
    :return:
    """
    # a parser to parse the input expressions.
    parser = ParseModelOutput(canvas_shape=[64, 64], stack_size=stack_size,
                              unique_draws=None, steps=steps)

    program = parser.Parser.parse(query_exp)
    if not validity(program, len(program), len(program) - 1):
        return query_exp, 16

    x = []
    for p in program:
        if p["value"] in ["c", "s", "t"]:
            x += [int(t) for t in p["param"]]

    optimizer = Optimize(query_exp, metric=metric, stack_size=stack_size, steps=steps)
    optimizer.get_target_image(target_image)

    if max_iter == None:
        # None will stop when tolerance hits, not based on maximum iterations
        res = minimize(optimizer.objective, x, method="Powell", tol=0.0001,
                       options={"disp": False, 'return_all': False})
    else:
        # This will stop when max_iter hits
        res = minimize(optimizer.objective, x, method="Powell", tol=0.0001, options={"disp": False, 'return_all': False, "maxiter": max_iter})

    final_value = res.fun
    res = res.x.astype(np.int)
    for i in range(2, res.shape[0], 3):
        res[i] = np.clip(res[i], 8, 32)
    res = np.clip(res, 8, 56)
    predicted_exp = optimizer.make_expression(res)
    return predicted_exp, final_value



def parse2000(expression: str):
    """
    Takes an empression, returns a serial program
    :param expression: program expression in postfix notation
    :return program:
    """
    shape_types = ["c", "s", "t"]
    op = ["*", "+", "-"]
    program = []
    for index, value in enumerate(expression):
        if value in shape_types:
            # draw shape instruction
            program.append({})
            program[-1]["value"] = value
            program[-1]["type"] = "draw"
            # find where the parenthesis closes
            close_paren = expression[index:].index(")") + index
            program[-1]["param"] = expression[index + 2:close_paren].split(
                ",")
        elif value in op:
            # operations instruction
            program.append({})
            program[-1]["type"] = "op"
            program[-1]["value"] = value
        elif value == "$":
            # must be a stop symbol
            program.append({})
            program[-1]["type"] = "stop"
            program[-1]["value"] = "$"
    return program

