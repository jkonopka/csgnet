
# This script takes hyperparameter search results as input
#   python3 this.py < alpha_csg_results_master
# and sorts by each possible hyperparameter,
#   e.g. by ascending learning rate
# The output will be one table PER HYPERPARAMETER

import sys

def maybefloat(s):
    try:
        return float(s)
    except:
        return s

records = []

for line in sys.stdin:
    line = line.rstrip()
    if not line:  continue
    fields = line.replace('|', ' ').split()
    vals = [maybefloat(fields[i]) for i in range(2, len(fields), 2)]
    records.append((vals, line))


for i in range(len(records[0][0])):
    records.sort(key=lambda x: x[0][i])
    for vals, line in records:  print(line)
    print('#')



